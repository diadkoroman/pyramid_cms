# -*- coding: utf-8 -*-

import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.rst')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.rst')) as f:
    CHANGES = f.read()

requires = [
    'pymysql',
    'pyramid',
    'pyramid_chameleon',
    'pyramid_debugtoolbar',
    'pyramid_tm',
    'sqlalchemy',
    'sqlalchemy_utils',
    'transaction',
    'zope.sqlalchemy',
    'waitress',
    'gunicorn',
    'wtforms',
    'wtforms_alchemy',
    'wtforms_sqlalchemy'
    ]

setup(name='pyramid_cms',
      version='0.1',
      description='pyramid_cms',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        'Programming Language :: Python',
        'Framework :: Pyramid',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
        ],
      author='Roman Shmatok',
      author_email='ramon1@ukr.net',
      url='',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='pyramid_cms',
      install_requires=requires,
      )
