SHELL := /bin/bash
PROJECTDIR=/home/roman/projects/pyramid/pyramid_cms

pysetup:
	# setup package
	python setup.py develop

install: pysetup
	pip install -r requirements.txt

runtests:
	python $(PROJECTDIR)/setup.py test --test-suite pyramid_cms.tests
	
runtests_RD:
	python $(PROJECTDIR)/setup.py test --test-suite pyramid_cms.assets.handlers.rdhandler.tests
