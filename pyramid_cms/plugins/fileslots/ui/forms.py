# -*- coding: utf-8 -*-
""" Форми для плагіна "Файлові слоти" """

import time

from wtforms.fields import (
    DateTimeField,
    SelectMultipleField,
    SelectField,
    PasswordField,
    FormField,
    FieldList,
    FileField,
    HiddenField,
    TextField,
    TextAreaField
    )

from wtforms import widgets
from wtforms import validators
from wtforms import form
from wtforms_sqlalchemy.fields import (
    QuerySelectField,
    QuerySelectMultipleField
)

from wtforms_alchemy import ModelForm

from pyramid_cms.assets.db.sqlalchemy.connection import DBSession, Q
from pyramid_cms.assets.forms import UIForm
from pyramid_cms.assets.forms import FormInit
from pyramid_cms.assets.handlers import encr
from pyramid_cms.apps.roots import dbmodels as rootsdb
from pyramid_cms.apps.files import dbmodels as filesdb

################################################################################
class NameForm(ModelForm):
    """
    Additional form to create file names
    """
    class Meta:
        model = filesdb.UploadedFileName
        exclude = ['active']
    lang_id = HiddenField()
################################################################################

################################################################################
class AddFileForm(UIForm):
    """ Форма для додавання файлу в слот """
    exclude = \
    (
        'id',
        'created',
        'updated',
        'descriptor',
        'fpath'
    )
    slot_id = HiddenField()
    upload = FileField()
    categs = QuerySelectMultipleField()
    class Meta:
        model = filesdb.UploadedFile
        
    def validate_and_add(self, **kw):
        """ Валідація і додавання файлу """
        columns = \
            [
                c for c in self.Meta.model.columns_names()
                if c not in self.exclude
            ]
        if self.validate():
            
            obj = filesdb.UploadedFile()
            obj.names = []
            obj.categs = []
            for attr in columns:
                setattr(obj,attr, getattr(self, attr).data)

            for n in self.names:
                # titles
                name = filesdb.UploadedFileName()
                name.lang_id = n.lang_id.data
                name.content = n.content.data
                obj.names.append(name)
            
            try:
                """
                Якщо форма має дані про завантажений файл у полі upload - 
                беремо ці дані в роботу
                """
                obj.fpath = self.upload.data.__dict__
            except:
                pass
            obj.categs = self.categs.data
            obj.descriptor = str(int(time.time()))
            try:
                obj.slots.append(Q(filesdb.FileSlot).get(self.slot_id.data))
            except:
                pass
            obj.save()
            return True
        return False
        
def modify_add_form(entries):
    """ Append additional props to add item form """
    class_obj = AddFileForm
    setattr(class_obj,
            'names',
            FieldList(FormField(NameForm),
                      min_entries=entries))

class AddFileFormInit(FormInit):
    """ Form init for edit item form """
    form_class = AddFileForm
    def set_form_vars(self):
        self.f.categs.query = \
            Q(filesdb.FileCategory)\
            .filter(filesdb.FileCategory.active == True)\
            .order_by('inner_name')
################################################################################


################################################################################
class EditFileForm(UIForm):
    """ Форма для редагування файлу в слоті """
    exclude = \
    (
        'id',
        'created',
        'updated',
        'fpath'
    )
    slots = QuerySelectMultipleField()
    upload = FileField()
    categs = QuerySelectMultipleField()
    class Meta:
        model = filesdb.UploadedFile
        
    def validate_and_edit(self, **kw):
        """ Валідація і редагування файлу """
        columns = \
            [
                c for c in self.Meta.model.columns_names()
                if c not in self.exclude
            ]
        if self.validate():
            obj = self._obj
            obj.categs = []
            for attr in columns:
                setattr(obj,attr, getattr(self, attr).data)

            for n in self.names:
                # titles
                #try:
                name = obj.names\
                           .filter_by(lang_id=n.lang_id.data)\
                           .first()
                #except:
                if not name:
                    name = filesdb.UploadedFileName()
                name.lang_id = n.lang_id.data
                name.content = n.content.data
                obj.names.append(name)
            
            try:
                """
                Якщо форма має дані про завантажений файл у полі upload - 
                беремо ці дані в роботу
                """
                obj.fpath = self.upload.data.__dict__
            except:
                pass
            obj.categs = self.categs.data
            obj.slots = self.slots.data
            obj.save()
            return True
        return False
        
def modify_edit_form(entries):
    """ Append additional props to edit item form """
    class_obj = EditFileForm
    setattr(class_obj,
            'names',
            FieldList(FormField(NameForm),
                      min_entries=entries))

class EditFileFormInit(FormInit):
    """ Form init for edit item form """
    form_class = EditFileForm
    def set_form_vars(self):
        self.f.categs.query = \
            Q(filesdb.FileCategory)\
            .filter(filesdb.FileCategory.active == True)\
            .order_by('inner_name')
        self.f.slots.query = \
            Q(filesdb.FileSlot)\
            .filter(filesdb.FileSlot.active == True)\
            .order_by('mnemo')
################################################################################
