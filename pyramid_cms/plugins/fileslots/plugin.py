# -*- coding: utf-8 -*-
"""
Fileslots management plugin
"""
import os
import transaction
from sqlalchemy.sql.expression import asc, desc

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.plugins import Plugin

from pyramid_cms.apps.roots import dbmodels as rootsdb
from pyramid_cms.apps.files import dbmodels as filesdb

from .ui import forms

def langs_count():
    """ Function for counting langs """
    lc = 0
    try:
        lc = Plugin.count_langs(langs=Plugin.get_langs())
    except:
        pass
    return lc
    
class FileSlots(Plugin):
    # languages nessessary for plugin functioning
    langs = Plugin.get_langs()

    langs_count = langs_count()
    _default_class = filesdb.FileSlot
    _add_form_class = forms.AddFileFormInit(forms.modify_add_form(langs_count))
    _edit_form_class = forms.EditFileFormInit(forms.modify_edit_form(langs_count))
    packagename = admin.admin.config.packagename
    template = \
        os.path.join(
            os.path.relpath(
                os.path.dirname(__file__),
                admin.admin.config.basedir), 
            'templates/plugin.pt')
            
    def __init__(self, section_class=None, parent=None):
        self.inner_name = "Fileslot Manager"
        self.mnemo = "fileslots"
        self.widgets = [self.mnemo]
        super().__init__(section_class=section_class, parent=parent)
    
    def _load_default_forms(self):
        """
        За умовчанням у стек форми плагіна завантажується форма за умовч.
        """
        self._add_form_stack = self._add_form_class
        self._edit_form_stack = self._edit_form_class

    def _add_form(self, *args, **kw):
        """
        Create and return add form instance.
        Before we got prepared class through
        'modify_additem_form' function.
        This function adds 'names' fields to the main form
        """
        f = None
        if self._add_form_stack:
            f = self._add_form_stack(*args, **kw)
        return f
    
    def _edit_form(self, *args, **kw):
        """
        Create and return edit form instance.
        Before we got prepared class through
        'modify_edititem_form' function.
        This function adds 'names' fields to the main form
        """
        f = None
        if self._edit_form_stack:
            f = self._edit_form_stack(*args, **kw)
        return f

    def get_itemdict(self, item):
        def _get_filedict(fitem):
            f = \
            {
                'id': str(fitem.id),
                'name': fitem.inner_name,
                'ftype': os.path.splitext(fitem.fpath)[1] if fitem.fpath else '',
                'categs': [(str(c.id),c.inner_name) for c in fitem.categs.all()],
                'active': fitem.active
            }
            return f
        # для кожного об’єкта проводимо цю дію, щоб вивести на клієнта дані
        i = {
                'id': str(item.id),
                'name': item.inner_name,
                'mnemo': item.mnemo,
                'itemslist': list(map(_get_filedict, item.files.order_by(desc(filesdb.UploadedFile.id)))),
                'active': item.active
            }
        return i

    def get_items(self):
        """
        Отримуємо перелік елементів
        """
        items = Q(self._current_class)
        return list(map(self.get_itemdict, items))

""" Реєстрація плагіна в загальному стеку """
admin.admin._plugins.append(FileSlots)
