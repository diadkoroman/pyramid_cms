# -*- coding: utf-8 -*-
"""
Module with Plugin main class
"""
import os
#import transaction
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.plugins import Plugin

from .ui import forms

class FM(Plugin):
    """
    Плагін файлового менеджера.
    Файловий менеджер запроектовано як універсальне рішення для маніпуляцій
    з файлами всередині пакунка pyramid_cms.
    Може використовуватися інтегровано у ckeditor.
    Оскільки файловий менеджер надає один з ключових сервісів, визначальних
    для функціоналу, то його інтегровано у контекст адмін-частини.
    """
    packagename = admin.admin.config.packagename
    template = "plugins/fm/templates/fm_widget.pt"
    
    """
    Коренева робоча директорія файлового менеджера.
    Використовується при формуванні поточної робочої
    директорії та відносних шляхів до елементів поточної робочої директорії.
    Змінюється лише якщо змінено значення admin.admin.config.files
    """
    rootdir = getattr(admin.admin.config, 'files', '')
    
    """
    Поточна робоча директорія фалового менеджера.
    За умовчанням отримує налаштування з rootdir
    Спеціальний метод аналізує стан рядка запиту, і якщо
    знаходить self.req.GET['path'], об’єднує це значення зі
    значенням поточної робочої директорії
    """
    cwdir = rootdir
    
    """
    Батьківська директорія для поточної (cwdir).
    Визначається спеціальним методом , щоразу при зміні
    поточної робочої директорії.
    """
    cwdir_parent = None
    
    """
    Елементи поточної директорії - стек.
    """
    cwdir_items = None

    # клас форми для додавання файлу
    _add_file_form_class = forms.AddFileFormInit(forms.AddFileForm)
    
    def __init__(self, section_class=None, parent=None):
        """
        Назва віджета, яку можна використовувати у меню адмін-частини
        замість використання назви класу.
        """
        self.inner_name = "File Manager"

        """
        Мнемо-ім’я віджета. За умовчанням додається у стек widgets
        у якості назви основного віджета.
        """
        self.mnemo = self.__class__.__name__.lower()

        """
        У стек widgets додається мнемо-ім’я віджета у якості
        назви основного віджета
        """
        self.widgets = [self.mnemo]
        super().__init__(section_class=section_class, parent=parent)
        
    def _load_default_forms(self):
        """
        За умовчанням у стек форми плагіна завантажується форма для 
        додавання файлів.
        """
        self._form_stack = self._add_file_form_class
        
    def set_cwdir(self, path=None):
        """
        Метод, що встановлює поточну робочу директорію.
        """
        self.cwdir = getattr(admin.admin.config, 'files', '')
        if path:
            self.cwdir = os.path.join(self.cwdir, path)
        return self.cwdir
            
    def list_cwdir(self):
        """
        Виводить вміст поточної робочої директорії
        """
        # очищаємо стек від попередніх значень
        self.cwdir_items = []
        
        cke_vars = self.req.GET
        
        if os.path.isdir(self.cwdir):
            items = os.listdir(self.cwdir)
            for i in items:
                """
                Для кожного елемента директорії знаходимо
                абсолютний на відносний шлях
                """
                abs_item_path = os.path.join(self.cwdir,i)
                rel_item_path = os.path.relpath(abs_item_path,self.rootdir)
                """
                При формуванні урла ми враховуємо наступне:
                1. щоб CKEditor вніс інформацію про клікнутий файл у своє поле
                URL, йому потрібно передати кілька аргументів у спеціальну
                функцію. Щоб не шукати кожен раз ці аргументи, ми відразу
                прив’язуємо їх до всіх урлів, які формуються у файловому
                менеджері.
                Беремо ці аргументи з урла, який формується самим CKEditor-ом
                при відкриванні ним поп-апа з вікном менеджера.
                Аргумент path не враховуємо, бо це "наш" аргумент, що містить
                відносний шлях до файлу/директорії, або не містить взагалі
                ніфіга.
                """
                item_href = '?path={0}&{1}'.format(rel_item_path,\
                            '&'.join([
                                '{0}={1}'.format(k,v) \
                                    for k,v in cke_vars.items()\
                                        if k not in ('path',)])\
                                            if cke_vars else '')
                """
                На майбутнє, якщо буде потреба, у даних, що виводяться, є
                маркери, котрі позначають - директорія це чи файл.
                """
                if os.path.isdir(abs_item_path):
                    self.cwdir_items.append(
                            (
                                item_href,
                                i,
                                '[dir]',
                                os.path.getsize(abs_item_path),
                                False,
                                )
                            )
                elif os.path.isfile(abs_item_path):
                    self.cwdir_items.append(
                            (
                                item_href,
                                i,
                                '[file]',
                                os.path.getsize(abs_item_path),
                                # якщо це картинка - виводимо відносний шлях до
                                # неї, щоб зробити thumbnail
                                rel_item_path if os.path.splitext(i)[1] in
                                ('.jpg','.jpeg','.gif','.png', '.ico') else False,
                                )
                            )

            # сортуємо за назвою
            self.cwdir_items.sort(key=lambda i: i[1])
            # сортуємо - директорії перші
            self.cwdir_items.sort(key=lambda i: i[2])
        """
        Лістинг виводить у будь-якому разі - порожній він чи містить якісь дані,
        щоб уникнути помилок при роботі файлового менеджера.
        """
        return self.cwdir_items
    
    # встановити батьківський URL
    def set_parent_url(self):
        # отримуємо змінні CKEditor
        cke_vars = self.req.GET
        '''
        Формуємо шлях до батьківської директорії.
        Формується наступним чином:
        від поточного значення path віднімається останній елемент
        '/'.join(req.GET.get('path').split('/')[:-1])
        далі йдуть значення, отримані з request.GET, за винятком "path"
        (ми його вже опрацювали).
        Параметр return_value дає вказівку методу повертати(True) або 
        не повертати(False) сформоване значення.
        '''
        try:
            parent_path = '/'.join(cke_vars.get('path').split('/')[:-1])
            self.cwdir_parent = '?path={0}&{1}'.format(parent_path,\
                            '&'.join([
                                '{0}={1}'.format(k,v) \
                                    for k,v in cke_vars.items() \
                                        if k not in ('path',)]))
        except:
            pass
        return self.cwdir_parent
    
    def _form(self, *args, **kw):
        """ Розгортання універсального ui для плагіна """
        f = None
        if self._form_stack:
            f = self._form_stack(*args, **kw)
        return f
        

admin.admin._plugins.append(FM)
