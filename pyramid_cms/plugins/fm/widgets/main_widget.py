# -*- coding: utf-8 -*-
import os

from pyramid.httpexceptions import HTTPFound
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import DBSession, Q
from pyramid_cms.assets.widgets import AdminWidget

class FM(AdminWidget):
    """
    Головний віджет менеджера файлів.
    Використовується для відображення списку файлів,
    додавання, редагування та видалення файлів.
    """
    template_custom_path = 'plugins/fm/templates'
    downloads_url = admin.admin.config.downloads_dir
    

    def dispatch(self):
        
        """
        Метод, що відповідає за формування виводу даних віджета.
        """
        
        """
        у self.kw['current'] з в’юхи передається екземпляр
        поточного плагіна
        """
        self.curr = self.kw['current']

        """
        Отримуємо значення поточної робочої директорії
        """
        self.wdata['body']['cwdir'] = \
            self.curr.set_cwdir(self.req.GET.get('path', None))
        """
        Отримуємо урл для переходу у батьківську директорію
        """
        self.wdata['body']['parent_url'] = self.curr.set_parent_url()
        
        """
        Створюємо лістинг поточної директорії
        """
        
        self.wdata['body']['listing'] = \
            self.curr.list_cwdir()
        
        if os.path.isfile(self.curr.cwdir):
            """
            # формуємо змінні
            cke_funcNum = self.req.GET.get('CKEditorFuncNum','')
            cke_editor = self.req.GET.get('CKEditor','')
            cke_langCode = self.req.GET.get('langCode','')
            cke_message = ''
            # форматуємо урл, щоб він не був відносним до адмінки
            cke_path = '/{0}'.format(self.req.GET.get('path', None))
            # спеціальна змінна отримує код для ініцалізації внесення урл-а
            self.wdata['body']['url_init'] = '''
            <script type="text/javascript">
            window.opener.CKEDITOR.tools.callFunction({0},'{1}','{2}');
            window.close();
            </script>
            '''.format(cke_funcNum,cke_path,cke_message)
            """
            self.process_file()
            
        elif os.path.isdir(self.curr.cwdir):
            """
            Щоб уникнути глюка, коли після кліка на файлі неможливо повторно
            відкрити вікно файлового браузера (воно відразу закривається)
            потрібно витирати значення url_init, щоб воно не
            підсажувало закриваючий жабаскрипт-код, коли це не потрібно. Якщо
            цього не зробити, url_init зберігається у пам’яті і щоразу підсажує
            на сторінку код із рядків 193-198.Через це вікно відразу закривається.
            """
            self.process_dir()
            #self.wdata['body']['url_init'] = False
        
            
    def process_file(self):
        """ Опрацювання кліку на файлі """
        # формуємо змінні
        cke_funcNum = self.req.GET.get('CKEditorFuncNum','')
        cke_editor = self.req.GET.get('CKEditor','')
        cke_langCode = self.req.GET.get('langCode','')
        cke_message = ''
        # форматуємо урл, щоб він не був відносним до адмінки
        opt = self.req.GET.get('opt', None)
        """
        Шлях не починається з 'downloads'для картинок - для них є спеціально
        встановлені статичні урли.
        """
        if opt and opt == 'images':
            cke_path = '/{0}'.format(self.req.GET.get('path', None))
        else:
            cke_path = '/{0}/{1}'.format(self.downloads_url,self.req.GET.get('path', None))
        # спеціальна змінна отримує код для ініцалізації внесення урл-а
        self.wdata['body']['url_init'] = """
        <script type="text/javascript">
        window.opener.CKEDITOR.tools.callFunction({0},'{1}','{2}');
        window.close();
        </script>
        """.format(cke_funcNum,cke_path,cke_message)

        
    def process_dir(self):
        """ Опрацювання кліку на директорії """
        self.wdata['body']['url_init'] = False
            
    def get(self):
        """
        Дії, що повинні буть виконані при запиті типу GET
        -------------------------------------------------
        - отримуємо ui з поточного плагіна і записуємо її дані в стек для виводу
        """
        form = self.kw['current']\
            .ui(meta={'csrf_context':self.req.session},
                req=self.req.GET)
        self.wdata['body']['form'] = form
        
    def post(self):
        """
        Дії, що повинні бути виконані при запиті типу POST
        --------------------------------------------------
        """
        form = self.kw['current']\
            .ui(self.req.POST,
                meta={'csrf_context':self.req.session},
                req=self.req.GET)
        if form.validate_and_add(curr=self.curr):
            """
            Якщо форма валідується, це означає, що через неї
            передано валідний файл. Отже, ми викликаємо функцію,
            що опрацьовує клік на файл.
            """
            self.dispatch()
            self.wdata['body']['test'] = 'FORM VALID'
        else:
            self.wdata['body']['test'] = form.errors
        self.wdata['body']['form'] = form

admin.admin.widgets.append(FM)
