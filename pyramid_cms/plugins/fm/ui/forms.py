# -*- coding: utf-8 -*-

"""
Форми для плагіна FM (файловий менеджер)
"""
import os
import transaction

from wtforms import widgets
from wtforms import validators
from wtforms import form
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.fields import (
    SelectMultipleField,
    SelectField,
    FormField,
    FieldList,
    FileField,
    HiddenField
    )

from wtforms_alchemy import ModelForm

from pyramid_cms.assets.db.sqlalchemy.connection import DBSession, Q
from pyramid_cms.assets.forms import UIForm
from pyramid_cms.assets.forms import FormInit
from pyramid_cms.assets.forms import ProtectedForm
from pyramid_cms.apps.files import dbmodels as filesdb

class AddFileForm(ProtectedForm):
    """ Форма для додавання файлів """
    upload = FileField()
    
    def write_file(self, **kw):
        """ Метод, що відповідає за запис файлу """
        
        # екземпляр поточного плагіна
        curr = kw.get('curr', None)
        # екземпляр завантаженого файла
        fileobj = kw.get('fileobj', None)
        try:
            with open(os.path.join(curr.cwdir,fileobj['filename']), 'wb') as f:
                f.write(fileobj['file'].read())
                f.close()
            return True
        except:
            pass
        
    def save_upl_file(self, **kw):
        """ 
        Метод, що створює запис про файл у БД і записує його,
        автоматично визначаючи директорію з розширенням файлу.
        """
        fileobj = kw.get('fileobj', None)
        """
        Якщо файл записався успішно:
        - створюємо екземпляр класу UploadedFile
        """
        try:
            upl_file = filesdb.UploadedFile()
            upl_file.inner_name = fileobj['filename']
            upl_file.fpath = fileobj
            upl_file.save()
        except:
            pass
    
    def validate_and_add(self, **kw):
        """ Валідація і додавання файлу """
        curr = kw.get('curr', None)
        
        if self.validate():
            """
            Якщо форма провалідувалася:
            1. перевіряємо, чи передано файл для завантаження. Якщо файл 
            передано:
            - записуємо файл, використовуючи поточну робочу директорію плагіна,
            котрий передається у метод як kw['curr']
                - створюємо екземпляр класу UploadedFile(завантажений файл)
            """
            if self.upload.data not in (None, ''):
                """
                Якщо форма має дані про завантажений файл у полі upload - 
                беремо ці дані в роботу
                """
                fileobj = self.upload.data.__dict__
                """
                Є потреба записувати файли в ручному режимі, у поточну
                директорію, а не в ту, яку визначить автоматика.
                Тому користуємося методом write_file, а не 
                save_upl_file
                """
                self.write_file(curr=curr, fileobj=fileobj)
            return True
        return False
        
        
    
    def validate_and_edit(self):
        """ Валідація і редагування файлу """
        pass
    
class AddFileFormInit(FormInit):
    """ Ініціалізатор для форми додавання файлу """
    pass
