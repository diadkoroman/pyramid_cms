# -*- coding: utf-8 -*-

"""
Форми для плагіна "Реєстрація користувача"
"""

from wtforms.fields import (
    DateTimeField,
    SelectMultipleField,
    SelectField,
    PasswordField,
    FormField,
    FieldList,
    FileField,
    HiddenField,
    TextField,
    TextAreaField
    )

from wtforms import widgets
from wtforms import validators
from wtforms import form
from wtforms_sqlalchemy.fields import (
    QuerySelectField,
    QuerySelectMultipleField
)

from wtforms_alchemy import ModelForm

from pyramid_cms.assets.db.sqlalchemy.connection import DBSession, Q
from pyramid_cms.assets.forms import UIForm
from pyramid_cms.assets.forms import FormInit
from pyramid_cms.assets.handlers import encr
from pyramid_cms.apps.roots import dbmodels as rootsdb
from pyramid_cms.apps.auth import dbmodels as authdb

###############################################################################
class AddRegisteredUserForm(UIForm):
    """
    Форма реєстрації користувача у адмін-частині
    """
    exclude = \
    (
        'id',
        'created',
        'updated',
        'password',
        'token'
    )
    # новий пароль
    passw1 = \
        PasswordField(
            validators=[
                validators.InputRequired(),
                validators.EqualTo(fieldname='passw2')
                ])
    # новий пароль (повторне введення)
    passw2 = \
        PasswordField(
            validators=[validators.InputRequired()])
    groups = QuerySelectMultipleField()
    class Meta:
        model = authdb.User

    def validate_and_add(self):
        """ Validate and add item """
        columns = \
            [
                c for c in self.Meta.model.columns_names()
                if c not in self.exclude
            ]

        if self.validate():
            obj = authdb.User()
            for attr in columns:
                setattr(obj,attr, getattr(self, attr).data)
            obj.groups = []
            # поле обов'язкове, тому перевіряти ввід додатково немає потреби
            obj.password = encr.encr1(self.passw1.data)
            obj.token = encr.tokenize(self.login.data)
            # groups
            obj.groups = self.groups.data
            obj.save()
            return True
        return False
        
class AddRegisteredUserFormInit(FormInit):
    """ Form init for edit item form """
    form_class = AddRegisteredUserForm
    def set_form_vars(self):
        self.f.groups.query = Q(authdb.Group)
###############################################################################


###############################################################################
def validate_old_password(form, field):
    # валідатор старого пароля
    """
    Валідатор починає працювати тільки якщо заповнено поле для
    зміни пароля.
    """
    if form.passw1.data not in ('', None):
        old_passw = form._obj.password
        if encr.encr1(field.data) != old_passw:
            raise validators.ValidationError('Incorrect Old Password input!')

class EditRegisteredUserForm(UIForm):
    """
    Форма реєстрації користувача у адмін-частині
    """
    exclude = \
    (
        'id',
        'created',
        'updated',
        'password',
        'token'
    )
    # старий пароль
    passw0 = \
        PasswordField(validators=[validate_old_password])
    # новий пароль
    passw1 = \
        PasswordField(validators=[validators.EqualTo(fieldname='passw2')])
    # новий пароль (повторне введення)
    passw2 = \
        PasswordField()
    groups = QuerySelectMultipleField()
    class Meta:
        model = authdb.User

    def validate_and_edit(self):
        """ Validate and change item """
        columns = \
            [
                c for c in self.Meta.model.columns_names()
                if c not in self.exclude
            ]

        if self.validate():
            obj = self._obj
            for attr in columns:
                setattr(obj,attr, getattr(self, attr).data)
            if self.passw1.data not in ('', None):
                """
                При зміні пароля змінюється і токен
                """
                obj.password = encr.encr1(self.passw1.data)
                obj.token = encr.tokenize(self.login.data)
            # groups
            obj.groups = self.groups.data
            obj.save()
            return True
        return False
        
def modify_form():
    """
    Модифікатор форми.
    У цьому випадку лише повертає клас форми без модифікацій
    """
    class_obj = EditRegisteredUserForm
    return class_obj
        
class EditRegisteredUserFormInit(FormInit):
    """ Form init for edit item form """
    form_class = EditRegisteredUserForm
    def set_form_vars(self):
        self.f.groups.query = Q(authdb.Group)
###############################################################################
