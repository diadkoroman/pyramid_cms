# -*- coding: utf-8 -*-

"""
Модуль з плагіном для здійснення реєстрації користувача у системі
"""
import os
import transaction
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.plugins import Plugin

from pyramid_cms.apps.auth import dbmodels as authdb

from .ui import forms

class Registration(Plugin):
    """
    Плагін, що використовується для здійснення реєстрації користувача
    PCMS
    """
    _default_class = authdb.User
    _add_form_class = forms.AddRegisteredUserFormInit()
    _edit_form_class = forms.EditRegisteredUserFormInit()
    
    packagename = admin.admin.config.packagename
    template = \
        os.path.join(
            os.path.relpath(
                os.path.dirname(__file__),
                admin.admin.config.basedir), 
            'templates/plugin.pt')

    def __init__(self, section_class=None, parent=None):
        self.inner_name = "Registration Manager"
        self.mnemo = "registration"
        self.widgets = [self.mnemo]
        super().__init__(section_class=section_class, parent=parent)

    def _load_default_forms(self):
        """
        За умовчанням у стек форми плагіна завантажується форма за умовч.
        """
        self._add_form_stack = self._add_form_class
        self._edit_form_stack = self._edit_form_class
        
    def _add_form(self, *args, **kw):
        """
        Create and return add form instance.
        Before we got prepared class through
        'modify_additem_form' function.
        This function adds 'names' fields to the main form
        """
        f = None
        if self._add_form_stack:
            f = self._add_form_stack(*args, **kw)
        return f
    
    def _edit_form(self, *args, **kw):
        """
        Create and return edit form instance.
        Before we got prepared class through
        'modify_edititem_form' function.
        This function adds 'names' fields to the main form
        """
        f = None
        if self._edit_form_stack:
            f = self._edit_form_stack(*args, **kw)
        return f
        
    def get_itemdict(self, item):
        # для кожного об’єкта User проводимо цю дію, щоб вивести на клієнта дані
        groups = [g.name for g in item.groups]
        i = {
            'id': str(item.id),
            'name': '{0} {1}'.format(item.name, item.surname) if item.name or item.surname else item.login, 
            'username': item.login,
            'is_superuser': '{0}'.format('+' if 'superusers' in groups else '-'),
            'groups': ', '.join(groups),
            'has_password': '{0}'.format('+' if item.password else '-'),
            'token': item.token,
            'active': '{0}'.format('+' if item.active else '-'),
            }
        return i
        
    def get_items(self):
        """
        Отримуємо перелік користувачів
        """
        items = Q(self._default_class)
        return list(map(self.get_itemdict, items))
""" Реєстрація плагіна в загальному стеку """
admin.admin._plugins.append(Registration)
