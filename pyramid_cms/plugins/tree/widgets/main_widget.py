# -*- coding: utf-8 -*-

"""
Widgets for Tree Plugin
"""

from pyramid.httpexceptions import HTTPFound
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import DBSession, Q
from pyramid_cms.assets.widgets import AdminWidget


"""
m = variable for request.GET['m'] (mode)
s = variable for request.GET['s'] (slot)
c = variable for request.GET['c'] (category)
i = variable for request.GET['i'] (item)
"""
class Tree(AdminWidget):
    """
    Multiwidget to display Tree plugin
    """
    template_custom_path = 'plugins/tree/templates'
    
    def dispatch(self):
        """ Dispatcher method """
        curr = self.kw['current']
        self.wdata['name'] = curr.inner_name
        self.wdata['path'] = curr.get_plugin_path(self.req)
        self.wdata['listing'] = {}
        self.wdata['add_form'] = {}
        self.wdata['edit_form'] = {}
        self.wdata['delete_form'] = {}
        if getattr(curr, 'template', False):
            self.wdata['template'] = curr.get_template()
            treenodes = curr.get_treenodes()
            listing = self.wdata['listing']
            listing['admin'] = treenodes[0]
            listing['project'] = treenodes[1]
            if 'add' in self.req.GET:
                self._add_form_widget()
            if 'i' in self.req.GET \
               and (not 'add' in self.req.GET) \
               and (not 'delete' in self.req.GET):
                self._edit_form_widget()
            if 'delete' in self.req.GET:
                self._delete_form_widget()

    def _add_form_widget(self):
        """ Widget of form for adding node items """
        def _data_for_get():
            """ Actions for GET """
            form = self.kw['current']\
                       .ui(meta={'csrf_context':self.req.session},
                           req=self.req.GET)
            return form

        def _data_for_post():
            """ Actions for POST """
            form = self.kw['current']\
                       .ui(
                           self.req.POST,
                           meta={'csrf_context':self.req.session},
                           req=self.req.GET)
            option = self.req.POST.get('option','stay')
            if form.validate_and_add():
                redir_to = self.kw['current'].get_plugin_path(self.req)
                #raise Exception(redir_to)
                raise HTTPFound(location=redir_to)
            return form

        if self.req.method == 'GET':
            form = _data_for_get()
        elif self.req.method == 'POST':
            form = _data_for_post()
        self.wdata['add_form']['langs'] = self.kw['current'].get_langs()
        self.wdata['add_form']['name'] = 'Add tree node item'
        self.wdata['add_form']['body'] = form

    def _edit_form_widget(self):
        """ Widget of form for edit node items """
        try:
            item = int(self.req.GET.get('i'))
            i = self.kw['current'].get(item)
        except:
            item = None
            i = None

        def _data_for_get(i):
            if i:
                form = self.kw['current']\
                           .ui(
                               meta={'csrf_context':self.req.session},
                               obj=i,
                               req=self.req.GET)
            else:
                form = self.kw['current']\
                           .ui(
                               meta={'csrf_context':self.req.session},
                               req=self.req.GET)
            return form

        def _data_for_post(i):
            if i:
                form = self.kw['current']\
                           .ui(
                               self.req.POST,
                               meta={'csrf_context':self.req.session},
                               obj=i,
                               req=self.req.GET)
                option = self.req.POST.get('option','stay')
                if form.validate_and_edit():
                    redir_to = self.kw['current'].get_plugin_path(self.req)
                    raise HTTPFound(location=redir_to)
            form = self.kw['current']\
                       .ui(
                           self.req.POST,
                           meta={'csrf_context':self.req.session},
                           req=self.req.GET)
            return form

        if self.req.method == 'GET':
            form = _data_for_get(i)
        elif self.req.method == 'POST':
            form = _data_for_post(i)
        self.wdata['edit_form']['langs'] = self.kw['current'].get_langs()
        self.wdata['edit_form']['name'] = 'Edit tree node item'
        self.wdata['edit_form']['body'] = form

    def _delete_form_widget(self):
        """ Delete item form """
        # Get value for variable
        delete = self.req.GET.get('delete', None)
        path = self.kw['node'].get_path()
        self.wdata['delete_form']['name'] = ''
        self.wdata['delete_form']['baseurl'] = ''
        self.wdata['delete_form']['back_url'] = ''
        self.wdata['delete_form']['frw_url'] = ''
        self.wdata['delete_form']['childobj'] = ''
        # Getting item to delete
        try:
            slot = self.req.GET.get('s','')
            item = self.req.GET.get('i','')
            i = self.kw['current'].get(int(item))
            self.wdata['delete_form']['name'] = i.inner_name or 'NoName'
            self.wdata['delete_form']['baseurl'] = '{0}?s={1}&i={2}'\
                .format(path, slot, item)
            self.wdata['delete_form']['back_url'] = path
            self.wdata['delete_form']['frw_url'] = '{0}?s={1}&i={2}'\
                .format(path, slot, item)
        except:
            item = None
            i = None
        if delete and delete == 'y':
            if i:
                i.delete()
                raise HTTPFound(location=path)
admin.admin.widgets.append(Tree)

'''
def tree_widget(req, data, **kw):
    """ Multiwidget for tree plugin """
    curr = kw['current']
    data['tree'] = {}
    data['tree']['name'] = curr.inner_name
    data['tree']['path'] = curr.get_plugin_path(req)
    data['tree']['listing'] = {}
    data['tree']['add_form'] = {}
    data['tree']['edit_form'] = {}
    data['tree']['delete_form'] = {}
    if getattr(curr, 'template', False):
        data['tree']['template'] = curr.get_template()
        treenodes = curr.get_treenodes()
        listing = data['tree']['listing']
        listing['admin'] = treenodes[0]
        listing['project'] = treenodes[1]
        # inner call widgets in parent widget
        if 'add' in req.GET:
            add_form_widget(req, data['tree']['add_form'], **kw)
        if 'i' in req.GET and (not 'add' in req.GET) and (not 'delete' in req.GET):
            edit_form_widget(req, data['tree']['edit_form'], **kw)
        if 'delete' in req.GET:
            delete_form_widget(req, data['tree']['delete_form'], **kw)
admin.admin.widgets.append(tree_widget)
'''
'''
def add_form_widget(req, data, **kw):
    """ Widget of form for adding node items """

    def _data_for_get():
        """ Actions for GET """
        if 'add' in req.GET:
            form = kw['current'].ui(meta={'csrf_context':req.session},
                req=req.GET)
            return form
        return None

    def _data_for_post():
        """ Actions for POST """
        if 'add' in req.GET:
            form = kw['current'].ui(req.POST, meta={'csrf_context':req.session},
                req=req.GET)
            option = req.POST.get('option','stay')
            form.validate_and_add()
            redir_to = kw['current'].get_plugin_path(req)
                #raise Exception(redir_to)
            raise HTTPFound(location=redir_to)
            return form
        return None

    if req.method == 'GET':
        form = _data_for_get()
    elif req.method == 'POST':
        form = _data_for_post()
    data['langs'] = kw['current'].get_langs()
    data['name'] = 'Add tree node item'
    data['body'] = form


def edit_form_widget(req, data, **kw):
    # request for update instance
    try:
        item = int(req.GET.get('i'))
        instance = kw['current'].get(item)
    except:
        item = None
        instance = None

    def _data_for_get(instance):
        if instance:
            form = kw['current'].ui(meta={'csrf_context':req.session},
                obj=instance,
                req=req.GET)
        else:
            form = kw['current'].ui(meta={'csrf_context':req.session},
                req=req.GET)
        return form

    def _data_for_post(instance):
        if instance:
            form = kw['current'].ui(req.POST, meta={'csrf_context':req.session},
                obj=instance,
                req=req.GET)
            option = req.POST.get('option','stay')
            form.validate_and_edit()
            redir_to = kw['current'].get_plugin_path(req)
                #raise Exception(redir_to)
            raise HTTPFound(location=redir_to)
        form = kw['current'].ui(req.POST, meta={'csrf_context':req.session},
            req=req.GET)
        return form

    if req.method == 'GET':
        form = _data_for_get(instance)
    elif req.method == 'POST':
        form = _data_for_post(instance)
    data['langs'] = kw['current'].get_langs()
    data['name'] = 'Edit tree node item'
    data['body'] = form


def delete_form_widget(req, data, **kw):
    """ Delete item form """
    # Get value for variable
    delete = req.GET.get('delete', None)
    path = kw['node'].get_path()
    data['name'] = ''
    data['back_url'] = ''
    data['frw_url'] = ''
    data['childobj'] = None
    # Getting item to delete
    try:
        slot = req.GET.get('s')
        item = req.GET.get('i')
        instance = kw['current'].get(int(item))
        data['name'] = getattr(instance,'inner_name', 'noname')
        data['baseurl'] = '{0}?s={1}&i={2}'.format(path, slot, item)
        data['back_url'] = path
        data['frw_url'] = '{0}?s={1}&i={2}'.format(path, slot, item)
    except:
        item = None
        instance = None
    if delete and delete == 'y':
        if instance:
            instance.delete()
            raise HTTPFound(location=path)
'''
