# -*- coding: utf-8 -*-
"""
Module with Plugin main class
"""
import transaction
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.plugins import Plugin

from pyramid_cms.apps.roots import dbmodels as rootsdb
from pyramid_cms.apps.auth import dbmodels as authdb

from .ui import forms

def langs_count():
    """ Function for counting langs """
    lc = 0
    try:
        lc = Plugin.count_langs(langs=Plugin.get_langs())
    except:
        pass
    return lc

class Tree(Plugin):
    """
    Plugin provodong operations with TreeNode objects
    (with a site tree).
    """
    # languages nessessary for plugin functioning
    langs = Plugin.get_langs()

    langs_count =  langs_count()
    # langs_count = 3
    # add treenode form
    _add_treenode_form_class = \
                               forms.TreeAddItemFormInit(
                                   forms.modify_additem_form(langs_count),
                               )

    # edit treenode  form
    _edit_treenode_form_class = \
                                forms.TreeEditItemFormInit(
                                    forms.modify_edititem_form(langs_count),
                                )
    _default_class = rootsdb.TreeNode


    packagename = admin.admin.config.packagename
    template = "plugins/tree/templates/tree_widget.pt"

    def __init__(self, section_class=None, parent=None):
        self.inner_name = "Application Tree"
        self.mnemo = "tree"
        self.widgets = [self.mnemo]
        super().__init__(section_class=section_class, parent=parent)

    def _load_default_forms(self):
        """
        Method assings default form classes
        to plugin form stacks
        """
        # default form class for add_form stack
        self._add_form_stack = self._add_treenode_form_class
        # default form class for edit_form stack
        self._edit_form_stack = self._edit_treenode_form_class

    def _add_form(self, *args, **kw):
        """
        Create and return add form instance.
        Before we got prepared class through
        'modify_additem_form' function.
        This function adds 'names' fields to the main form
        """
        f = self._add_form_stack(*args, **kw)
        return f

    def _edit_form(self, *args, **kw):
        """
        Create and return edit form instance.
        Before we got prepared class through
        'modify_edititem_form' function.
        This function adds 'names' fields to the main form
        """
        f = self._edit_form_stack(*args, **kw)
        return f

    def get_langs(self):
        try:
            class_obj = rootdb.Language
            self.langs = Q(class_obj)\
                .filter(class_obj.active == True)\
                .order_by('deflang')
        except:
            transaction.abort()
        return self.langs

    def get_treenodes(self):
        """ Get treenodes """
        class_obj = rootsdb.TreeNode
        self.treenodes = Q(class_obj)\
            .filter(class_obj.parent_id == None)\
            .order_by('rank')
        return [self.treenodes.filter_by(admin_node=True),\
                self.treenodes.filter_by(admin_node=False)]


admin.admin._plugins.append(Tree)
