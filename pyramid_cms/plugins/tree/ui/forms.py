# -*- coding: utf-8 -*-

"""
Forms for plugin Tree
"""
import time

from wtforms.fields import (
    DateTimeField,
    SelectMultipleField,
    SelectField,
    FormField,
    FieldList,
    FileField,
    HiddenField,
    TextField,
    TextAreaField
    )
from wtforms import widgets
from wtforms import validators
from wtforms import form
from wtforms_sqlalchemy.fields import (
    QuerySelectField,
    QuerySelectMultipleField
)

from wtforms_alchemy import ModelForm

from pyramid_cms.assets.db.sqlalchemy.connection import DBSession, Q
from pyramid_cms.assets.forms import UIForm
from pyramid_cms.assets.forms import FormInit
from pyramid_cms.apps.roots import dbmodels as rootsdb
from pyramid_cms.apps.auth import dbmodels as authdb


class TitleForm(ModelForm):
    """
    Additional form to create node titles
    """
    class Meta:
        model = rootsdb.PageTitle
        exclude = ['active']
    lang_id = HiddenField()

class DescrForm(ModelForm):
    """
    Additional form to create node titles
    """
    class Meta:
        model = rootsdb.PageDescription
        exclude = ['active']
    lang_id = HiddenField()

class TextForm(ModelForm):
    """
    Additional form to create node titles
    """
    class Meta:
        model = rootsdb.PageText
        exclude = ['active']
    lang_id = HiddenField()


class TreeAddItemForm(UIForm):
    """
    Add Tree item form
    """
    exclude = (
        'id',
        'created',
        'updated',
        'site_id',
        'site',
        'pagetype_id',
        'pagetype',
        'parent_id',
        'parent',
        'sibling_id',
        'sibling',
        'redirect_id',
        'redirect',
        'children',
        'permissions',
        'titles',
        'descriptions',
        'texts',
        'tags'
    )

    class Meta:
        model = rootsdb.TreeNode

    site = QuerySelectField()
    pagetype = QuerySelectField()
    #parent = QuerySelectField(allow_blank=True, blank_text='----')
    parent = HiddenField()
    redirect = QuerySelectField(allow_blank=True, blank_text='----')
    tags = QuerySelectMultipleField(allow_blank=True, blank_text='----')
    kwords = QuerySelectMultipleField()

    def validate_and_add(self):
        """ Validate and add item """
        columns = [c for c in self.Meta.model.columns_names()
                   if not c in self.exclude]
        if self.validate():
            obj = rootsdb.TreeNode()
            obj.titles = []
            obj.descriptions = []
            obj.texts = []
            obj.tags = []
            obj.kwords = []
            for attr in columns:
                setattr(obj,attr, getattr(self, attr).data)

            for t in self.titles:
                # titles
                title = rootsdb.PageTitle()
                title.lang_id = t.lang_id.data
                title.content = t.content.data
                obj.titles.append(title)
            for d in self.descriptions:
                # descriptions
                description = rootsdb.PageDescription()
                description.lang_id = d.lang_id.data
                description.content = d.content.data
                obj.descriptions.append(description)
            for t in self.texts:
                # texts
                text = rootsdb.PageText()
                text.lang_id = t.lang_id.data
                text.content = t.content.data
                obj.texts.append(text)
            # inner_name
            obj.inner_name = self.titles[0].content.data
            # site
            obj.site = self.site.data
            # pagetype
            obj.pagetype = self.pagetype.data
            
            # Мнемо-назва розділу
            """
            Мнемо-назву розділу формуємо на основі поточного часового зрізу
            у випадку, коли мнемо-назву для розділу не зазначено.
            """
            if self.mnemo.data in ('', False, None):
                obj.mnemo = str(int(time.time()))
            # parent
            if self.parent.data not in (0,None,'None'):
                obj.parent_id = self.parent.data
            # redirect
            if self.redirect.data not in (0, None, 'None'):
                obj.redirect_id = self.redirect.data.id
            else:
                obj.redirect_id = None
            # permissions
            obj.permissions = [i for i in Q(authdb.Permission)\
                .filter(authdb.Permission.name == 'view')]
            if self.tags.data not in (0, None, 'None'):
                obj.tags = self.tags.data
            if self.kwords.data not in (0, None, 'None'):
                obj.kwords = self.kwords.data
            obj.save()
            return True
        return False


def modify_additem_form(entries):
    """ Append additional props to add item form """
    class_obj = TreeAddItemForm
    setattr(class_obj,
            'titles',
            FieldList(FormField(TitleForm),
                      min_entries=entries))
    setattr(class_obj,
            'descriptions',
            FieldList(FormField(DescrForm),
                      min_entries=entries))
    setattr(class_obj,
            'texts',
            FieldList(FormField(TextForm),
                      min_entries=entries))
    return class_obj

class TreeAddItemFormInit(FormInit):
    """ Form init for add item form """
    def set_form_vars(self):
        self.f.site.query = Q(rootsdb.Site).all()
        self.f.pagetype.query = Q(rootsdb.PageType).all()
        self.f.redirect.query = \
            Q(rootsdb.TreeNode)\
            .filter(rootsdb.TreeNode.admin_node == False)
        self.f.parent.query = \
            Q(rootsdb.TreeNode).all()
        self.f.tags.query = \
            Q(rootsdb.TreeNodeTag).all()
        self.f.kwords.query = \
            Q(rootsdb.TreeNodeKword)
        #self.f.permissions.query = Q(authdb.Permission)


def get_sites():
    return Q(rootsdb.Site).all()

class TreeEditItemForm(UIForm):
    """ Edit treenode form  """
    exclude = (
        'id',
        #'created',
        'updated',
        'site_id',
        'site',
        'pagetype_id',
        'pagetype',
        'parent_id',
        'parent',
        'sibling_id',
        'sibling',
        'redirect_id',
        'redirect',
        'children',
        'permissions',
        'titles',
        'descriptions',
        'texts'
    )
    errors_tracker = None

    class Meta:
        model = rootsdb.TreeNode

    site = QuerySelectField(allow_blank=True, blank_text='----')
    pagetype = QuerySelectField(allow_blank=True, blank_text='----')
    parent = QuerySelectField(allow_blank=True, blank_text='----')
    redirect = QuerySelectField(allow_blank=True, blank_text='----')
    tags = QuerySelectMultipleField()
    kwords = QuerySelectMultipleField()
    created = DateTimeField()

    def validate_and_edit(self):
        """ Validate and change item """
        columns = [c for c in self.Meta.model.columns_names()
                   if not c in self.exclude]
        if self.validate():
            obj = self._obj
            for attr in columns:
                setattr(obj,attr, getattr(self, attr).data)

            for t in self.titles:
                # titles
                #try:
                title = obj.titles\
                           .filter_by(lang_id=t.lang_id.data)\
                           .first()
                #except:
                if not title:
                    title = rootsdb.PageTitle()
                title.lang_id = t.lang_id.data
                title.content = t.content.data
                obj.titles.append(title)
            for d in self.descriptions:
                # descriptions
                #try:
                description = obj.descriptions\
                                 .filter_by(lang_id=d.lang_id.data)\
                                 .first()
                #except:
                if not description:
                    description = rootsdb.PageDescription()
                description.lang_id = d.lang_id.data
                description.content = d.content.data
                obj.descriptions.append(description)
            for t in self.texts:
                # texts
                #try:
                text = obj.texts\
                          .filter_by(lang_id=t.lang_id.data)\
                          .first()
                #except:
                if not text:
                    text = rootsdb.PageText()
                text.lang_id = t.lang_id.data
                text.content = t.content.data
                obj.texts.append(text)

            # inner_name
            obj.inner_name = self.titles[0].content.data
            # site
            obj.site = self.site.data
            # pagetype
            obj.pagetype = self.pagetype.data
            # Мнемо-назва розділу
            """
            Мнемо-назву розділу формуємо на основі поточного часового зрізу
            у випадку, коли мнемо-назву для розділу не зазначено.
            """
            if self.mnemo.data in ('', False, None):
                obj.mnemo = str(int(time.time()))
            # parent
            if self.parent.data not in (0,None,'None'):
                obj.parent_id = self.parent.data.id
            # redirect
            if self.redirect.data not in (0, None, 'None'):
                obj.redirect_id = self.redirect.data.id
            else:
                obj.redirect_id = None
            # permissions
            obj.permissions = [i for i in Q(authdb.Permission)\
                .filter(authdb.Permission.name == 'view')]
            obj.tags = self.tags.data
            obj.kwords = self.kwords.data
            # created data
            obj.created = self.created.data
            obj.save()
            return True
        return False

def modify_edititem_form(entries):
    """ Append additional props to edit item form """
    class_obj = TreeEditItemForm
    setattr(class_obj,
            'titles',
            FieldList(FormField(TitleForm),
                      min_entries=entries))
    setattr(class_obj,
            'descriptions',
            FieldList(FormField(DescrForm),
                      min_entries=entries))
    setattr(class_obj,
            'texts',
            FieldList(FormField(TextForm),
                      min_entries=entries))
    return class_obj


class TreeEditItemFormInit(FormInit):
    """ Form init for edit item form """

    def set_form_vars(self):
        self.f.site.query = Q(rootsdb.Site).all()
        self.f.pagetype.query = Q(rootsdb.PageType).all()
        self.f.redirect.query = \
            Q(rootsdb.TreeNode)\
            .filter(rootsdb.TreeNode.admin_node == False).all()
        self.f.parent.query = \
            Q(rootsdb.TreeNode)\
            .filter(rootsdb.TreeNode.admin_node == False).all()
        self.f.tags.query = \
            Q(rootsdb.TreeNodeTag).all()
        self.f.kwords.query = \
            Q(rootsdb.TreeNodeKword).all()
        #self.f.permissions.query = [i for i in Q(authdb.Permission).all()]
