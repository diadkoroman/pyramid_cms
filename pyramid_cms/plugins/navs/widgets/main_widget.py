# -*- coding: utf-8 -*-
from pyramid.httpexceptions import HTTPFound
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import DBSession, Q
from pyramid_cms.assets.widgets import AdminWidget


"""
m = variable for request.GET['m'] (mode)
s = variable for request.GET['s'] (slot)
c = variable for request.GET['c'] (category)
i = variable for request.GET['i'] (item)
"""
class Navs(AdminWidget):
    """
    Multiwidget to display Navs plugin
    """
    def dispatch(self):
        """ Dispatcher method """
        self.wdata['path'] = None
        self.wdata['template'] = None
        self.wdata['slots'] = {}
        self.wdata['add_form'] = {}
        self.wdata['edit_form'] = {}
        self.wdata['delete_form'] = {}
        if getattr(self.kw['current'], 'template', False):
            curr = self.kw['current']
            self.wdata['path'] = curr.get_plugin_path(self.req)
            self.wdata['template'] = curr.get_template()
            slots = curr.get_slots()
            for slot in slots:
                slot_dict = self.wdata['slots'][slot.mnemo] = {}
                slot_dict['name'] = slot.inner_name
                slot_dict['id'] = slot.id
                slot_dict['data'] = [i for i in slot.items.filter_by(parent=None)]
        if 'add' in self.req.GET:
            self._add_form_widget()
        if 'i' in self.req.GET \
           and (not 'add' in self.req.GET) \
           and (not 'delete' in self.req.GET):
            self._edit_form_widget()
        if 'delete' in self.req.GET:
            self._delete_form_widget()

    def _add_form_widget(self):
        """ Add-item-form generator """
        def _data_for_get():
            form = self.kw['current']\
                .ui(
                    meta={'csrf_context':self.req.session},
                    req=self.req.GET)
            return form
        

        def _data_for_post():
            form = self.kw['current']\
                       .ui(self.req.POST,
                           meta={'csrf_context':self.req.session},
                           req=self.req.GET)
            #option = req.POST.get('option','stay')
            if form.validate_and_add():
                redir_to = self.kw['current'].get_plugin_path(self.req)
                #raise Exception(redir_to)
                raise HTTPFound(location=redir_to)
            return form

        if self.req.method == 'GET':
            form = _data_for_get()
        elif self.req.method == 'POST':
            form = _data_for_post()

        self.wdata['add_form']['langs'] = self.kw['current'].get_langs()
        self.wdata['add_form']['name'] = 'Add navigator item'
        self.wdata['add_form']['body'] = form

    def _edit_form_widget(self):
        """ Edit-item-form generator """
        try:
            item = int(self.req.GET.get('i'))
            i = self.kw['current'].get(item)
        except:
            item = None
            i = None
        def _data_for_get(i):
            if i:
                form = self.kw['current']\
                           .ui(
                               meta={'csrf_context':self.req.session},
                               obj=i,
                               req=self.req.GET)
            else:
                form = self.kw['current']\
                           .ui(
                               meta={'csrf_context':self.req.session},
                               req=self.req.GET)
            return form

        def _data_for_post(instance):
            if i:
                form = self.kw['current']\
                           .ui(
                               self.req.POST,
                               meta={'csrf_context':self.req.session},
                               obj=i,
                               req=self.req.GET)
                option = self.req.POST.get('option','stay')
                if form.validate_and_edit():
                    redir_to = self.kw['current'].get_plugin_path(self.req)
                    #raise Exception(redir_to)
                    raise HTTPFound(location=redir_to)
            form = self.kw['current']\
                       .ui(
                           self.req.POST,
                           meta={'csrf_context':self.req.session},
                           req=self.req.GET)
            return form

        if self.req.method == 'GET':
            form = _data_for_get(i)
        elif self.req.method == 'POST':
            form = _data_for_post(i)

        self.wdata['edit_form']['langs'] = self.kw['current'].get_langs()
        self.wdata['edit_form']['name'] = 'Edit navigator item'
        self.wdata['edit_form']['body'] = form

    def _delete_form_widget(self):
        """ Delete item form """
        # Get value for variable
        delete = self.req.GET.get('delete', None)
        path = self.kw['node'].get_path()
        self.wdata['delete_form']['name'] = ''
        self.wdata['delete_form']['back_url'] = ''
        self.wdata['delete_form']['frw_url'] = ''
        self.wdata['delete_form']['childobj'] = None
        # Getting item to delete
        try:
            slot = self.req.GET.get('s')
            item = self.req.GET.get('i')
            i = self.kw['current'].get(int(item))
            self.wdata['delete_form']['name'] = \
                                                getattr(i, 'inner_name', 'noname')
            self.wdata['delete_form']['baseurl'] = '{0}?s={1}&i={2}'\
                .format(path, slot, item)
            self.wdata['delete_form']['back_url'] = path
            self.wdata['delete_form']['frw_url'] = '{0}?s={1}&i={2}'\
                .format(path, slot, item)
        except:
            item = None
            i = None
        if delete and delete == 'y':
            if i:
                i.delete()
                raise HTTPFound(location=path)
admin.admin.widgets.append(Navs)

'''
def navs_widget(req, data, **kw):
    """ Multiwidget for navs plugin. """
    data['navs'] = {}
    data['navs']['path'] = None
    data['navs']['template'] = None
    data['navs']['slots'] = {}
    data['navs']['add_form'] = {}
    data['navs']['edit_form'] = {}
    data['navs']['delete_form'] = {}
    if getattr(kw['current'], 'template', False):
        curr = kw['current']
        data['navs']['template'] = curr.get_template()
        data['navs']['path'] = curr.get_plugin_path(req)
        slots = curr.get_slots()
        for slot in slots:
            slot_dict = data['navs']['slots'][slot.mnemo] = {}
            slot_dict['name'] = slot.inner_name
            slot_dict['id'] = slot.id
            slot_dict['data'] = [i for i in slot.items.filter_by(parent=None)]
            
        # inner call widgets in parent widget
        if 'add' in req.GET:
            add_form_widget(req, data['navs']['add_form'], **kw)
        if 'i' in req.GET and (not 'add' in req.GET) and (not 'delete' in req.GET):
            edit_form_widget(req, data['navs']['edit_form'], **kw)
        if 'delete' in req.GET:
            delete_form_widget(req, data['navs']['delete_form'], **kw)

admin.admin.widgets.append(navs_widget)
'''
'''
def add_form_widget(req, data, **kw):
    def _data_for_get():
        if 'add' in req.GET:
            form = kw['current'].ui(meta={'csrf_context':req.session},
                req=req.GET)
            return form
        return None

    def _data_for_post():
        if 'add' in req.GET:
            form = kw['current'].ui(req.POST, meta={'csrf_context':req.session},
                req=req.GET)
            option = req.POST.get('option','stay')
            form.validate_and_add()
            redir_to = kw['current'].get_plugin_path(req)
                #raise Exception(redir_to)
            raise HTTPFound(location=redir_to)
            return form
        return None
    
    if req.method == 'GET':
        form = _data_for_get()
    elif req.method == 'POST':
        form = _data_for_post()
    #form = getattr(add_form_widget, '_data_for_{0}'.format(req.method.lower()))
    data['langs'] = kw['current'].get_langs()
    data['name'] = 'Add navigator item'
    data['body'] = form
'''
'''
def edit_form_widget(req, data, **kw):
    # request for update instance
    try:
        item = int(req.GET.get('i'))
        instance = kw['current'].get(item)
    except:
        item = None
        instance = None
        
    def _data_for_get(instance):
        if instance:
            form = kw['current'].ui(meta={'csrf_context':req.session},
                obj=instance,
                req=req.GET)
        else:
            form = kw['current'].ui(meta={'csrf_context':req.session},
                req=req.GET)
        return form

    def _data_for_post(instance):
        if instance:
            form = kw['current'].ui(req.POST, meta={'csrf_context':req.session},
                obj=instance,
                req=req.GET)
            option = req.POST.get('option','stay')
            form.validate_and_edit()
            redir_to = kw['current'].get_plugin_path(req)
                #raise Exception(redir_to)
            raise HTTPFound(location=redir_to)
        form = kw['current'].ui(req.POST, meta={'csrf_context':req.session},
            req=req.GET)
        return form

    if req.method == 'GET':
        form = _data_for_get(instance)
    elif req.method == 'POST':
        form = _data_for_post(instance)
    data['langs'] = kw['current'].get_langs()
    data['name'] = 'Edit navigator item'
    data['body'] = form
'''    
'''    
def delete_form_widget(req, data, **kw):
    """ Delete item form """
    # Get value for variable
    delete = req.GET.get('delete', None)
    path = kw['node'].get_path()
    data['name'] = ''
    data['back_url'] = ''
    data['frw_url'] = ''
    data['childobj'] = None
    # Getting item to delete
    try:
        slot = req.GET.get('s')
        item = req.GET.get('i')
        instance = kw['current'].get(int(item))
        data['name'] = getattr(instance,'inner_name', 'noname')
        data['baseurl'] = '{0}?s={1}&i={2}'.format(path, slot, item)
        data['back_url'] = path
        data['frw_url'] = '{0}?s={1}&i={2}'.format(path, slot, item)
    except:
        item = None
        instance = None
    if delete and delete == 'y':
        if instance:
            instance.delete()
            raise HTTPFound(location=path)
'''
