# -*- coding: utf-8 -*-
"""
Forms for plugin navs.
"""
import transaction
from wtforms.fields import (
    SelectMultipleField,
    SelectField,
    FormField,
    FieldList,
    FileField,
    HiddenField
    )
from wtforms import widgets
from wtforms import validators
from wtforms import form
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField

from wtforms_alchemy import ModelForm

from pyramid_cms.assets.db.sqlalchemy.connection import DBSession, Q
from pyramid_cms.assets.forms import UIForm
from pyramid_cms.assets.forms import FormInit
from pyramid_cms.apps.navs import dbmodels as navsdb
from pyramid_cms.apps.roots import dbmodels as rootsdb


##############################################################################
class NameForm(ModelForm):
    class Meta:
        model = navsdb.NavItemName
        exclude = ['active']
    lang_id = HiddenField()
    imgfile = FileField()
##############################################################################

##############################################################################
class NavsAddItemForm(UIForm):
    """ Add navigator item form """
    exclude = (
                'id',
                'created',
                'updated',
                'site_id',
                'node_id',
                'navslot_id',
                'parent_id',
                'navslot',
                'categs',
                'node',
                'parent',
                'children',
                )
    class Meta:
        model = navsdb.NavItem
    navslot_id = HiddenField()
    node = QuerySelectField(allow_blank=True, blank_text='----')
    parent_id = HiddenField()
    #parent = QuerySelectField(allow_blank=True, blank_text='----')

    def validate_and_add(self):
        columns = [c for c in self.Meta.model.columns_names()
                   if not c in self.exclude]
        if self.validate():
            obj = navsdb.NavItem()
            obj.names = []
            for attr in columns:
                setattr(obj,attr, getattr(self, attr).data)
            for e in self.names:
                name = navsdb.NavItemName()
                name.lang_id = e.lang_id.data
                name.content = e.content.data
                obj.names.append(name)
            obj.navslot_id = self.navslot_id.data
            obj.node = self.node.data
            if self.parent_id.data not in (0, None, 'None'):
                obj.parent_id = self.parent_id.data
            #obj.parent = self.parent.data
            obj.save()
            return True
        return False


def modify_additem_form(entries):
    """ Append additional props to add item form """
    class_obj = NavsAddItemForm
    setattr(class_obj, 'names', FieldList(FormField(NameForm), min_entries=entries))
    return class_obj

class NavsAddItemFormInit(FormInit):
    """ Initializer for add nav item form """
    def set_form_vars(self):
        self.f.node.query = \
            Q(rootsdb.TreeNode)\
            .filter_by(admin_node=False)\
            .order_by('inner_name')
##############################################################################


##############################################################################
class NavsEditItemForm(UIForm):
    """ Edit navigator item form """
    exclude = (
                'id',
                'created',
                'updated',
                'site_id',
                'node_id',
                'navslot_id',
                'parent_id',
                'navslot',
                'categs',
                'node',
                'parent',
                'children',
                )
    class Meta:
        model = navsdb.NavItem
    navslot_id = HiddenField()
    node = QuerySelectField(allow_blank=True, blank_text='----')
    parent = QuerySelectField(allow_blank=True, blank_text='----')

    def validate_and_edit(self):
        columns = [c for c in self.Meta.model.columns_names() if not c in self.exclude]
        if self.validate():
            obj = self._obj
            #obj.names = []
            for attr in columns:
                setattr(obj,attr, getattr(self, attr).data)
            for e in self.names:
                try:
                    name = obj.names.filter_by(lang_id=e.lang_id.data).first()
                except:
                    name = navsdb.NavItemName()
                    name.item_id = obj.id
                name.lang_id = e.lang_id.data
                name.content = e.content.data
                obj.names.append(name)
            obj.parent = self.parent.data
            obj.node = self.node.data
            obj.save()
            return True
        return False

def modify_edititem_form(entries):
    """ Append additional props to edit item form """
    class_obj = NavsEditItemForm
    setattr(class_obj, 'names', FieldList(FormField(NameForm), min_entries=entries))
    return class_obj

class NavsEditItemFormInit(FormInit):
    """ Initializer for edit nav item form """
    def set_form_vars(self):
        self.f.parent.query = Q(navsdb.NavItem)
        self.f.node.query = \
            Q(rootsdb.TreeNode)\
            .filter_by(admin_node=False)\
            .order_by('inner_name')
##############################################################################
