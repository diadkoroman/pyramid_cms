# -*- coding: utf-8 -*-
"""
Module with Plugin main class
"""
#import transaction
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.plugins import Plugin

from pyramid_cms.apps.navs import dbmodels as navsdb
from pyramid_cms.apps.roots import dbmodels as rootdb

from .ui import forms

def langs_count():
    """ Function for counting langs """
    lc = 0
    try:
        lc = Plugin.count_langs(langs=Plugin.get_langs())
    except:
        pass
    return lc

class Navs(Plugin):
    """ Plugin for operations with Navigator objects """
    langs = Plugin.get_langs()
    langs_count = langs_count()
    
    # form to add navitem instance
    _add_navitem_form_class = forms.NavsAddItemFormInit(
        forms.modify_additem_form(langs_count))
    # form to edit  navitem instance
    _edit_navitem_form_class = forms.NavsEditItemFormInit(
        forms.modify_edititem_form(langs_count))
    _default_class = navsdb.NavItem

    packagename = admin.admin.config.packagename
    template = "plugins/navs/templates/navs_widget.pt"

    def __init__(self, section_class=None, parent=None):
        self.inner_name = "Navigation Manager"
        self.mnemo = "navs"
        self.widgets = [self.mnemo]
        super().__init__(section_class=section_class, parent=parent)

    def _load_default_forms(self):
        """
        Method assings default form classes
        to plugin form stacks
        """
        # default form class for add_form stack
        self._add_form_stack = self._add_navitem_form_class
        # default form class for edit_form stack
        self._edit_form_stack = self._edit_navitem_form_class
        
    
    def _add_form(self, *args, **kw):
        """
        Create and return add form instance.
        Before we got prepared class through
        'modify_additem_form' function.
        This function adds 'names' fields to the main form
        """
        f = self._add_form_stack(*args, **kw)
        return f

    def _edit_form(self, *args, **kw):
        """
        Create and return edit form instance.
        Before we got prepared class through
        'modify_edititem_form' function.
        This function adds 'names' fields to the main form
        """
        f = self._edit_form_stack(*args, **kw)
        return f
    
    def get_langs(self):
        try:
            class_obj = rootdb.Language
            self.langs = Q(class_obj)\
                .filter(class_obj.active == True)\
                .order_by('deflang').all()
        except:
            pass
        return self.langs
    

    def get_slots(self):
        class_obj = navsdb.NavSlot
        slots = Q(class_obj)\
            .filter(class_obj.active == True)\
            .order_by('rank')
        return slots


admin.admin._plugins.append(Navs)
