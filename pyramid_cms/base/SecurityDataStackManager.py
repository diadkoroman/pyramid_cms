# -*- coding: utf-8 -*-

class SecurityDataStackManager(object):
    inst = None
    def __new__(cls):
        if cls.inst is None:
            cls.inst = SecurityDataStackManager.__SecurityDataStackManager()
        return cls.inst
    # при запиті атрибута клас віддає inst.<attr>
    def __getattr__(self, attr_name):
        return getattr(self.inst, attr_name)

    # при призначенні атрибута клас призначає його в inst.<attr>
    def __setattr__(self, attr_name,value):
        return self.inst.__setattr__(attr_name, value)

    class __SecurityDataStackManager(object):
        def __init__(self):
            self.users = {}
            self.groups = {}