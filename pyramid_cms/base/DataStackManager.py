# -*- coding: utf-8 -*-

class DataStackManager(object):
    """ Class for managing stacks """
    def __init__(self, **kw):
        self.data = []
        self.allowed_datatypes = list(kw.get("allowed_datatypes", []))
        self.max_length = int(kw.get("max_length", 100))
    
    @property
    def all(self):
        return self.data

    def sorted(self,sort_mode=None):
        """
        Method for stack sorting
        """
        if sort_mode:
            try:
                res = sorted(self.data, key=lambda i:i.__name__)
            except AttributeError:
                res = sorted(self.data, key=lambda i:i.__class__.__name__)
            except:
                res = self.data
            if sort_mode == 'desc':
                res.reverse()
            return res
        return self.all
        
    def append(self, i):
        """ Adding data to stack. """
        self.data.append(i)
    
    
    def remove(self, name):
        """ Delete data from stack. """
        for i in self.data:
            if name == i.__name__ or name == i.__name__.lower():
                self.data.remove(i)
    
    
    def get(self, name):
        """ Get data from stack """
        for i in self.data:
            try:
                if name in (i.__name__, i.__name__.lower()):
                    return i
            except:
                if name in (i.__class__.__name__, i.__class__.__name__.lower()):
                    return i
        return False
        
    def in_stack(self, i):
        """ Check if i in stack. """
        return True if i in self.data else False
