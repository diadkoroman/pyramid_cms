# -*- coding: utf-8 -*-
from .ConfigStorage import ConfigStorage
from .DataStackManager import DataStackManager
from .SecurityDataStackManager import SecurityDataStackManager


class Admin(object):
    inst = None

    def __new__(cls):
        if cls.inst is None:
            cls.inst = Admin.__Admin()
        return cls.inst

    # при запиті атрибута клас віддає inst.<attr>
    def __getattr__(self, attr_name):
        return getattr(self.inst, attr_name)

    # при призначенні атрибута клас призначає його в inst.<attr>
    def __setattr__(self, attr_name, value):
        return self.inst.__setattr__(attr_name, value)

    class __Admin(object):
        # sections allowed for rewriting by user
        _allowed_sections = ['config']
        # setting forbidden for rewriting by user
        _forbidden_settings = []
        def __init__(self):
            """ Initialization of inner class """
            # admin common stack
            self.admin = ConfigStorage()
            # admin.config data
            self.admin.config = ConfigStorage()
            # content_types
            self.admin.content_types = DataStackManager()
            # field tables
            self.admin.fields = DataStackManager()
            # field types
            self.admin.field_types = DataStackManager()
            # plugins
            # stack for plugin classes(unregistered)
            self.admin._plugins = DataStackManager()
            # stack for plugin instances(registered)
            self.admin.plugins = DataStackManager()
            # viewtypes
            self.admin.viewtypes = DataStackManager()
            # widgets
            self.admin.widgets = DataStackManager()
            
            # project common stack
            self.project = ConfigStorage()
            # project config data
            self.project.config = ConfigStorage()
            #
            # viewtypes
            self.project.viewtypes = DataStackManager()
            # widgets
            self.project.widgets = DataStackManager()

            # security interface
            self.security = SecurityDataStackManager()

        def create_field_types(self, stack):

            """
            Method to create field types.
            It can be predefined and user-defined values.
            *stack is a dict with initialization data
            """
            
            from pyramid_cms.assets.db.sqlalchemy.connection import Q
            from pyramid_cms.dbmodels import FieldType

        def rewrite_setting(self, settings_section=None, setting_name=None, value=None):
            """ Rewrite admin settings values """
            if settings_section and setting_name:
                if settings_section in self._allowed_sections:
                    if setting_name not in self._forbidden_settings:
                        try:
                            setattr(getattr(self.admin,settings_section), setting_name, value)
                        except:
                            pass
