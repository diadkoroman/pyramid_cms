# -*- coding: utf-8 -*-

"""
Request/Response subscribers
"""

from pyramid.events import subscriber
from pyramid.events import NewRequest, NewResponse

@subscriber(NewResponse)
def add_x_frame_options(event):
    resp = event.response
    resp.headers.add('X-Frame-Options', 'DENY')