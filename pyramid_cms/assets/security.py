# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import DBSession

USERS = {}
GROUPS = {}


def groupfinder(userid, request):
    if userid in admin.security.users:
        return admin.security.groups.get(userid, [])

def cleanup(event):
    def cleanup_dbsess(request):
        DBSession.remove()
    event.request.add_finished_callback(cleanup_dbsess)
