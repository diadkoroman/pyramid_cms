# -*- coding: utf-8 -*-

# SQLALCHEMY IMPORTS
from sqlalchemy import (
    Column,
    Boolean,
    ForeignKey,
    Integer,
    String,
    DateTime,
    Text)
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.expression import asc,desc
from sqlalchemy.orm import joinedload,subqueryload
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship
from .Model import Model



class LiteColumnsMixin(Model):
    id = Column(Integer,primary_key=True)
    created = Column(DateTime, default=func.now())
    updated = Column(DateTime, default=func.now(), onupdate=func.now())
    active = Column(Boolean, default=True)

    def __str__(self):
        try:
            return self.inner_name
        except:
            return str(self.id)

class StandardColumnsMixin(LiteColumnsMixin):
    inner_name = Column('inner_name',String(150),nullable=True)

class RankedColumnsMixin(StandardColumnsMixin):
    rank = Column(Integer, default=0)

class LocalizedStandardNameMixin(StandardColumnsMixin):
    @declared_attr
    def ukr_name(cls):
        colname = "ukr_name"
        return Column(colname,String(150),nullable = True)
        
    @declared_attr
    def rus_name(cls):
        colname = "rus_name"
        return Column(colname,String(150),nullable = True)
        
    def localized(self,lang_mnemo):
        return getattr(self,"{0}_name".format(lang_mnemo))
    
    
class LocalizedStandardTextMixin(StandardColumnsMixin):
    @declared_attr
    def ukr_text(cls):
        colname = "ukr_text"
        return Column(colname,Text,nullable = True)
        
    @declared_attr
    def rus_text(cls):
        colname = "rus_text"
        return Column(colname,Text,nullable = True)
        
    def localized(self,lang_mnemo):
        return getattr(self,"{0}_text".format(lang_mnemo))
    
    
class LocalizedStandardStringMixin(StandardColumnsMixin):
    @declared_attr
    def ukr_string(cls):
        colname = "ukr_string"
        return Column(colname,String(150),nullable = True)
        
    @declared_attr
    def rus_string(cls):
        colname = "rus_string"
        return Column(colname,String(150),nullable = True)
        
    def localized(self,lang_mnemo):
        return getattr(self,"{0}_string".format(lang_mnemo))
    
    
class LocalizedRankedMixin(RankedColumnsMixin):
    @declared_attr
    def ukr_name(cls):
        colname = "ukr_name"
        return Column(colname,String(150),nullable = True)
        
    @declared_attr
    def rus_name(cls):
        colname = "rus_name"
        return Column(colname,String(150),nullable = True)
        
    def localized(self,lang_mnemo):
        return getattr(self,"{0}_name".format(lang_mnemo))
        
'''
Базові рядкові слоти для виводу текстових даних у обмеженій кількості
'''
class TinyStringSlot(LiteColumnsMixin):
    @declared_attr
    def content(cls):
        return Column(String(10),nullable = True)
        
class SmallStringSlot(LiteColumnsMixin):
    @declared_attr
    def content(cls):
        return Column(String(50),nullable = True)
        
class StringSlot(LiteColumnsMixin):
    @declared_attr
    def content(cls):
        return Column(String(150),nullable = True)
        
class LongStringSlot(LiteColumnsMixin):
    @declared_attr
    def content(cls):
        return Column(String(250),nullable = True)
        
'''
Базовий текстовий слот для виводу текстових даних
'''
class TextSlot(LiteColumnsMixin):
    @declared_attr
    def content(cls):
        return Column(Text,nullable = True)
