# -*- coding: utf-8 -*-

'''
Файл, що постачає усілякі корисні фічі для моделей БД, наприклад: стандартні
функції і методи, мікс-іни та, можливо, якісь кастомні поля.
'''

# з конфігу імпортуємо всякі налаштування
from pyramid_cms.base import admin
from pyramid_cms.config import settings


#################### ФУНКЦІЇ ################################################
def tablename_(tname,app = False):
    '''
    функція, що створює назву для таблиці БД,
    використовуючи префікс.'''
    # якщо назву таблиці не зазначено - функція матюкається. Якщо зазначено -
    # функція повертає назву таблиці у форматі <префікс>_<назва таблиці>
    if tname:
        if not tname is '':
            return '{0}_{1}'.format(
                admin.admin.config.packagename if not app else str(app),tname)
    raise Exception('"tname" argument empty or not set!')


def descr(**kw):
    '''
    функція, що формує опис для таблиці, беручи аргументами
    kwargs із індексами мовних версій
    '''
    # функція максимально універсальна - ми беремо усі **kw і вносимо у єдиний
    # dict як його елементи. Цей dict потім стає основою для атрибута таблиці БД
    dsc = {}
    for k,v in kw.items():
        dsc[k] = v
    return dsc


def fk_(tname,fk = False,app = False):
    '''
    функція, що формує рядок із зазначенням назви таблиці і колонки,
    що буде ключем для foreign key.tname - назва таблиці(фрагмент)
    .fk - назва колонки для ключа
    '''
    # назву таблиці не визначено - функція матюкається. Якщо визначено -
    # спочатку локально запускається tablename і формує назву таблиці, а потім
    # сама виконує решту роботи
    if tname:
        if not tname is '':
            t = tablename_(tname,app=app)
            return '{0}.{1}'.format(t,'id' if not fk else fk)
    raise Exception('"tname" argument empty or not set!')


# Створюємо функцію внесення foreign key, актуальну для поточного застосунку
#app_fk_ = lambda tn: fk_(tn,app=admin.project.packagename)


# Створюємо локальну версію функції tablename
#app_tablename_ = lambda tn: tablename(tn,app=admin.project.packagename)


##############################################################################
