# -*- coding: utf-8 -*-

import os,time,transaction
from pyramid.httpexceptions import HTTPFound
from wtforms_alchemy import ModelForm, model_form_factory
from ..connection import DBSession, Base, Metadata, Q
from pyramid_cms.assets.forms import ProtectedForm

class Model(object):

    '''
    Назва моделі у однині й множині.
    Використовується для відображення у адміністративному інтерфейсі
    '''
    verbose_name = 'назва моделі'
    verbose_name_plural = 'назва моделі(мн.)'
    
    '''
    Перелік колонок таблиці БД, яку описує модель.
    Використовується методом Model.table
    Якщо параметр не заповнено - виводитися будуть усі колонки.
    '''
    columns_listing = None
    
    '''
    Аліаси для назв колонок.
    Використовується для заміни оригінальних назв колонок в таблиці БД на аліаси.
    Якщо параметр не заповнено - виводитися будуть оригінальні назви колонок.
    '''
    columns_aliases = {
        'created':'створено',
        'updated':'оновлено'
            }
            
    '''
    Перелік колонок, яких не повинно бути у виводі.
    Використовується методом Model.exclude_columns.
    Якщо значення параметра False - фільтрація не відбувається.
    '''
    exclude = False
    
    '''
    Параметр, що активує поле пошуку при відображенні лістингу таблиці (True),
    а також активує метод search для моделі.
    '''
    search_field = False
    
    # опція додавання елементів(дозволено за умовч.)
    add_option = True
    # опція редагування елементів
    edit_option = True
    # опція видалення елементів
    delete_option = True
    
    #query = DBSession.query

    # вивід назв колонок
    @classmethod
    def columns_names(cls):
        colnames = False
        tablename = cls.__tablename__
        try:
            colnames = []
            if not cls.columns_listing is None:
                if isinstance(cls.columns_listing,(tuple,list)):
                    for column in Metadata.tables[tablename].columns:
                        if column.name in cls.columns_listing:
                            colnames.append(column.name)
            else:
                for column in Metadata.tables[tablename].columns:
                    colnames.append(column.name)
        except:
            pass
        return colnames

    # вивід назв колонок з можливістю зазначати аліаси
    @classmethod
    def columns_names_aliased(cls,colnames):
        colnames_aliased = []
        # якщо зазначено аліаси для колонок
        if cls.columns_aliases and isinstance(cls.columns_aliases,(dict,)):
            akeys = cls.columns_aliases.keys()
            for colname in colnames:
                if colname in akeys:
                    colnames_aliased.append(cls.columns_aliases.get(colname,colname))
                else:
                    colnames_aliased.append(colname)
            # якщо умова відпрацювала - повертаємо результат її роботи
            return colnames_aliased
        # у іншому випадку повертаємо просто назви колонок
        return colnames

    # вивід назв колонок(сирий)
    @classmethod
    def columns_names_raw(cls):
        colnames = False
        tablename = cls.__tablename__
        try:
            colnames = []
            for column in Metadata.tables[tablename].columns:
                colnames.append(column.name)
        except:
            pass
        return colnames

    # вивід даних таблиці
    @classmethod
    def columns_data(cls,**kw):
        ()
        order_by = kw.get('order_by', False)
        desc = kw.get("desc",False)
        limit = kw.get('limit',False)
        page = kw.get('page',False)
        #try:
        query = cls.exclude_columns(cls.columns_names())
        sql = 'SELECT {0} FROM {1}'.format(
            ','.join(query),cls.__tablename__)
        if order_by:
            sql = sql + " " + "ORDER BY {0} {1}"\
                .format(order_by,"DESC" if desc == True else "ASC")
        res = DBSession.query(*query).from_statement(sql).all()
        '''
        Якщо активовано пошук - додатково опрацьовуємо результат виводу
        '''
        if cls.search_field:
            search_item = kw.get('search_item',False)
            res = cls.search(res,search_item = search_item)
            
        """ ЯКЩО ВКАЗАНО ЛІМІТ - АКТИВУЄМО ЛІЧИЛЬНИК СТОРІНОК """
        pages = False
        if limit:
            pages = len(res)/int(limit) + 1
            pages = int(pages)
            
        """СОРТУВАННЯ ЗАПИЧІВ ЗА ПЕВНОЮ ХАРАКТЕРИСТИКОЮ"""
        #if kw.get('order_by'):
        #    res.sort(key=lambda i:getattr(i,kw.get('order_by')))
                
        """Зворотній порядок записів"""
        if kw.get('reverse'):
            res.reverse()
                
        if limit:
            if page:
                page = int(page)
                p = (page -1)*limit
                return (res[p:(limit + p)],pages)
            return (res[:limit],pages)
        return (res,pages)
    
    '''
    Метод, що виключає колонки з лістингу, якщо їх вказано у спеціальній
    властивості exclude
    '''
    @classmethod
    def exclude_columns(cls,columns):
        if cls.exclude:
            if isinstance(cls.exclude,(list,tuple)):
                return [col for col in columns if not col in cls.exclude]
        return columns
    
    # формування таблиці
    @classmethod
    def table(cls,**kw):
        content = cls.columns_data(**kw)
        colnames = cls.exclude_columns(cls.columns_names())
        table = {}
        table['name'] = cls.model_name()
        table['mnemo'] = cls.model_mnemo()
        table['colnames'] = cls.columns_names_aliased(colnames)
        table['data'] = [[getattr(i,attr) for attr in colnames] for i in content[0]]
        table['rows'] = len(table['data'])
        table['pages'] = content[1]
        table['add_option'] = cls.add_option
        table['edit_option'] = cls.edit_option
        table['delete_option'] = cls.delete_option
        return table

    @classmethod
    def model_name(cls,lang = False):
        try:
            return cls.verbose_name_plural[0].upper() + cls.verbose_name_plural[1:]
        except:
            return cls.__tablename__[0].upper() + cls.__tablename__[1:]

    @classmethod
    def model_mnemo(cls):
        try:
            return cls.alias
        except:
            return cls.__name__.lower()
            
    @classmethod
    def all(cls):
        """ Return all items without any exclusion """
        return DBSession.query(cls)

    
    @classmethod
    def index(cls, **kw):
        """
        Index of items. 
        Mainly is used to show all items of class(dbtable).
        """
        q = DBSession.query(cls)
        exclude_id = kw.get('exclude_id', False)
        order_by = kw.get('order_by', False)
        try:
            if exclude_id:
                q = q.filter(~cls.id.in_(exclude_id))
            if order_by:
                q = q.order_by(order_by)
        except:
            pass
        if not q:
            q = [(0,'----')]
        return q
        
    @classmethod
    def get(cls, id):
        return DBSession.query(cls).get(id)
    
    @classmethod
    def filter_by(cls,**kw):
        return DBSession.query(cls).filter_by(**kw)
        
    @classmethod
    def filter(cls,*args):
        return DBSession.query(cls).filter(*args)
    
    @classmethod
    def new(cls,**kw):
        obj = cls()
        for k, v in kw.items():
            setattr(obj, k, v)
        DBSession.add(obj)
        #try:
        transaction.commit()
        return True
        #except:
        #    transaction.abort()
        #    return False

    @classmethod
    def show(cls, id):
        return DBSession.query(cls).get(id)

    @classmethod
    def update(cls, id, **kw):
        obj = cls.show(id)
        for k, v in kw.items():
            if k in obj.__dict__.keys():
                setattr(obj, k, v)
        DBSession.add(obj)
        try:
            transaction.commit()
            return True
        except:
            transaction.abort()
            return False


    def add_to_session(self):
        """ Add instance to session """
        DBSession.add(self)
            
    @classmethod
    def count(cls):
        try:
            return Q(cls).count()
        except:
            return 0

    def delete(self):
        obj = self
        DBSession.delete(obj)
        try:
            transaction.commit()
            return True
        except Exception as e:
            transaction.abort()
            raise e

    def save(self, **kw):
        sess_flush = kw.get("flush", False)
        DBSession.add(self)
        if sess_flush:
            DBSession.flush()
        try:
            transaction.commit()
            return True
        except Exception as e:
            transaction.abort()
            raise e

    @classmethod
    @property
    def query(cls):
        return DBSession.query(cls)

    # вивід даних для <select>
    @classmethod
    def get_items_to_select(cls,from_zero = False,callback=False):
        items = DBSession.query(cls).filter(cls.active == True)
        if not callback:
            def itemname(item):
                try:
                    return item
                except:
                    return item.inner_name
        else:
            def itemname(item):
                try:
                    return getattr(item,callback)()
                except:
                    return item.inner_name
        
        if from_zero:
            listing = [('0','нічого не вибрано')]
            listing.extend([(str(item.id),itemname(item)) for item in items])
            return listing
        listing = [(str(item.id),itemname(item)) for item in items]
                
        return listing

    @classmethod
    def _uiform(cls):
        """ Custom form factory class """
        UIForm = model_form_factory(base=ProtectedForm)
        """ Fix for situation when wtforms can`t get session from form """
        def get_session():
            return DBSession
        UIForm.get_session = get_session
        return UIForm
        
    @classmethod
    def _form(cls, *args, **kw):
        UIForm = cls._uiform()
        class __F(UIForm):
            exclude = "id","created","updated"
            class Meta:
                model = cls

            def validate_and_add(self):
                """ Validate and add item """
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    # if validation has been passed...
                    obj = cls()
                    for attr in columns:
                        try:
                            setattr(obj,attr, getattr(self, attr).data)
                        except:
                            pass
                    obj.save(flush=True)
                    return True
                return False

            def validate_and_edit(self):
                if self.validate():
                    obj = self._obj
                    self.populate_obj(obj)
                    obj.save(flush=True)
                    return True
                return False

        return __F(*args, **kw)
        
    @classmethod
    def ui(cls, *args, **kw):
        f = cls._form(*args, **kw)
        return f
