# -*- coding: utf-8 -*-
import os
import re
import time

# SQLALCHEMY IMPORTS
import sqlalchemy.types as types

# APP IMPORTS
from pyramid_cms import admin

'''
ПОЛЕ ДЛЯ ЗАВАНТАЖЕННЯ ФАЙЛІВ У ПОПЕРЕДНЬО ВИЗНАЧЕНУ ЛОКАЦІЮ
'''

class UploadFileField(types.TypeDecorator):
    '''
    Тип, від якого наслідує це поле - String
    '''
    impl = types.String
    
    def __init__(self,*args,**kwargs):
        '''
        MIME-типи файлів, які приймає поле.
        Задається при декларуванні поля як kwarg
        '''
        self.mimetypes = kwargs.pop('mimetypes',False)
        '''
        Шлях для завантаження файлу.
        Задається при декларуванні поля як kwarg
        '''
        self.upload_to = kwargs.pop('upload_to','')
        '''
        Назва файлу, що буде завантажуватися.
        Потрібна для того, щоб не вантажити файл із сирцевою назвою
        з міркувань безпеки.
        '''
        self.uploading_filename = False
        
        super().__init__(*args,**kwargs)
    
    def _validate_mimetype(self):
        '''
        Валідація MIME-типу.
        Міме-типи задаються у вигляді kwargs['mimetypes'] при означенні поля.
        Якщо файл має не той міме-тип, що зазначено серед можливих - 
        файл не зберігається.
        '''
        try:
            mimetype = self.value.get('type',False)
            if self.mimetypes and isinstance(self.mimetypes,(list,tuple)):
                if mimetype in self.mimetypes:
                    return True
            return False
        except:
            return False
    
    def _check_filename(self):
        '''
        Очищаємо назву файлу від зайвих символів.
        '''
        self.uploading_filename = re.sub(r'[@#$%\^\&)(\+]+','_',self.value['filename'])
    
    def _upload_file(self):
        # Перевіряємо назву файлу
        self._check_filename()
        # Встановлюємо шлях для запису
        file_path = os.path.join(admin.admin.config.files, self.upload_to, self.uploading_filename)
        '''
        Завантаження файлу у визначену локацію.
        Записуємо файл.
        Якщо його довжина становить 0 байтів(буфер fp порожній) - видаляємо цей файл
        '''
        try:
            with open(file_path,'wb') as f:
                f.write(self.value['file'].read())
                f.close()
            if os.path.getsize(file_path) == 0:
                os.unlink(file_path)
        except Exception as e:
            raise Exception(e)
    
    def process_bind_param(self, value, dialect):
        '''
        Функція, котра визначає, що саме записувати у колонку таблиці,
        та є місцем, де можна проводити додаткові дії з даними.
        '''
        self.value = value
        '''
        Якщо файл проходить валідацію, він записується у директорію,
        визначену в конфігу.
        '''
        if self._validate_mimetype():
            self._upload_file()
            #return self.uploading_filename
            return os.path.join('/',self.upload_to, self.uploading_filename)
        return None
