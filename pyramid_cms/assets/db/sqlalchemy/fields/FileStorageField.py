# -*- coding: utf-8 -*-
import os,re

# SQLALCHEMY IMPORTS
import sqlalchemy.types as types

# APP IMPORTS
from pyramid_cms import admin
from .UploadFileField import UploadFileField
"""
ПОЛЕ ДЛЯ ЗАВАНТАЖЕННЯ ФАЙЛІВ У ПОПЕРЕДНЬО ВИЗНАЧЕНУ ЛОКАЦІЮ,
З АВТОМАТИЧНИМ ВИЗНАЧНИКОМ ПІДДИРЕКТОРІЙ
"""

class FileStorageField(UploadFileField):
    
    def _check_filename(self):
        """
        Очищаємо назву файлу від зайвих символів.
        """
        self.uploading_filename = re.sub(r'[@#$%\^\&)(\+]+','_',self.value['filename'])
        """
        На основі розширення файлу визначаємо піддиректорію, куди він
        буде завантажуватися.
        """
        self.uploading_subdir = os.path.splitext(self.value['filename'])[1][1:]
        
    
    def _check_subdir(self,subdir):
        """
        Перевірка на наявність субдиректорії для завантаження файлу із
        відповідним розширенням.
        Якщо субдиректорії немає - вона створюється.
        """
        if not os.path.isdir(subdir):
            os.mkdir(subdir)
    
    def _upload_file(self):
        # Перевіряємо назву файлу
        self._check_filename()
        # Встановлюємо шлях до субдиректорії та перевіряємо її наявність
        if self.upload_to:
            subdir = os.path.join(admin.admin.config.files,self.upload_to,self.uploading_subdir)
        else:
            subdir = os.path.join(admin.admin.config.files,self.uploading_subdir)
        self._check_subdir(subdir)
        # Встановлюємо шлях для запису
        self.file_path = os.path.join(subdir,self.uploading_filename)
        """
        Завантаження файлу у визначену локацію.
        Записуємо файл.
        Якщо його довжина становить 0 байтів(буфер fp порожній) - видаляємо цей файл
        """
        try:
            with open(self.file_path,'wb') as f:
                f.write(self.value['file'].read())
                f.close()
            if os.path.getsize(self.file_path) == 0:
                os.unlink(self.file_path)
        except:
            pass

    def process_bind_param(self, value, dialect):
        """
        Функція, котра визначає, що саме записувати у колонку таблиці,
        та є місцем, де можна проводити додаткові дії з даними.
        На відміну від батьківського класу записує у поле БД не назву файлу,
        а абсолютний шлях до файлу, включно з його назвою - це спростить 
        у подальшому доступ до файлу.
        """
        self.value = value
        """
        Якщо файл проходить валідацію, він записується у директорію,
        визначену в конфігу.
        """
        if self._validate_mimetype():
            self._upload_file()
            return os.path.join(
                '/',
                self.upload_to,
                self.uploading_subdir,
                self.uploading_filename
            )

        return None
