# -*- coding: utf-8 -*-

'''
ЯКЩО ВИКОРИСТОВУЄМО АЛХІМІЮ, ТО ВІДПОВІДНО ФОРМУЄМО АТРИБУТИ
З`ЄДНАННЯ
'''
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session,sessionmaker
from zope.sqlalchemy import ZopeTransactionExtension
DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()
Metadata = Base.metadata
Q = DBSession.query
