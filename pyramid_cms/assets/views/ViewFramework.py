# -*- coding: utf-8 -*-

"""
ViewFramework - mixing class for different viewtypes
"""
from pyramid.httpexceptions import HTTPFound, HTTPNotFound

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Q
from pyramid_cms.apps.roots.dbmodels import TreeNode, PageType
from pyramid_cms.config.scripts.init_scenarios import start_init


class ViewFramework(object):
    """
    ViewFramework - mixing class for different viewtypes
    """
    _impl = None
    view_type = None
    excluded_viewtypes = ("admin_init", 'downloadspage')

    def __init__(self, request):
        self.req = request
        self.rmd = request.matchdict
        self.node = None
        if self.req.timemark[0]:
            if self.req.timemark[1] == self.req.timemark2[0]:
                self.get_view_type()

    def __getattr__(self, attrname):
        try:
            return getattr(self._impl, attrname)
        except:
            raise Exception("Attribute {0} not found!".format(attrname))
            
    def get_tree_node(self):
        """ Get tree node from database """
        # make a copy of matchdict
        path = list(self.rmd.get("tail",[]))[:]
        def _get_tree_node(node, stack):
            if stack:
                current_mnemo = stack.pop(0)
                # pop last item through each iteration
                next_item = \
                    node.children\
                        .filter_by(mnemo=current_mnemo, active=True).first()
                if next_item:
                    _get_tree_node(next_item, stack)
            else:
                if node:
                    #if node.redirect_id:
                    #    self.node = Q(TreeNode).get(node.redirect_id)
                    #else:
                    self.node = node
                else:
                    self.node = None

        rootnode  = Q(TreeNode)\
            .filter(TreeNode.mnemo == "/", TreeNode.active == True).first()
        _get_tree_node(rootnode, path)
        
    def get_notfound_node(self):
        return Q(TreeNode).filter(TreeNode.mnemo == '404').first()

    def get_node_by_mnemo(self, mnemo):
        node = Q(TreeNode).filter(TreeNode.mnemo == mnemo).first()
        if not node:
            node = self.get_notfound_node()
        return node

    def get_view_type(self):
        """
        Getting view types.
        1. If there are incomplete init signals from start_init script
        view type sets at "admin_init".
        """
        if False in start_init(self.req):
            self.view_type = "admin_init"
        else:
            self.get_tree_node()
            try:
                # trying get viewtype from current node
                self.view_type = self.node.pagetype.mnemo
            except:
                # if not - raise Not Found exception
                """
                We can`t get view type in 2 cases: 
                1. page doesn`t exist | was not found because of inactivity
                2. page exists but has no page type
                """
                if "welcome" in self.rmd.get("tail"):
                    """ For WELCOME PAGES slightly another logic is applied """
                    self.view_type = "admin_init"
                if 'downloads' in self.rmd.get('tail'):
                    """ Також інша логіка застосовується для сторінок завантаження """
                    self.view_type = 'downloadspage'
                else:
                    self.node = self.get_notfound_node()
                    self.view_type = self.node.pagetype.mnemo
                    #raise HTTPNotFound()
        try:
            
            view = admin.admin.viewtypes.get(self.view_type)
            if not view:
                # якщо не знайшли потрібний тип в’ю - призначаємо 404
                self.node = self.get_notfound_node()
                self.view_type = self.node.pagetype.mnemo
                view = admin.admin.viewtypes.get(self.view_type)
            if self.view_type not in self.excluded_viewtypes:
                self.set_assets(view)
            self._impl = view(self.req)
        except Exception as ex:
            raise ex

    def set_assets(self, view):
        """ Set attribbutes to self._impl (current viewtype) """
        view.node = self.node
        view.template = self.node.pagetype.template
        view.widgets = [w.mnemo for w in self.node.pagetype.widgets]
        view.permissions = [p.name for p in self.node.permissions]
