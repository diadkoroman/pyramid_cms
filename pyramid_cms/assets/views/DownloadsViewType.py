# -*- coding: utf-8 -*-

"""
Put __doc__ info here.
"""

import os, datetime
# PYRAMID IMPORTS
from pyramid.httpexceptions import HTTPFound, HTTPNotFound, HTTPForbidden
from pyramid.response import Response, FileResponse

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.views import ProjectViewType
from pyramid_cms.apps.files import dbmodels as filesdb
from pyramid_cms.apps.stats import dbmodels as statsdb

class DownloadsViewType(object):
    
    """
    Базовий клас для розгортання в’юхи, котра відповідатиме за завантаження
    файлів на боці проекту.
    Головні особливості:
     - відслідковується айпі клієнта, що входить завантажувати файл
     - визначається реферер, тобто сторінка, з якої відбувається перехід на
     посилання для скачування. При відсутності реферера скачування файлу
    блокується. Таким чином ми створюємо труднощі для тих, хто хотів би напряму
    качати файли роботом.
     - визначається кількість скачувань одного файлу одним клієнтом за день.
     При перевищенні ліміту скачування блокується.
     - визначається загальний ліміт завантажень для одного айпішника за день.
     При перевищенні ліміту скачування блокується.
    Інформація:
    downloads_url - Шлях для завантаження. Встановлюється у вигляді
    назви елемента урла, з якого починається пошук файлу для завантаження. 
    Наприклад для шляху 'http://<site>/downloads/<file>' необхідно
    визначити downloads_url = 'downloads'
    За умовчанням downloads_url = 'downloads'
     
    УВАГА!
    На боці проекту необхідно розгорнути кастомізований клас для в’юхи
    цього типу і визначити(перевизначити) наступні атрибути:
    site_id - ID сайту. За умовчанням не встановлюється.
    
    daily_file_downloads_limit - добовий ліміт завантажень файлу для 
    одного клієнта (integer). За умовчанням - 1.
    daily_total_downloads_limit - загальнодобовий ліміт усіх завантажень файлів
    для одного клієнта (integer). За умовчанням - 1.
    """
    downloads_url = 'downloads'
    root_dir = admin.admin.config.files
    visits = statsdb.Visit
    
    def __init__(self, req):
        self.req = req
        # IP клієнта
        self.client_ip = req.client_addr
        # хост (початкова сторінка)
        self.host = req.host
        # Сторінка, з якої прийшов клієнт
        self.referrer = req.referrer
        # запитаний урл
        self.requested_url = req.path_info
        # поточна дата
        self.today = datetime.date.today().isoformat()
        self.tail = req.matchdict.get('tail')
        self.filepath = os.path.join(self.root_dir, '/'.join(self.tail[1:]))
        
    # метод, що визначає сторінку, з якої прийшов клієнт
    def check_referrer(self):
        '''
        Якщо у запиті є хост і реферер і якщо реферер не є хостом
        (вхід у директорію не з першої сторінки) - повертаємо True
        Інакше повертаємо False
        '''
        if not self.host is None:
            if not self.referrer is None and not self.referrer == self.host:
                return True
        return False
        
    # Метод, що повертає шлях до запитаного файлу
    def check_filepath(self):
        '''
        Залежно від результатів відбувається або формування шляху до
        файлу, або повертається False
        '''
        if os.path.isfile(self.filepath):
            return self.filepath
        return False
        
    # Метод, що визначає загальну кількість скачувань з цього IP на дату
    def check_all_downloads_from_client_ip(self):
        '''
        Перевіряємо у візитах доступи з цього IP до урлів, що ведуть
        на завантаження файлів. Якщо кількість таких доступів за день 
        перевищує встановлений ліміт - повертаємо False.
        Інакше повертаємо True
        '''
        visits = Q(self.visits)\
        .filter(self.visits.client_ip == self.client_ip,
        self.visits.requested_url.like('%'+self.downloads_url+'%'),
        self.visits.created.like(self.today + '%'))\
        .count()
        if visits >= self.daily_total_downloads_limit:
            return False
        return True
        
    # Метод, що визначає кількість скачувань файлу з цього IP на дату
    def check_file_downloads_from_client_ip(self):
        """
        Перевіряємо у візитах доступи з цього IP до конкретного файлу.
        Якщо кількість таких доступів за день перевищує ліміт - повертаємо
        False.
        Інакше повертаємо True
        """
        visits = Q(self.visits)\
        .filter(self.visits.client_ip == self.client_ip,
        self.visits.requested_url.like('%'+self.filepath.split('/')[-1]),
        self.visits.created.like(self.today + '%'))\
        .count()
        if visits >= self.daily_file_downloads_limit:
            return False
        return True
        
    def get(self):
        # і якщо йому взагалі ще можна сьогодні кчати файли
        if self.check_all_downloads_from_client_ip():
            if self.check_file_downloads_from_client_ip():
                # і якщо з файлом усе в порядку
                fpath = self.check_filepath()
                if fpath:
                    return FileResponse(fpath,request=self.req)
        # перевищено ліміт скачувань на сьогодні
        raise HTTPFound(location='/404/')
        
    def post(self):
        return self.get()
