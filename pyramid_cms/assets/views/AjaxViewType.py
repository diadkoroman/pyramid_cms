# -*- coding: utf-8 -*-

"""
View type for work out ajax-requests
"""

import json

from pyramid.renderers import render
from pyramid.response import Response

class AjaxViewType(object):
    """
    Клас в’ю для опрацювання аякс-запитів
    -------------------------------------
    option - містить дані про поточну опцію, яка повинна бути виконана.
    Значення отримується із запиту спеціальним методом get_option, котрий
    відпрацьовує на етапі ініціалізації класу.
    
    outp_type - містить дані про тип відповіді, яку хоче сайт.
    Значення отримується із контейнера data спеціальним методом get_outp_type
    
    default_outp_type  - дефолтне значення для типу відповіді
    
    data - контейнер для даних, 
    отриманих через self.req.POST або self.req.GET
    Значення отримується із запиту спеціальним методом get_data, котрий
    відпрацьовує на етапі ініціалізації класу.
    """
    option = None
    outp_type = None
    default_outp_type = 'json'
    data = None
    # view response stack
    resp = {}

    def __init__(self, req):
        self.req = req
        self.rmd = req.matchdict
        self.tail = req.matchdict.get('tail')
        
        self.get_data()
        self.get_option()
        self.outp_type = self.default_outp_type
        self.get_outp_type()

    def get_option(self):
        """
        Метод, що встановлює значення атрибута option
        Якщо self.data має хоч щось, то намагаємося звідти
        витягти елемент "option"
        """
        if self.data:
            self.option = self.data.get('option', None)

    def get_outp_type(self):
        """
        Метод, що встановлює значення атрибута outp_type
        Якщо self.data містить дані, то нимагаємося витягти
        "outp_type", в усіх інших випадках користуємося
        дефолтним значенням з атрибута default_outp_type
        """
        if self.data:
            self.outp_type = self.data.get('outp_type', self.default_outp_type)

    def get_data(self):
        """ 
        Метод, що встановлює значення атрибута data
        У випадку POST дані беруться з self.req.POST
        У випадку GET - з self.req.GET відповідно.
        """
        self.data = getattr(self.req, self.req.method, None)

    def get(self, renderer):
        """ Виконується для GET-запитів """
        # self.get_option()
        try:
            getattr(self, '{0}_option'.format(self.option))()
        except AttributeError:
            pass
        resp = render(renderer, dict(self.resp))
        return Response(resp)

    def post(self, renderer):
        """ Виконується для POST-запитів """
        # self.get_option()
        try:
            getattr(self, '{0}_option'.format(self.option))()
        except AttributeError:
            pass
        resp = render(renderer, json.dumps(dict(self.data)))
        return Response(resp)

