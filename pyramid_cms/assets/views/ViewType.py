# -*- coding: utf-8 -*-
import os
import transaction
from pyramid_beaker import cache
from pyramid.httpexceptions import HTTPForbidden, HTTPFound
from pyramid.renderers import render, render_to_response
from pyramid.response import Response

from pyramid_cms import admin
from pyramid_cms.assets.views import errors
from pyramid_cms.assets.db.sqlalchemy import Q

from pyramid_cms.apps.roots import dbmodels as rootsdb
from pyramid_cms.apps.slots import dbmodels as slotsdb

""" ViewType class is a basys for every View implementation. """


class ViewType(object):

    """ Package name should be rewritten for each project-sided view """
    abs_tpl_path = None
    node = None
    package_name = admin.admin.config.packagename
    permissions = []
    renderer = None
    test = False
    test_data = None
    test_xml_document = False
    xml = False
    vdata = None
    

    def __init__(self, req):
        self.req = req
        self.rmd = req.matchdict
        self.kw = {}   # kw resources
        self.kw["get"] = req.GET
        self.kw["post"] = req.POST
        self.kw["tail"] = list(req.matchdict.get("tail",[]))
        self.kw["session"] = req.session
        self.kw["node"] = self.node
        self.kw['slots'] = {}
        self.templates = admin.admin.config.templates
        """
        Заглушив стек self._widgets, бо в ньому немає видимого сенсу
        """
        # self._widgets = admin.admin.widgets
        
        # self.vdata = etree.Element("HtmlDocument")
        self.vdata = {}
        self.vdata['slots'] = {}
        self.vdata["package_name"] = self.package_name
        self.vdata['project_name'] = \
                                     getattr(admin.project.config,
                                             'packagename',
                                             False)
        # if self.xml:
        # preprocessing data
        #self.create_root_element()
        self.get_curr_lang()
        self.get_slots()
        self.prepare_data()
        self.get_data()
        self.manage_widgets()
        self.get_widgets(**self.kw)
        self.get_template()
        # self.process_vdata()

    def get_curr_lang(self):
        """
        Setting current language for each request.
        1. Try to get lang value from tail[0]
        2. If 1. fails - set default lang value
        3. Try to get language item from database
        4. If 3. fails - set default lang mnemo
        """
        #try:
        lmnemo = self.req.matchdict.get('lang', admin.admin.config.deflang)
        self.req.curr_lang = Q(rootsdb.Language).filter(rootsdb.Language.mnemo == lmnemo).scalar()
            #if not lang:
            #    lang = self.lang_by_deflang()
            #    if not lang:
            #        lang = admin.admin.config.deflang
            # self.req.curr_lang = lang
        #except:
        #    transaction.abort()
        #    self.get_curr_lang()

    def get_commons(self):
        """ Prepare data independent from request method """
        pass
    
    def get_POSTs(self):
        """ Get data for POST request """
        pass
        
    def get_GETs(self):
        """ Get data for GET request """
        pass
        
    def get_data(self):
        """ Wrapper for getting data """
        self.get_commons()
        getattr(self, "get_{0}s".format(self.req.method))()
        
    def get_project_verbose_name(self):
        """ Отримуємо назву проекту(читабельну) для відображення в тайтлі """
        self.vdata['project_name'] = None
        try:
            return admin.project.config.verbose_name.get(self.req.curr_lang.mnemo,'')
        except:
            return None
        
    def get_pagetitle(self):
        """ Отримуємо заголовок для сторінки """
        self.vdata['pagetitle'] = None
        try:
            return self.node.titles\
            .filter_by(lang_id=self.req.curr_lang.id).first().content
        except:
            return None

    def prepare_data(self):
        """ Preprocessing various view data """
        pass

    def create_root_element(self):
        self.vdata = etree.Element("HtmlDocument")

    def create_template(self):
        """ Method to create templates """
        abspath = os.path.join(
            admin.admin.config.templates[
                admin.admin.config.templates_default_dir
                ], self.template)
        if self.xml:
            # generate template file for xslt
            self.generate_xsl_file(abspath)
        else:
            with open(abspath, 'w') as f:
                header = '<!-- {0} page template (start)-->'.format(self.template)
                content = '<!-- SET CONTENT HERE -->'
                footer = '<!-- {0} page template (end) -->'.format(self.template)
                f.write(header + '\n\n' + content + '\n\n' + footer)
        return abspath
        
    def generate_xsl_file(self, abspath, **kw):
        with open(abspath, 'w') as f:
            declaration = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            stylesheet = """<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">"""
            output = """<xsl:output method=\"{output_method}\" doctype-system="" encoding=\"{encoding}\" omit-xml-declaration=\"yes\"/>""".format(
                                                 output_method=\
                                                 kw.get("output_method","html"),
                                                 encoding=\
                                                 kw.get("encoding","UTF-8")
                                                 )
            template = "<xsl:template match=\"/\"></xsl:template>"
            stylesheet_close = "</xsl:stylesheet>"
            f.write(declaration + '\n' + stylesheet + '\n' + output + "\n" + template + "\n" + stylesheet_close)
        
    def manage_widgets(self, **kw):
        """ Manage common widget_resources """
        pass

    def get_template(self):
        for path in self.templates:
            self.abs_tpl_path = os.path.join(path, self.template)
            if os.path.isfile(os.path.join(path, self.template)):
                return self.abs_tpl_path
        return self.create_template()

    def get_widgets(self, **kw):
        """ Collecting all widgets """
        # self.manage_widgets()
        for w in self.widgets:
            try:
                """
                First we try get new widgets defined as classes
                inheriting from base "Widget" class
                """
                #try:
                admin.admin.widgets.get(w)(self.req, self.vdata, **kw)()
                # self._widgets.get(w)(self.req, self.vdata, **kw)()
                #except:
                #    raise errors.WidgetNotFoundError(w)
            except Exception as e:
                raise e
                #raise errors.WidgetNotFoundError(w)
    

    def render(self):
        """ Render template function. """
        # self.get_widgets(**self.kw)
        if self.test:
            return Response(self.test_data)
        # elif self.test_xml_document:
        #    return Response(etree.tostring(self.vdata, pretty_print=True))
        
        if self.renderer:
            resp = render(self.renderer, self.vdata, request=self.req)
            
        else:
            resp = render(self.get_template(), self.vdata, request=self.req)
        #raise Exception(self.req.response.headers)
        return Response(resp)

    def get(self):
        """ Response GETs """
        """
        Опрацьовуємо внутрішній редирект(в межах дерева сайту).
        Якщо не можемо опрацювати його з якихось причин - просто
        проходимо далі. Відображати тут помилку поки що немає потреби.
        """
        if self.node.redirect_id:
            try:
                return HTTPFound(
                location='/{0}{1}'.format(
                    self.req.curr_lang.mnemo, 
                    Q(rootsdb.TreeNode).get(self.node.redirect_id).get_path()))
            except:
                pass
        """
        Опрацьовуємо зовнішній редирект(за межі дерева сайту).
        Якщо не можемо опрацювати його з якихось причин - просто
        проходимо далі. Відображати тут помилку поки що немає потреби.
        При цьому ми перевіряємо, чи є рядок валідною http - адресою.
        Якщо це не так - додаємо до рядка реквізит http - запиту
        і пробуємо ще раз.
        """
        if self.node.redir_address:
            try:
                if self.node.redir_address.startswith('http'):
                    return HTTPFound(location=self.node.redir_address)
                return HTTPFound(location='http://' + self.node.redir_address)
            except:
                pass
        if self.permissions:
            if isinstance(self.permissions, (list, tuple)):
                for p in self.permissions:
                    if not self.req.has_permission(p):
                        #return HTTPForbidden()
                        return HTTPFound(location='/404')
        return self.render()

    def post(self):
        """ Resonse POSTs """
        """
        Опрацьовуємо внутрішній редирект(в межах дерева сайту).
        Якщо не можемо опрацювати його з якихось причин - просто
        проходимо далі. Відображати тут помилку поки що немає потреби.
        """
        if self.node.redirect_id:
            try:
                return HTTPFound(
                location='/{0}{1}'.format(
                    self.req.curr_lang.mnemo, 
                    Q(rootsdb.TreeNode).get(self.node.redirect_id).get_path()))
            except:
                pass
        """
        Опрацьовуємо зовнішній редирект(за межі дерева сайту).
        Якщо не можемо опрацювати його з якихось причин - просто
        проходимо далі. Відображати тут помилку поки що немає потреби.
        При цьому ми перевіряємо, чи є рядок валідною http - адресою.
        Якщо це не так - додаємо до рядка реквізит http - запиту
        і пробуємо ще раз.
        """
        if self.node.redir_address:
            try:
                if self.node.redir_address.startswith('http'):
                    return HTTPFound(location=self.node.redir_address)
                return HTTPFound(location='http://' + self.node.redir_address)
            except:
                pass
        if self.permissions:
            if isinstance(self.permissions, (list, tuple)):
                for p in self.permissions:
                    if not self.req.has_permission(p):
                        return HTTPForbidden()
        return self.render()


    def get_slots(self):
        """
        Отримуємо слоти
        Ми імпортуємо не підготовлену локаль а цілий слот на випадок, якщо нам
        навіщось знадобиться використовувати інші локалі слоту на клієнтській
        сторінці (у шаблоні наприклад). Поки що така поведінка є 
        експериментальною. Якщо буде потреба зменшити трафік, збільшити 
        відгукливість сайту або просто не буде потреби імпортувати всі локалі
        слоту - ми можемо змінити цю поведінку.
        
        Поки що стеки для слотів створюються 
        і у ViewType.kw  і у ViewType.vdata
        Така поведінка є експериментальною.
        Надалі будемо дивитися, від якого стеку є користь
        kw - стек, що активно використовується віджетами
        vdata - стек прямого виводу з ViewType на клієнта, 
        тому обидва варіанти, в принципі, мають право на існування.
        """
        if not self.kw.get('slots', False):
            """
            Створюємо стек для слотів у ViewType.kw, 
            якщо його раптом не забув створити __init__
            (або забули дати вказівку init-у ми;))
            """
            self.kw['slots'] = {}
        self.kw['slots']['strings'] = {}
        self.kw['slots']['texts'] = {}
        
        if not self.vdata.get('slots', False):
            """
            Створюємо стек для слотів у ViewType.vdata, 
            якщо його раптом не забув створити __init__
            (або забули дати вказівку init-у ми;))
            """
            self.vdata['slots'] = {}
        self.vdata['slots']['strings'] = {}
        self.vdata['slots']['texts'] = {}
        
        """ Спочатку рядкові... """
        try:
            for i in Q(slotsdb.FreeString)\
                .filter(slotsdb.FreeString.active == True):
                self.vdata['slots']['strings'][i.mnemo] = \
                    self.kw['slots']['strings'][i.mnemo] = i
        except:
            pass
            
        """... А потім і текстові """
        try:
            for i in Q(slotsdb.FreeText)\
            .filter(slotsdb.FreeText.active == True):
                self.vdata['slots']['texts'][i.mnemo] = \
                    self.kw['slots']['texts'][i.mnemo] = i
        except:
            pass
