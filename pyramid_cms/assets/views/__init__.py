from pyramid_cms.assets.views.ViewFramework import ViewFramework
from pyramid_cms.assets.views.ViewType import ViewType
from pyramid_cms.assets.views.AdminViewType import AdminViewType
from pyramid_cms.assets.views.ProjectViewType import ProjectViewType
from pyramid_cms.assets.views.DownloadsViewType import DownloadsViewType
from pyramid_cms.assets.views.AjaxViewType import AjaxViewType
from pyramid_cms.assets.views.NotFoundViewType import NotFoundViewType
# xml/xslt test branch
# from pyramid_cms.assets.views.xml.XmlViewType import XmlViewType
