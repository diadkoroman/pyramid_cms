# -*- coding: utf-8 -*-

"""
View for 404 pages customization.
Set on PCMS side.
Base class for 404-pages registered on PCMS side as 
pyramid_cms.views.viewtypes.NotFound_Page
Template for this pagetype is created on project side and can(should:))
be customized by developer.
"""

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.views import ProjectViewType

class NotFoundViewType(ProjectViewType):
    """
    Base class for 404 pages
    """
    def render(self):
        """
        Щоб коректно відіслати HTTP-заголовки,
        ми отримуємо стандартний response від ViewType
        і змінюємо його HTTP status code на 404 Not Found,
        та повертаємо змінений  response
        """
        resp = super().render()
        resp.status_code = 404
        return resp
