# -*- coding: utf-8 -*-
'''
import os
import transaction
from lxml import etree
from pyramid.httpexceptions import HTTPForbidden
from pyramid.renderers import render
from pyramid.response import Response

from pyramid_cms import admin
from pyramid_cms.assets.views import errors
from pyramid_cms.assets.db.sqlalchemy import Q

from pyramid_cms.apps.roots import dbmodels as rootsdb
from pyramid_cms.apps.slots import dbmodels as slotsdb

"""
XmlViewType
"""


class XmlViewType(object):

    """ Package name should be rewritten for each project-sided view """
    abs_tpl_path = None
    node = None
    package_name = admin.admin.config.packagename
    permissions = []
    renderer = None
    test = False
    test_data = None
    test_xml_document = False
    xml = False
    vdata = None
        

    def __init__(self, req):

        self.req = req
        self.rmd = req.matchdict
        self.kw = {}   # kw resources
        self.kw["get"] = req.GET
        self.kw["post"] = req.POST
        self.kw["tail"] = list(req.matchdict.get("tail",[]))
        self.kw["session"] = req.session
        self.kw["node"] = self.node
        self.kw['slots'] = {}
        self.templates = admin.admin.config.templates
        self.template = self.__class__.__name__.lower() + '.xsl'
        self._widgets = admin.admin.widgets
        self.vdata = {}
        self.vdata['document'] = self.create_root_element()
        self.vdata['slots'] = {}
        self.vdata["package_name"] = self.package_name
        self.vdata['project_name'] = \
                                        getattr(admin.project.config,
                                                'packagename',
                                                False)
        # if self.xml:
        # preprocessing data
        # self.get_slots()
        #
        self.prepare_data()
        # self.get_data()

        self.get_template()
        # self.process_vdata()

    def get_commons(self):
        """ Prepare data independent from request method """
        pass
    
    def get_POSTs(self):
        """ Get data for POST request """
        pass
        
    def get_GETs(self):
        """ Get data for GET request """
        pass
        
    def get_data(self):
        """ Wrapper for getting data """
        self.get_commons()
        getattr(self, "get_{0}s".format(self.req.method))()
        
    def get_pagetitle(self):
        """ Отримуємо заголовок для сторінки """
        pagetitle = etree.subElement(self.vdata['document'])
        #self.vdata['pagetitle'] = None
        try:
            pagetitle.text = self.node.titles\
            .filter_by(lang_id=self.req.curr_lang.id).first().content
        except:
            pass

    def prepare_data(self):
        """ Preprocessing various view data """
        pass

    def create_root_element(self):
        """
        Creation of root element consists of such parts:
        1. create root element itself
        2. create root elements headers section(metainformation: url,language etc.)
        3. create headers section children
        4. create document body section
        """
        root_element = etree.Element("HtmlDocument")
            
        # headers element
        headers = etree.SubElement(root_element,'DocumentHeaders')
            
        # url element
        url = etree.SubElement(headers, 'Url')
        url.text = self.node.get_path()
            

        lang = etree.SubElement(headers, 'Lang')
        etree.SubElement(lang, 'id').text = str(getattr(self.req.curr_lang, 'id', ''))
        etree.SubElement(lang, 'mnemo').text = str(getattr(self.req.curr_lang, 'mnemo'))
            
        #css section
        etree.SubElement(headers, 'Css').text='css'
        #jss section
        etree.SubElement(headers, 'Jss').text='jss'
            
        # document body section
        body = etree.SubElement(root_element, 'DocumentBody')
            
        # we return body element for completion instead of root
        return root_element

    def create_template(self):
        """ Method to create templates """
        abspath = os.path.join(
            admin.admin.config.templates[
                admin.admin.config.templates_default_dir
                ], self.template)
        # generate template file for xslt
        self.generate_xsl_file(abspath)
        return abspath
            
    def generate_xsl_file(self, abspath, **kw):
        with open(abspath, 'w') as f:
            declaration = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            stylesheet = """<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">"""
            output = """<xsl:output method=\"{output_method}\" doctype-system="" encoding=\"{encoding}\" omit-xml-declaration=\"yes\"/>""".format(
                                                     output_method=\
                                                     kw.get("output_method","html"),
                                                     encoding=\
                                                     kw.get("encoding","UTF-8")
                                                     )
            template = "<xsl:template match=\"/\"></xsl:template>"
            stylesheet_close = "</xsl:stylesheet>"
            f.write(declaration + '\n' + stylesheet + '\n' + output + "\n" + template + "\n" + stylesheet_close)
            
    def manage_widgets(self, **kw):
        """ Manage common widget_resources """
        pass
            
    def in_widgets(self, name):
        if name in self.widgets:
            return True
        return False

    def get_template(self):
        for path in self.templates:
            self.abs_tpl_path = os.path.join(path, self.template)
            if os.path.isfile(os.path.join(path, self.template)):
                return self.abs_tpl_path
        return self.create_template()

    def get_widgets(self, **kw):
        """ Collecting all widgets """
        # first we correct widgets output if needed
        self.manage_widgets()
        # and than init all widgets
        for w in self.widgets:
            try:
                """
                First we try get new widgets defined as classes
                inheriting from base "Widget" class
                """
                #try:
                self._widgets.get(w)(self.req, self.vdata, **kw)()
                #except:
                #    raise errors.WidgetNotFoundError(w)
            except Exception as e:
                raise e
                #raise errors.WidgetNotFoundError(w)

    def render(self):
        """ Render template function. """
        # we get widgets up before rendering for it actual state
        self.get_widgets(**self.kw)
            
        if self.test_xml_document:
            return Response(etree.tostring(self.vdata['document'], pretty_print=True))
        if self.renderer:
            resp = render(self.renderer, self.vdata, request=self.req)
        else:
            resp = render(self.get_template(), self.vdata['document'], request=self.req)
        return Response(resp)

    def get(self):
        """ Response GETs """
        if self.permissions:
            if isinstance(self.permissions, (list, tuple)):
                for p in self.permissions:
                    if not self.req.has_permission(p):
                        return HTTPForbidden()
        return self.render()

    def post(self):
        """ Resonse POSTs """
        if self.permissions:
            if isinstance(self.permissions, (list, tuple)):
                for p in self.permissions:
                    if not self.req.has_permission(p):
                        return HTTPForbidden()
        return self.render()


    def get_slots(self):
        """
        Отримуємо слоти
        Ми імпортуємо не підготовлену локаль а цілий слот на випадок, якщо нам
        навіщось знадобиться використовувати інші локалі слоту на клієнтській
        сторінці (у шаблоні наприклад). Поки що така поведінка є 
        експериментальною. Якщо буде потреба зменшити трафік, збільшити 
        відгукливість сайту або просто не буде потреби імпортувати всі локалі
        слоту - ми можемо змінити цю поведінку.
        
        Поки що стеки для слотів створюються 
        і у ViewType.kw  і у ViewType.vdata
        Така поведінка є експериментальною.
        Надалі будемо дивитися, від якого стеку є користь
        kw - стек, що активно використовується віджетами
        vdata - стек прямого виводу з ViewType на клієнта, 
        тому обидва варіанти, в принципі, мають право на існування.
        """
        if not self.kw.get('slots', False):
            """
            Створюємо стек для слотів у ViewType.kw, 
            якщо його раптом не забув створити __init__
            (або забули дати вказівку init-у ми;))
            """
            self.kw['slots'] = {}
        self.kw['slots']['strings'] = {}
        self.kw['slots']['texts'] = {}
            
        if not self.vdata.get('slots', False):
            """
            Створюємо стек для слотів у ViewType.vdata, 
            якщо його раптом не забув створити __init__
            (або забули дати вказівку init-у ми;))
            """
            self.vdata['slots'] = {}
        self.vdata['slots']['strings'] = {}
        self.vdata['slots']['texts'] = {}
        
        """ Спочатку рядкові... """
        try:
            for i in Q(slotsdb.FreeString)\
                .filter(slotsdb.FreeString.active == True):
                self.vdata['slots']['strings'][i.mnemo] = \
                    self.kw['slots']['strings'][i.mnemo] = i
        except:
            pass
            
        """... А потім і текстові """
        try:
            for i in Q(slotsdb.FreeText)\
            .filter(slotsdb.FreeText.active == True):
                self.vdata['slots']['texts'][i.mnemo] = \
                    self.kw['slots']['texts'][i.mnemo] = i
        except:
            pass
'''
