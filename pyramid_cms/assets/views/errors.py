# -*- coding: utf-8 -*-

class WidgetNotFoundError(Exception):
    
    def __init__(self,val):
        self.val = val

    def __str__(self):
        return "Widget object {0} was not found!".format(repr(self.val))
