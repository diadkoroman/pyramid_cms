# -*- coding: utf-8 -*-
import os
from pyramid.httpexceptions import HTTPForbidden
from pyramid.renderers import render
from pyramid.response import Response

from pyramid_cms import admin
from pyramid_cms.assets.views import errors
from pyramid_cms.assets.views.ViewType import ViewType

""" ViewType class is a basys for every View implementation. """


class AdminViewType(ViewType):
    """ Package name should be rewritten for each project-sided view """
    pass
