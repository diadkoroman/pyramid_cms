# -*- coding: utf-8 -*-
import os
from pyramid.httpexceptions import HTTPForbidden
from pyramid.renderers import render
from pyramid.response import Response

from pyramid_cms import admin
from pyramid_cms.assets.views import errors
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.views.ViewType import ViewType

from pyramid_cms.apps.roots import dbmodels as rootsdb

""" ViewType class is a basys for every View implementation. """


class ProjectViewType(ViewType):

    """ Package name should be rewritten for each project-sided view """
    abs_tpl_path = None
    node = None
    package_name = admin.project.config.packagename
    permissions = []
    #test = True
    test_data = 'test'
    test_xml_document = False
    xml = False
    vdata = None

    def __init__(self, req):

        self.req = req
        self.rmd = req.matchdict
        self.kw = {}   # kw resources
        self.kw["get"] = req.GET
        self.kw["post"] = req.POST
        self.kw["tail"] = list(req.matchdict.get("tail",[]))
        self.kw["session"] = req.session
        self.kw["node"] = self.node
        self.kw['slots'] = {}
        self.templates = admin.project.config.templates
        """
        Заглушив стек self._widgets, бо в ньому немає видимого сенсу
        """
        # self._widgets = admin.admin.widgets
        # self.vdata = etree.Element("HtmlDocument")
        self.vdata = {}
        self.vdata["package_name"] = self.package_name
        self.vdata['project_name'] = \
                                     getattr(admin.project.config,
                                             'packagename',
                                             False)
        # if self.xml:
        # preprocessing data
        #self.create_root_element()
        self.get_curr_lang()
        self.get_slots()
        self.get_project_verbose_name()
        self.get_pagetitle()
        self.prepare_data()
        self.get_data()
        self.manage_widgets()
        self.get_widgets(**self.kw)
        self.get_template()
        # self.process_vdata()
        
    def get_curr_lang(self):
        """
        Setting current language for each request.
        1. Try to get lang value from tail[0]
        2. If 1. fails - set default lang value
        3. Try to get language item from database
        4. If 3. fails - set default lang mnemo
        """
        #try:
        lmnemo = self.req.matchdict.get('lang', admin.project.config.deflang)
        self.req.curr_lang = Q(rootsdb.Language).filter(rootsdb.Language.mnemo == lmnemo).scalar()
            #if not lang:
            #    lang = self.lang_by_deflang()
            #    if not lang:
            #        lang = admin.admin.config.deflang
            # self.req.curr_lang = lang
        #except:
        #    transaction.abort()
        #    self.get_curr_lang()
        
    def get_project_verbose_name(self):
        self.vdata['project_name'] = super().get_project_verbose_name()
        
    def get_pagetitle(self):
        """
        Окрема реалізація тайтлу для сторінки на стороні проекту.
        Назву проекту отримуємо зі словника у налаштуваннях за індексом мови.
        Потім, при можливості, додаємо назву самої сторінки.
        """
        self.vdata['pagetitle'] = super().get_pagetitle()
        

    def create_template(self):
        """ Method to create templates """
        abspath = os.path.join(
            admin.project.config.templates[
                admin.project.config.templates_default_dir
                ], self.template)
        if self.xml:
            # generate template file for xslt
            self.generate_xsl_file(abspath)
        else:
            with open(abspath, 'w') as f:
                header = '<!-- {0} page template (start)-->'.format(self.template)
                content = '<!-- SET CONTENT HERE -->'
                footer = '<!-- {0} page template (end) -->'.format(self.template)
                f.write(header + '\n\n' + content + '\n\n' + footer)
        return abspath
