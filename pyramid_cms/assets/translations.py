# -*- coding: utf-8 -*-
"""
Translation factories kit.
"""

from pyramid.i18n import TranslationStringFactory

# Strings for models and all model-sided items
_db_ts = TranslationStringFactory("dbmodels")
# String for forms
_form_ts = TranslationStringFactory("forms")
