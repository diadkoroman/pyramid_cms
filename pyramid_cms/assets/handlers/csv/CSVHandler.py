# -*- coding: utf-8 -*-

"""
Handler for .csv files.
Functionality:
1. Get data from input csv and put it into the temporary file
2. Parse data as dict or as list from the temporary file
3. Set up data output in:
    - dict
    - list
    - json
    - xml
"""
import os
import time
import csv

from pyramid_cms import admin


class BaseCSVHandler(object):
    """
    Class imlementing basic functionality
    """
    """
    input_data is a data dict from any form filefield
    """
    # result of reading
    csvdata_read = None
    input_data = None
    
    # keys for getting data from input_data dict
    file_key = None
    filename_key = None
    
    # reader type for csv file
    reader = None
    
    # temporary dir for saving csv data in temp files
    tmp_dir = None
    # name of temporary file
    tmp_filename = None
    # path to temporary file
    tmp_filepath = None
    # reading option for tmp_file
    tmp_ropt = 'r'
    # writing option for tmp_file
    tmp_wopt = 'wb'
    # allowed mime types
    allowed_mime_types = admin.admin.config.csv_mime
    
    # allowed csv extensions
    allowed_csv_exts = admin.admin.config.csv_ext

    def __init__(self, **kw):
        """
        Keyword arguments is setting up 
        for exisiting class attributes only.
        """
        # setting stack for data read
        self.csvdata_read = []
        # setting up temporary dir
        self.tmp_dir = admin.admin.config.tmp_dir
        
        for k, v in kw.items():
            if hasattr(self, k):
                setattr(self, k, v)

    def is_csv_file(self, obj):
        # check for .csv extension
        if self.allowed_csv_exts:
            if iter(self.allowed_csv_exts):
                if os.path.splitext(obj)[1] in self.allowed_csv_exts:
                    return True
        return False
        
    def read_csv_data(self):
        # reading csv data from file
        with open(self.tmp_filepath, 'r') as f:
            if not self.reader:
                self.csvdata_read = [row for row in csv.reader(f)]
            if self.reader == 'dict':
                self.csvdata_read = [row for row in csv.DictReader(f)]
        # !!! deleting tmp file must be inserted here before return
        self.delete_tmp_file()
        return self.csvdata_read
        
    def set_tmp_filename(self):
        # setting up temporary file name
        self.tmp_filename = 'tmp_{0}.csv'.format(str(int(time.time())))
        return self.tmp_filename
        
    def delete_tmp_file(self):
        try:
            os.remove(self.tmp_filepath)
        except:
            pass
        
    def write_tmp_file(self, csvdata):
        # writing input csv data into the temporary file
        self.tmp_filepath = os.path.join(self.tmp_dir, self.set_tmp_filename())
        with open(self.tmp_filepath,'wb') as tmpf:
            tmpf.write(csvdata.read())
    
    def read_data(self):
        """
        1. Check if input data is csv file
        """
        try:
            data = self.input_data.__dict__
            if self.is_csv_file(data[self.filename_key]):
                # if data is a csv file - write temp file...
                self.write_tmp_file(data[self.file_key])
                # ... read csv data from temp file...
                return self.read_csv_data()
        except Exception as e:
            raise e

class WTFormsCSVHandler(BaseCSVHandler):
    file_key = 'file'
    filename_key = 'filename'
