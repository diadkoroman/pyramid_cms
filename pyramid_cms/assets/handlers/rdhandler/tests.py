# -*- coding: utf-8 -*-

"""
Тести для перевірки роботи RemoteSourceDataHandler
"""

import unittest

from pyramid_cms.assets.handlers.rdhandler import RemoteSourceDataHandler

class RemoteSourceDataHandlerTests(unittest.TestCase):
    """
    Тестування роботи обробника даних
    """
    def test_datahandler_creation(self):
        # перевіряємо, чи створюється екземпляр
        try:
            dh = RemoteSourceDataHandler(
                request_url='http://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
            print('{} instance has been created'.format(dh.__class__))
        except Exception as e:
            print(e)
            
    def test_set_incorrect_attribute(self):
        # перевряємо можливість встановити неіснуючий атрибут
        try:
            dh = RemoteSourceDataHandler(
                request_url='http://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
            dh.fakeattr = 'fakeattr'
            print(dh.fakeattr)
        except Exception as e:
            print(e)
            
    def test_attributes(self):
        # перевряємо значення атрибутів
        dh = RemoteSourceDataHandler(
            request_url='http://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
        for k, v in dh.__dict__.items():
            print(k + ' : ' + str(v))
        print(dh.proxy_address)
            
    def test_get_data(self):
        # перевіряємо можливість отримання даних з джерела
        try:
            dh = RemoteSourceDataHandler(
                request_url='http://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
            data = dh.get_data()
            print(data)
        except Exception as e:
            print(e)
