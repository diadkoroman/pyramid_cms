# -*- coding: utf-8 -*-
"""
Головний клас обробника віддалених джерел даних
"""

import re
import json
from urllib import request
from urllib import parse

from pyramid_cms.assets.handlers.rdhandler import local_settings as ls

class RemoteSourceDataHandler(object):
    """
    Базовий клас обробнка даних, отриманих із
    віддалених джерел.
    """

    # урл для запиту
    """
    УРЛ для запиту віддаленого джерела формується локально для кожного 
    екземпляра класу при його ініціалізації.
    """
    request_url = None

    # Тип вхідних даних за умовчанням
    """
    Тип вхідних даних за умовчанням формується на рівні локальних налаштувань,
    але для кожного екземпляра класу може бути переписаний при ініціалізації
    екземпляра
    """
    inp_type = ls.DEFAULT_INP_TYPE

    # Тип вихідних даних за умовчанням
    """
    Якщо цей параметр має значення, то перед тим як віддати дані
    через основний метод, вони попередньо обробляються для отримання
    запитаного типу вихідних даних.
    У іншому випадку віддаються дані у одному з видів iterables,
    що застосовуються у python
    """
    outp_type = ls.DEFAULT_OUTP_TYPE
    
    # Дані, підготовлені відповідно до директиви outp_type
    """
    Теоретично дані на цьому етапі вже можна використовувати для виводу,
    проте для чистоти API рекомендується користуватися виключно
    виводом processed_data.
    Тим більше що _prepared_data не містить результатів обробки даних
    можливими додатковими методами-фільтрами, і через це трактується
    як перехідний стан даних між "сирими" і "готовими".
    """
    _prepared_data = None

    # Оброблені дані, готові для видачі клієнтові
    """
    У даному випадку клієнтом може виступати і як WSGI-додаток, що виводить
    дані у браузер, так і інший додаток, що отримує ці дані для своїх потреб.
    """
    processed_data = None

    # Адреса проксі-сервера, що використовується для мережевого з’єднання
    """
    Якщо цей параметр має значення, то при ініціалізації екземпляра класу
    запускається спеціальний метод set_proxy_handler, котрий створює
    обробник проксі-сервера для з’єднання.
    """
    proxy_address = ls.PROXY_ADDRESS

    # Необроблені дані (не використовується напряму)
    _raw_data = None

    def __init__(self, **kw):
        """
        Ініціалізатор отримує довільний набір keyword-аргументів.
        Проте у процесі призначення їх атрибутами об’єкта він аналізує наявність
        такого атрибута у батьківського класу. Якщо keyword-аргумент не 
        входить до переліку наявних атрибутів - видаємо помилку.
        """
        for k, v in kw.items():
            self.__setattr__(k, v)
        """
        Є потреба реініціалізувати деякі атрибути, повернувши їх 
        у початковий стан.
        """
        self.processed_data = None
        self._raw_data = None
        """
        Обробник проксі формується, якщо введено значення для атрибута
        proxy_address
        """
        if self.proxy_address:
            self.set_proxy_handler()

    '''
    def __setattr__(self, k, v):
        """
        Призначити можна тільки той атрибут, що визначений для 
        батьківського класу.
        При введенні неіснуючого атрибута виводиться помилка
        """
        if not k in self.__dict__:
            raise AttributeError
        super().__setattr__(k, v)
    '''
        
    def get_data(self, **kw):
        """
        Запит даних.
        Залежно від значення self.outp_type відпрацьовує потрібний метод, що
        проводить коректну підготовку даних до подальшого опрацювання.
        kw призначений для параметрів, котрі регулюватимуть подальшу обробку даних.
        """
        self._raw_data = request.urlopen(self.request_url)
        self._prepared_data = self._raw_data
        if self.outp_type:
            try:
                getattr(self, 'process_{0}'.format(self.outp_type))(**kw)
            except Exception as e:
                raise e
        if kw:
            """
            Якщо задано модифікатори, то намагаємося запустити відповідні
            методи. Помилки відпацьовувати в даному контексті не потрібно.
            1. Переформатовуємо self.processed_data у dict
            2. Запускаємо усі потрібні методи. Кожен метод додаватиме у
            self.processed_data власну секцію "імені себе"
            """
            self.processed_data = {}
            for k, v in kw.items():
                if v:
                    try:
                        getattr(self, 'get_{0}'.format(k))(k)
                    except:
                        pass
            if not self.processed_data:
                """
                Якщо жодної секції не було створено під час роботи 
                методів-модифікаторів(неіснуючі методи або помилки в їх роботі)
                то підготовані для обробки дані передаються у вивід без змін.
                """
                self.processed_data = self._prepared_data
        else:
            # немає модифікаторів - дані передаються у вивід без змін
            self.processed_data = self._prepared_data
        return self.processed_data
        
    def process_json(self, **kw):
        """
        Обробка отриманих даних, конвертування з json
        """
        data = str(self._raw_data.read().decode('UTF-8'))
        self._prepared_data = json.loads(data)
        
    def process_xml(self, **kw):
        """
        Обробка отриманих даних, конвертування з xml
        """
        pass
        
    def set_proxy_handler(self):
        """
        У випадку необхідності сторюємо обробник входу через проксі
        """
        if isinstance(self.proxy_address, (dict,)):
            proxy = request.ProxyHandler(self.proxy_address)
            auth = request.HTTPBasicAuthHandler()
            opener = request.build_opener(proxy, auth, request.HTTPHandler)
            request.install_opener(opener)
