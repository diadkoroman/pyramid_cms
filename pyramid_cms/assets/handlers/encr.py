# -*- coding: utf-8 -*-
import hashlib
import random
import time

def encr1(string):
    string = hashlib.sha384(string.encode('UTF-8')).hexdigest()
    return hashlib.sha512(string.encode('UTF-8')).hexdigest()
    
def encr2(string):
    string = hashlib.sha384(string.encode('UTF-8')).hexdigest()
    return hashlib.sha384(string.encode('UTF-8')).hexdigest()
    
def encr3(string):
    string = hashlib.sha384(string.encode('UTF-8')).hexdigest()
    return hashlib.sha256(string.encode('UTF-8')).hexdigest()
    
    
# TOKENIZATION
random_words = ['moon','slow','sunny','winter','scale','short']

def tokenize(string):
    date = str(time.time())
    word = random_words[random.randrange(0,len(random_words) - 1)]
    resstr = date + string + word
    return encr3(resstr)
