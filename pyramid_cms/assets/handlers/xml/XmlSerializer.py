# -*- coding: utf-8 -*-
'''
"""
Xml serializers for different needs
"""
import io

""" Trying import lxml module """
try:
    from lxml import etree
    from lxml import html
except ImportError:
    pass

class BaseXmlSerializer(object):
    """
    Base class for xml-serializers
    """
    include = []
    exclude = []

    def __init__(self, input_data, parent=None, include=[], exclude=[]):
        """
        self.input_data is a stack for input
        We need to have list of input objects any way 
        so if input_data is not a list itself we put it into the list
        manually.
        """
        self.input_data = input_data if isinstance(input_data,(list,tuple)) else [input_data]
        self.include = include
        self.exclude = exclude
        [self.create_node(i, parent=parent) for i in self.input_data]

    def _get_attribute_names(self, obj):
        """
        Get attribute names for input_data
        obj is an object to get its attributes excluding privates
        """
        if self.include:
            return [attr for attr in dir(obj) if attr in self.include]
        elif self.exclude:
            return [attr for attr in dir(obj) if not attr in self.exclude]

    def create_node(self, obj, name=None, parent=None):
        """
        Creates xml-node.
        obj - data for node creation
        name - name of node(optional) 
        obj.__class__.__name__ is used if nam is not provided
        
        parent - parent node.
        node is created as root element if not provided
        else - as a subelement
        """
        
        # create name for node
        if not name:
            name = obj.__class__.__name__
        else:
            name = str(name)

        # create node
        if parent is not None:
            #if isinstance(parent.__class__, (etree.Element, etree.SubElement)):
            node = etree.SubElement(parent, name)
        else:
            node = etree.Element(name)
            
        # find its attributes
        attrs = self._get_attribute_names(obj)
        
        for attr in attrs:
            a = getattr(obj, attr, False)
            if not isinstance(a, (str, int, float)):
                try:
                    [self.create_node(i, name=attr, parent=node) for i in a]
                except:
                    self.create_node(a, name=attr, parent=node)
            else:
                etree.SubElement(node, attr).text = str(a)


class SQLAlchemyModelXmlSerializer(BaseXmlSerializer):
    """
    Xml serializer for sqlalchemy db models
    """
    pass
    
class WTFormsFormXmlSerializer(BaseXmlSerializer):
    """
    Xml serializer for wtforms form
    """
    include = \
        [
            'id', 
            'label', 
            'description',
            'name', 
            'default',
            'data',
            'errors',
            'value'
        ]
    
    def __init__(self, input_data, parent=None, include=[], exclude=[]):
        """
        self.input_data is a stack for input
        We need to have list of input objects any way 
        so if input_data is not a list itself we put it into the list
        manually.
        """
        form_element = etree.SubElement(parent, 'Form')
        
        self.input_data = [i for i in input_data]
        self.include.append(include)
        self.exclude = exclude
        [self.create_node(i, parent=form_element) for i in self.input_data]

    def create_node(self, obj, name=None, parent=None):
        """
        Creates xml-node.
        obj - data for node creation
        name - name of node(optional) 
        obj.__class__.__name__ is used if nam is not provided
        
        parent - parent node.
        node is created as root element if not provided
        else - as a subelement
        """
        
        # create name for node
        if not name:
            name = obj.__class__.__name__
        else:
            name = str(name)
        
        # create node
        if parent is not None:
            #if isinstance(parent.__class__, (etree.Element, etree.SubElement)):
            node = etree.SubElement(parent, name)
        else:
            node = etree.Element(name)
            
        # find its attributes
        attrs = self._get_attribute_names(obj)
        
        for attr in attrs:
            a = getattr(obj, attr, False)
            if not isinstance(a, (str, int, float)):
                try:
                    [self.create_node(i, name=attr, parent=node) for i in a]
                except:
                    self.create_node(a, name=attr, parent=node)
            else:
                etree.SubElement(node, attr).text = str(a)
                
class WTFormsFormSimpleXMLSerializer(object):
    def __init__(self, input_data, parent=None):
        #parser = etree.HTMLParser()
        
        self.input_data = [i for i in input_data]
        self.output_data = ['<form method="post" action="" enctype="multipart/form-data">']
        for i in input_data:
            self.output_data.append('<fieldgroup for="'+ i.name +'">')
            self.output_data.append(i.label())
            self.output_data.append(i())
            self.output_data.append('<errors>')
            self.output_data.append(','.join([err for err in i.errors]))
            self.output_data.append('</errors>')
            self.output_data.append('</fieldgroup>')
        self.output_data.append('</form>')
        self.f = html.fromstring(''.join(self.output_data))
        #tree   = etree.parse(io.StringIO(''.join(self.output_data)), parser)
        parent.append(self.f)
        
'''
