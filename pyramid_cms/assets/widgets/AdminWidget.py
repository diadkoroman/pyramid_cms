# -*- coding: utf-8 -*-

""" Base widget class for admin side """
from pyramid_cms import admin
from .Widget import Widget


class AdminWidget(Widget):
    """ Base widget class for admin side """

    default_temp_dir_index = 2

    def __init__(self, req, data, **kw):
        self.templates = getattr(admin.admin.config, 'templates', None)
        self.default_mode = 'root_mode'
        super().__init__(req, data, **kw)
