# -*- coding: utf-8 -*-

"""
Core Widget class for both project and admin pages.
"""
try:
    import os
    from lxml import etree
    from pyramid_cms.base import admin

    class XmlWidget(object):
        """ Core Widget class for both project and admin pages. """

        """ public attributes """
        # request
        req = None
        # main data stack
        data = None
        # keyword args
        kw = None

        # modes the Widget works in
        modes = None
        # xml_mode
        xml_mode = False
        # package name
        package_name = None
        
        # widget node
        widget_element = None
        # widget element name for xml_mode
        widget_element_name = 'Widget'

        # path to templates dir (from admin.admin or admin.project)
        templates = None
        
        # class for standard get_items method
        default_class = None
        # get items query.Is set in widget class definition
        get_items_query = None
        # default items for widget
        items = None

        """
        Індекс директорії за умовчанням для зберігання файлів-шаблонів
        віджета. Використовується при створенні шляху до шаблона віджета.
        Віджети адмін-частини та віджети проекту мають різні значення індексів.
        При цьому віджети адмін-частини мають спільну директорію для зберігання,
        зазначену в загальному класі AdminWidget.
        Для ProjectWidget директорія для зберігання віджета зазначається
        індивідуально для кожного класу віджета.
        """
        default_temp_dir_index = 0

        #template name
        template_name = None
        # template relative path(with package_name:)
        template_path = None

        """
        При потребі можна присвоїти нестандартний відносний шлях
        до директорії віджета.
        Якщо цей атрибут має значення, відмінне від False/None, то
        при формуванні шляху до файлу з шаблоном використовується
        воно, а не стандартне значення із self.templates
        """
        template_custom_path = None

        """ private attributes """
        # globally set modes (extensible)
        modes_and_perms = {
            'user_mode':['view'],
            'admin_mode':[],
            'root_mode':['all'],
        }
        # current mode for widget
        current_mode = None
        # default mode for widget
        default_mode = None
        
        # назви користувацьких змінних для wdata['meta']
        wdata_meta = None
        # назви користувацьких змінних для wdata['body']
        wdata_body = None

        def __init__(self, req, data, **kw):
            """ Initialize widget """
            self.req = req

            # we get global datastack from view
            self.data = data

            # self class name becoms a widget`s mnemo
            self.mnemo = self.__class__.__name__.lower()

            # inner widget`s datastack
            self.wdata = self.data[self.mnemo] = {}
            # metadata for widget
            self.wdata['meta'] = {}
            # widget`s data body
            self.wdata['body'] = {}
            # keyword arguments
            self.kw = kw

            self.create_widget_element()

            # its template file name
            self.template_name = '{0}_{1}'.format(
                self.mnemo,
                'widget.xsl')

            self.template_path = self._set_template_path()
            self.wdata['meta']['template'] = self.template_path

            # workon current mode foe widget
            self._set_current_mode()

            # dispatcher
            # self.dispatch()

        def __call__(self):
            """
            up 11.03.2016
            При виклику віджета:
            1. запускається метод, що генерує
            перелік користувацьких змінних для віджета
            2. запускається метод-диспечер для опрацювання даних
            """
            try:
                self.set_wdata()
                self.dispatch()
            except Exception as e:
                raise e
            try:
                getattr(self, self.req.method.lower())()
            except Exception as e:
                raise e

        def _create_template_file(self, absdir):
            """ Create template file if not exists """
            template_path = os.path.join(absdir,self.template_name)
            self.generate_xsl_file(template_path)
            return template_path
            
        def generate_xsl_file(self, abspath, **kw):
            with open(abspath, 'w') as f:
                declaration = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                stylesheet = """<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">"""
                output = """<xsl:output method=\"{output_method}\" doctype-system="" encoding=\"{encoding}\" omit-xml-declaration=\"yes\"/>""".format(
                                                     output_method=\
                                                     kw.get("output_method","html"),
                                                     encoding=\
                                                     kw.get("encoding","UTF-8")
                                                     )
                template = "<xsl:template match=\"/\"></xsl:template>"
                stylesheet_close = "</xsl:stylesheet>"
                f.write(declaration + '\n' + stylesheet + '\n' + output + "\n" + template + "\n" + stylesheet_close)

        def _set_current_mode(self):
            """
            Set current mode for widget.
            We search for proper mode in GET variables.
            If proper mode not found - use default
            """
            for i in self.req.GET:
                if i in self.modes_and_perms:
                    self.current_mode = i
            if not self.current_mode:
                self.current_mode = self.default_mode
                
        def _set_custom_template_path(self):
            """
            Встановлення шляху до шаблоня, якщо для віджета
            вказано особливий шлях для їх розміщення.
            """
            # створюємо повний шлях
            template_path = os.path.join(
                admin.admin.config.basedir,
                self.template_custom_path,
                self.template_name)
            
            # якщо файл існує - просто повертаємо шлях до нього
            if os.path.isfile(template_path):
                return template_path
            # якщо файл не існує - створюємо його і повертаємо шлях
            return self._create_template_file(
                os.path.join(
                    admin.admin.config.basedir,
                    self.template_custom_path))

        def _set_standard_template_path(self):
            """
            Встановлення шляху до шаблона, якщо  для віджета
            вказано стандартний шлях.
            """
            for path in self.templates:
                template_path = os.path.join(path, self.template_name)
                if os.path.isfile(os.path.join(path, self.template_name)):
                    # return template path if exists
                    return template_path
            # create and return template path
            return self._create_template_file(self.templates[self.default_temp_dir_index])

        def _set_template_path(self):
            """
            Setting self.template_relpath(private method).
            We go through list of template dirs.
            If template we need was not found - widget
            creates it automatically.
            """
            
            if self.template_custom_path:
                """
                Гілка для нестандартного шляху.
                """
                return self._set_custom_template_path()
                """
                template_path = os.path.join(
                    admin.admin.config.basedir,
                    self.template_custom_path,
                    self.template_name)
                if os.path.isfile(template_path):
                    return template_path
                return self._create_template_file(
                    os.path.join(
                        admin.admin.config.basedir,
                        self.template_custom_path))
                """
            
            if self.templates:
                """
                Гілка для стандартного шляху.
                """
                """
                for path in self.templates:
                    template_path = os.path.join(path, self.template_name)
                    if os.path.isfile(os.path.join(path, self.template_name)):
                        # return template path if exists
                        return template_path
                        # create and return template path
                return self._create_template_file(self.templates[self.default_temp_dir_index])
                """
                return self._set_standard_template_path()
            return None

        def dispatch(self):
            """
            Method for dispatch requests.
            Customized for each widget
            """
            pass
            
        def get_items(self, return_values=False):
            """
            Standard method for getting default widget items.
            Default widget items is set in Widget.default_class.
            Needs to be overwritten
            """
            if self.get_items_query:
                self.items = self.get_items_query
            if return_values:
                return self.items


        def add_mode_perms(self, modename, add_perms=None):
            """
            Extending permissions set for each mode oof widget.
            Only existing widget modes can be extended
            """
            if modename and add_perms:
                if self.modes_and_perms.get(modename, False):
                    if isinstance(add_perms, (str,)):
                        self.modes_and_perms[modename].append(add_perms)
                    if isinstance(add_perms, (list,tuple)):
                        try:
                            add_perms = list(add_perms)
                        except:
                            pass
                        self.modes_and_perms[modename].extend(add_perms)

        def reassign_template_path(self, new_path):
            """
            Метод, що перепризначає шлях до шаблона віджета.
            new_path - абсолютний або відносний шлях до
            директорії з шаблоном.
            """
            self.wdata['meta']['template'] = self.template_path = os.path.join(new_path, self.template_name)

        def remove_mode_perms(self, modename, remove_perms=None):
            """
            Remove permissions from widget`s mode.
            Only existing modes can be modified.
            """
            if modename and remove_perms:
                if self.modes_and_perms.get(modename, False):
                    if isinstance(remove_perms, (str,)):
                        self.modes_and_perms[modename].remove(remove_perms)
                    if isinstance(remove_perms, (list,tuple)):
                        for i in remove_perms:
                            self.modes_and_perms[modename].remove(i)
                            
        def set_wdata(self):
            """ Розгортання змінних для внутрішнього стека даних """
            # self.wdata['meta']
            if self.wdata_meta:
                for i in self.wdata_meta:
                    if i not in self.wdata['meta'].keys():
                        self.wdata['meta'][i] = None
                        
            # self.wdata['body']
            if self.wdata_body:
                for i in self.wdata_body:
                    if i not in self.wdata['body'].keys():
                        self.wdata['body'][i] = None

        def view(self):
            """ View data """
            pass

        def add(self):
            """ Add data """
            pass

        def edit(self):
            """ Edit data """
            pass

        def delete(self):
            """ Delete data """
            pass
            
        def get(self):
            pass
            
        def post(self):
            pass
            
        def create_widget_element(self):
            """ Create widget root element and append it to root element """
            attrs = \
                {
                    'name': self.__class__.__name__.lower(),
                }
            self.widget_element = \
                etree.SubElement(
                    self.data['document'].find('DocumentBody'), self.widget_element_name, attrs)
except:
    pass
