# -*- coding: utf-8 -*-
import os
""" Base widget class for project side """
from pyramid_cms import admin
from .Widget import Widget


class ProjectWidget(Widget):
    """ Base widget class for project side """

    def __init__(self, req, data, **kw):
        self.templates = getattr(admin.project.config, 'templates', None)
        self.default_mode = 'user_mode'
        super().__init__(req, data, **kw)

    def _set_custom_template_path(self):
        """
        Встановлення шляху до шаблона, якщо для віджета
        вказано особливий шлях для їх розміщення.
        """
        # створюємо повний шлях
        template_path = os.path.join(
            admin.project.config.basedir,
            self.template_custom_path,
            self.template_name)
        
        # якщо файл існує - просто повертаємо шлях до нього
        if os.path.isfile(template_path):
            return template_path
        # якщо файл не існує - створюємо його і повертаємо шлях
        return self._create_template_file(
            os.path.join(
                admin.project.config.basedir,
                self.template_custom_path))
