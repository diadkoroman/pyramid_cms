# -*- coding: utf-8 -*-
"""
Module with Plugin main class
"""
import transaction
from sqlalchemy.sql.expression import asc,desc

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import DBSession, Q
from pyramid_cms.apps.roots import dbmodels as rootsdb

class Plugin(object):
    """
    Base plugin class
    """
    inner_name = None
    langs = None
    langs_count = None
    # Does plugin use locales?
    localized = False
    mnemo = None
    node = None
    packagename = None
    parent = None
    template = None
    ui = None
    
    """
    Стеки, куди додаються форми, що надалі формуватимуть
    Plugin.ui
    Основне призначення стека - розгорнути екземпляр форми
    у її початковому стані, приготувавши її для обробки
    методами _form/_add_form/_edit_form.
    Названі внутрішні методи використовуються методом
    Plugin.ui для виводу актуального типу форми у її актуальному
    стані.
    Теоретично на рівні методів _form/_add_form/_edit_form
    можна встановлювати початкові зачення, дефолтні значення тощо.
    Але концепція FormInit-ів, що була реалізована дещо пізніше,
    дозволяє робити всі додаткові зміни на рівні FormInit-а, тому
    потреби у внесенні змін на рівні _form/_add_form/_edit_form
    більше немає, хоча така можливість і залишається.
    """
    # стек для форми додавання елемента
    _add_form_stack = None
    # стек для форми редагування елемента
    _edit_form_stack = None
    # стек для універсальної форми
    _form_stack = None
    
    #_form = None
    # class to work with in default mode (if need)
    _default_class = None
    _default_form_class = None
    _current_class = None
    widgets = None
    _sections_class = None

    def __init__(self, req=None, section_class=None, parent=None):
        """ Create instance of plugin """
        self.parent = parent
        # use request object got from admin instance
        self._register_section_class(section_class=section_class)
        #self._register_in_tree(parent=parent)
        self._register(parent=self.parent)
        self._bind_node(parent=self.parent)
        # load defaults
        self._load_defaults()
        # load default plugin forms
        self._load_default_forms()
        self.req = req

    def _register_section_class(self, section_class=None):
        """ Register class using to register plugin section in the tree. """
        if section_class:
            self._sections_class=section_class

    def _register_in_tree(self, parent=None):
        """
        Register plugin section in the tree
        At the end of process we get plugin`s node.
        This is need for provoding data into the plugins menu.
        """
        if parent:
            cls = self._sections_class
            DBSession.flush()
            plugin_section = Q(cls)\
                .filter(cls.mnemo == self.mnemo, cls.parent_id == parent.id)
            if plugin_section.count() == 0:
                #with transaction.manager:
                plugin_section = cls()
                plugin_section.inner_name = self.inner_name
                plugin_section.mnemo = self.mnemo
                plugin_section.pagetype_id = parent.pagetype_id
                plugin_section.parent_id = parent.id
                plugin_section.admin_node = parent.admin_node
                plugin_section.permissions = [p for p in parent.permissions]
                plugin_section.active = True
                plugin_section.save(flush=True)
                self.node = plugin_section
            else:
                self.node = plugin_section.first()

    def _register(self, **kw):
        """
        Register TreeNode instance for plugin.
        We try to get node from TreeNode(self._sections_class).
        If mismanaged(node does not exist) - create new TreeNode instance.
        """
        parent = kw.get('parent', None)
        self.node = None
        if parent:
            cls = self._sections_class
            node = Q(cls)\
            .filter(
                cls.mnemo == self.mnemo,
                cls.parent_id == parent.id
            ).first()
            if not node:
                plugin_section = cls()
                plugin_section.inner_name = self.inner_name
                plugin_section.mnemo = self.mnemo
                plugin_section.pagetype_id = parent.pagetype_id
                plugin_section.parent_id = parent.id
                plugin_section.admin_node = parent.admin_node
                plugin_section.permissions = [p for p in parent.permissions]
                plugin_section.active = True
                plugin_section.save()

    def _bind_node(self, **kw):
        """
        Bind treenode instance to plugin
        """
        if not self.node:
            parent = kw.get('parent', None)
            cls = self._sections_class
            if parent:
                self.node = Q(cls)\
                    .filter(
                        cls.mnemo == self.mnemo,
                        cls.parent_id == parent.id
                    ).first()
        return self.node

    def get(self, i):
        """
        Return instance of current workong class.
        Is used for example in plugin edit form widget
        to get instance into the form.
        """
        try:
            return Q(self._current_class).get(i)
        except:
            return None

    @staticmethod
    def get_langs():
        """ Get languages """
        langs = None
        with transaction.manager:
            try:
                class_obj = rootsdb.Language
                langs = Q(class_obj)\
                    .filter(class_obj.active == True)\
                    .order_by(desc(class_obj.deflang))
            except:
                transaction.abort()
        return langs

    @staticmethod
    def count_langs(langs=None):
        """ Count languages """
        if langs:
            return langs.count()
        return 0

    def get_plugin_path(self, req):
        try:
            tail = (
                admin.admin.config.admin_root_dir,
                admin.admin.config.plugins_dir,
                self.mnemo
            )
            return req.route_url('inner', tail=tail)
        except:
            return None

    def get_template(self):
        if self.packagename and self.template:
            return "{0}:{1}".format(self.packagename, self.template)
        return None

    def menu_data(self):
        """ Provide data for admin menu """
        self.node = self._bind_node(parent=self.parent)
        if self.node:
            return [self.node.get_path(), self.node.inner_name]
        return None

    def ui(self, *args, **kw):
        """
        Формуємо дані для користувацького інтерфейсу (UI).
        Якщо плагін має метод _form, який використовується для створення
        універсальної форми, то використовується він.
        Якщо є потреба окремо створювати форми для додавання та редагування
        елементів, то для створення форми використовується відповідно
        _add_form або _edit_form
        """
        req = kw.get('req', None)
        f = self._form(*args, **kw)
        if not f:
            if 'i' in req and 'add' in req:
                f = self._add_form(*args, **kw)
            elif not 'i' in req and 'add' in req:
                f = self._add_form(*args, **kw)
            elif 'i' in req and not 'add' in req:
                f = self._edit_form(*args, **kw)
            else:
                f = None
        return f

    def reload_curr_class(self, class_obj):
        """
        Reloading of _current_class attribute.
        For situations where plugin will need to reload
        its assets depending of mode
        """
        self._current_class = class_obj

    def reload_add_form_stack(self, form_class):
        """ Reloading of _add_form_stack attribute """
        self._add_form_stack = form_class

    def reload_edit_form_stack(self, form_class):
        """ Reloading of _edit_form_stack attribute """
        self._edit_form_stack = form_class
        
    def reload_form_stack(self, form_class):
        """ Reloading _form_stack attribute """
        self._form_stack = form_class

    def reload_all(self, **kw):
        """ Reloading all assets - default realization """
        class_obj = kw.get('class_obj', None)
        add_form_stack = kw.get('add_form_stack', None)
        edit_form_stack = kw.get('edit_form_stack', None)
        form_stack = kw.get('form_stack')
        if class_obj and add_form_stack and edit_form_stack and form_stack:
            """ All assets are needed """
            self.reload_curr_class(class_obj)
            self.reload_add_form_stack(add_form_stack)
            self.reload_edit_form_stack(edit_form_stack)
            self.reload_form_stack(form_stack)


    def _add_form(self, *args, **kw):
        """
        Create and return add form instance.
        Before we got prepared class through
        'modify_additem_form' function.
        This function adds 'names' fields to the main form
        """
        return None
    
    def _edit_form(self, *args, **kw):
        """
        Create and return edit form instance.
        Before we got prepared class through
        'modify_edititem_form' function.
        This function adds 'names' fields to the main form
        """
        return None

    def _form(self, *args, **kw):
        """ Розгортання універсального ui для плагіна """
        f = None
        if self._form_stack:
            f = self._form_stack(*args, **kw)
        return f

    def _load_default_forms(self):
        """
        Method assings default form classes to plugin form stacks
        default realization
        """
        self._form_stack = self._default_form_class

    def _load_defaults(self):
        """ Load defaults """
        # by default _default_class becomes a _current_class
        self._current_class = self._default_class

    def get_items_raw(self):
        """ Отримати перелік елементів (повний список) """
        items = Q(self._default_class)
        return items
        
    def get_items(self):
        """
        Отримати список елементів (з фільтрацією).
        Дефолтна реалізація повертає повний список елементів без 
        будь-якої фільтрації. Щоб відкоригувати вивід, слід створювати
        власну реалізацію цього методу на боці кожного плагіна
        """
        return self.get_items_raw()
