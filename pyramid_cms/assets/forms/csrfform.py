# -*- coding: utf-8 -*-
from datetime import timedelta
from pyramid import session
from wtforms import form
from wtforms.csrf.session import SessionCSRF

class ProtectedForm(form.Form):
    class Meta:
        csrf = True
        csrf_class = SessionCSRF
        csrf_secret=b"""\x9f\x83X\x17\xc4V\xaa\xea\xdc\x01\x88\x98
        \x0c\x1a\xfe\xdc\xb1R\xa7\xa4\x10\x9c\x83;\xbf?
        \x8c\x93\x1d\xf8F\x84cy\xbd|w\x87\x93\xc2\x0c\x8e
        \xc0\t\xf2v\xf1\xf7"""
        csrf_time_limit = timedelta(minutes=10)
