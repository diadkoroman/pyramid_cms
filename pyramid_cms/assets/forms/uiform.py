# -*- coding: utf-8 -*-
"""
UserInterface form modified for usage in Model.ui
and Plugin.ui
"""
from wtforms_alchemy import model_form_factory
from pyramid_cms.assets.forms import ProtectedForm

""" Базова форма моделі із доданим до неї CSRF-захистом """
UIForm = model_form_factory(base=ProtectedForm)

"""
Додатково створюємо трекер для відстежування помилок,
специфічних для нашої архітерктури форм
"""
UIForm.errors_tracker = None
