# -*- coding: utf-8 -*-

""" Form initializer """

class FormInit(object):
    """
    Ініціалізатор форми - створює екземпляр
    форми і становлює всі необхідні змінні, готуючи
    таким чином форму до виводу користувачеві.
    """
    form_class = None
    f = None
    fkw = {}
    
    def __init__(self, form_class=None):
        self.fkw = {}
        """
        form_class встановлюється через ініт тільки у випадках, коли 
        не визначено FormInit.form_class і через ініт передано
        значення для цієї змінної.
        """
        if not self.form_class and form_class:
            self.form_class = form_class

    def __call__(self, *args, **kw):
        #self.set_form_class_vars()
        """ Створюємо екземпляр форми"""
        self.f = self.form_class(*args, **kw)
        """
        Отримуємо сюди kw['req'] з форми,
        бо це єдиний спосіб отримувати змінні наскізно
        із форми у ініціалізатор форми.
        """
        self.req = kw.get('req', None)
        """ Встановлюємо усі необхідні змінні для форми """
        
        self.set_form_vars()
        return self.f
        
    def set_form_class_vars(self):
        pass

    def set_form_vars(self):
    #    """ set form variables """
        pass
