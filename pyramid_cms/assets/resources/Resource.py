# -*- coding: utf-8 -*-
from pyramid.httpexceptions import HTTPNotFound
from pyramid.security import Allow,Everyone

from pyramid_cms import admin
from pyramid_cms.assets.security import USERS, GROUPS
from pyramid_cms.apps.auth import dbmodels

class Resource(object):

    def __acl__(self):
        '''
        Генеруємо ACL (Access Control List)
        Групи отримуємо з БД
        '''
        try:
            groups = dbmodels.Group.filter_by(active = True)
            self.gp = []
            self.gp.append([Allow, Everyone, "view"])
            for group in groups:
                for perm in group.perms:
                    self.gp.append([Allow,'group:{0}'.format(group.name),perm.name])
            return self.gp
        except:
            pass

    def __init__(self,req):
        # request
        self.req = req
        # request.matchdict elements
        self.rmd = req.matchdict
        self.active_users = dbmodels.User.filter_by(active=True)
        # get users and groups
        self._get_users_and_groups(True)


    def _get_users_and_groups(self,init):
        '''
        Get all active users and groups
        '''
        if init:

            for i in self.active_users:
                admin.security.users[i.login] = USERS[i.login] = i.password
                admin.security.groups[i.login] = GROUPS[i.login] = ['group:{0}'\
                                                 .format(group.name) \
                                                 for group in i.groups]
