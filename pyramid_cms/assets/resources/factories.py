# -*- coding: utf-8 -*-
from pyramid_cms.assets.resources.Resource import Resource

def resource_factory(req):
    return Resource(req)
