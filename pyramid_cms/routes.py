# -*- coding: utf-8 -*-
"""
Module sets routes for pyramid_cms
"""

from pyramid_cms import admin


def add_static_routes(config):
    """ Adding static routes """
    config.add_static_view("bootstrap",
                           "{0}:static/bootstrap/3.1.1"
                           .format(admin.admin.config.packagename),
                           cache_max_age=3600)
    config.add_static_view("f6",
                           "{0}:static/f6"
                           .format(admin.admin.config.packagename),
                           cache_max_age=3600)
    config.add_static_view("css",
                           "{0}:static/css"
                           .format(admin.admin.config.packagename),
                           cache_max_age=3600)
    config.add_static_view("jss",
                           "{0}:static/jss"
                           .format(admin.admin.config.packagename),
                           cache_max_age=3600)
    config.add_static_view("jquery",
                           "{0}:static/jss/1.11.3"
                           .format(admin.admin.config.packagename),
                           cache_max_age=3600)
    config.add_static_view("ckeditor",
                           "{0}:static/ckeditor"
                           .format(admin.admin.config.packagename),
                           cache_max_age=3600)
    config.add_static_view("datetimepicker",
                           "{0}:static/datetimepicker"
                           .format(admin.admin.config.packagename),
                           cache_max_age=3600)


def routes(config):
    """ Routes for pyramid_cms """
    # baseurl = "/admin"
    add_static_routes(config)
    # config.add_route("ajax_json", "/ajax.json")
    config.add_route("index_localized", "/{lang:[a-z]{2,3}}/")
    config.add_route("index", "/")
    config.add_route("inner_localized", "/{lang:[a-z]{2,3}}/*tail")
    config.add_route("inner", "/*tail")

    # config.add_route("admin_home",
    #                 "{0}/".format(baseurl))
    # config.add_route("admin_content_types",
    #                 "{0}/content/types/".format(baseurl))
    # config.add_route("admin_content_types_add",
    #                 "{0}/content/types/add/".format(baseurl))
