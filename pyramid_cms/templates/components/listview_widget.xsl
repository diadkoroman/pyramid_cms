<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" doctype-system="" encoding="UTF-8" omit-xml-declaration="yes"/>
    <xsl:template name="ListView">
        <xsl:for-each select="//Widget[@name='listview']/ItemsContainer">
        <div class="row">
            <div class="col-md-6">
                <h4>
                    <xsl:value-of select="./ItemsTable/Name"/>
                </h4>
                <xsl:if test="count(./Options/Option)>0">
                    <xsl:for-each select="./Options/Option">
                        <a href="{./Element[1]}">
                            <xsl:value-of select="./Element[2]"/>
                        </a>
                    </xsl:for-each>
                </xsl:if>
            </div>
        </div>
        <hr></hr>
        
        <!---->
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <xsl:if test="./ItemsTable/Editable/text()='1'">
                                <th>Edit</th>
                            </xsl:if>
                            <xsl:for-each select="./ItemsTable/ColumnsList/ColName">
                                <th>
                                    <xsl:value-of select="."/>
                                </th>
                            </xsl:for-each>
                            <xsl:if test="./ItemsTable/Deleteable/text()='1'">
                                <th>Delete</th>
                            </xsl:if>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="./ItemsTable/ItemsList/Item">
                            <tr>
                                <xsl:if test="//ItemsContainer/ItemsTable/Editable/text()='1'">
                                    <th>
                                        <a href="{./EditPath/text()}">
                                        Edit
                                        </a>
                                    </th>
                                </xsl:if>
                                <xsl:for-each select="./Element">
                                    <td>
                                        <xsl:value-of select="."/>
                                    </td>
                                </xsl:for-each>
                                <xsl:if test="//ItemsContainer/ItemsTable/Deleteable/text()='1'">
                                    <th>
                                        <a href="{./DeletePath/text()}">
                                        Delete
                                        </a>
                                    </th>
                                </xsl:if>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
            </div>
        </div>
        <!---->
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
