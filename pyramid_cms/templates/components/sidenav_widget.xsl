<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" doctype-system="" encoding="UTF-8" omit-xml-declaration="yes"/>
    <xsl:template name="SideNav">
        <ul class="menu vertical">
            <li>
                <h5 class="menu-category">
                    Plugins
                    <span class="caret"></span>
                </h5>
                <ul class="menu vertical scrolling-area m0 p0">
                    <xsl:for-each select="//Widget[@name='sidenav']/SideNav/Node[@categ='plugins']">
                    <li>
                        <xsl:choose>
                            <xsl:when test="./Active/text()='1'">
                                <a class="active" href="{./Element[1]}">
                                    <xsl:value-of select="./Element[2]"/>
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                <a href="{./Element[1]}">
                                    <xsl:value-of select="./Element[2]"/>
                                </a>
                            </xsl:otherwise>
                        </xsl:choose>
                    </li>
                    </xsl:for-each>
                </ul>
                <hr/>
            </li>
            <li>
                <h5 class="menu-category">
                    Content Types
                    <span class="caret"></span>
                </h5>
                <ul class="menu vertical scrolling-area m0 p0">
                    <xsl:for-each select="//Widget[@name='sidenav']/SideNav/Node[@categ='content_types']">
                    <li>
                        <xsl:choose>
                            <xsl:when test="./Active/text()='1'">
                                <a class="active" href="{./Element[1]}">
                                    <xsl:value-of select="./Element[2]"/>
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                <a href="{./Element[1]}">
                                    <xsl:value-of select="./Element[2]"/>
                                </a>
                            </xsl:otherwise>
                        </xsl:choose>
                    </li>
                    </xsl:for-each>
                </ul>
                <hr/>
            </li>
        </ul>
        <script type="text/javascript" src="/jss/sidenav_engine.js">&#160;</script>
    </xsl:template>
</xsl:stylesheet>
