<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" doctype-system="" encoding="UTF-8" omit-xml-declaration="yes"/>
<xsl:template name="TopNav">
    <!-- topnav -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <!---->
            <div class="navbar-header">
                <a class="navbar-brand">PCMS</a>
            </div>
            <!---->
            <!---->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <xsl:for-each select="//Widget[@name='topnav']/TopNav/Node">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                <xsl:value-of select="./NodeText"/>
                                <span class="caret"></span>
                            </a>
                            <xsl:if test="count(./SubNode)>0">
                            <ul class="dropdown-menu">
                                <xsl:for-each select="./SubNode">
                                    <li>
                                        <a href="{./Element[1]/text()}">
                                            <xsl:value-of select="./Element[2]/text()"/>
                                        </a>
                                    </li>
                                </xsl:for-each>
                            </ul>
                            </xsl:if>
                        </li>
                    </xsl:for-each>
                </ul>
            </div>
            <!---->
        </div>
    </nav>
    <!-- topnav(end) -->
</xsl:template>
</xsl:stylesheet>
