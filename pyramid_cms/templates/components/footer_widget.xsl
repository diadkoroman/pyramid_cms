<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" doctype-system="" encoding="UTF-8" omit-xml-declaration="yes"/>
    <xsl:template name="Footer">
        <div class="row">
            <hr/>
            <div class="col-md-4">
                <p class="text-info">
                    <small>
                    <xsl:value-of select="//Widget[@name='footer']/Developer"/>, 
                    <xsl:value-of select="//Widget[@name='footer']/Developed-Year"/>
                    </small>
                </p>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>
