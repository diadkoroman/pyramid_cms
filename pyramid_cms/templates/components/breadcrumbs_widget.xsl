<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" doctype-system="" encoding="UTF-8" omit-xml-declaration="yes"/>
    <xsl:template name="Breadcrumbs">
        <xsl:if test="count(//Widget[@name='breadcrumbs'])>0">
            <nav aria-label="You are here:" class="breadcrumb-nav">
                <ol class="breadcrumb">
                    <xsl:for-each select="//Widget[@name='breadcrumbs']/BreadcrumbsContainer/Node">
                        <li>
                            <xsl:choose>
                                <xsl:when test="./Active/text()='1'">
                                    <a href="{./Path}">
                                        <xsl:value-of select="./Name"/>
                                    </a>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="./Name"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </li>
                    </xsl:for-each>
                </ol>
            </nav>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
