<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" doctype-system="" encoding="UTF-8" omit-xml-declaration="yes"/>
    <xsl:template name="CreateUserForm" match="//Widgets/Widget[@type='Forms'][@name='CreateUser']">
        <form 
        method="{//Widgets/Widget[@type='Forms'][@name='CreateUser']/MetaData/Method/text()}" 
        action="{//Widgets/Widget[@type='Forms'][@name='CreateUser']/MetaData/Action/text()}">
            <xsl:for-each select="//Widgets/Widget[@type='Forms'][@name='CreateUser']/Body/Field">
                <xsl:value-of select="./Label/text()" disable-output-escaping="yes"/>
                <xsl:value-of select="./Code/text()" disable-output-escaping="yes"/>
            </xsl:for-each>
            <button class="small button small-3 small-push-9" type="submit">
                Create Superuser
            </button>
        </form>
    </xsl:template>
    
    <xsl:template match="//Widgets/Widget[@type='Forms'][@name='Login']">
        <form method="post" action="">
            <xsl:for-each select="./Body/Field">
                <xsl:copy-of select="./Label"/>
                <input type="{@type}" name="{@name}"/>
            </xsl:for-each>
            <button class="small button small-3 small-push-9" type="submit">
                Sign In
            </button>
        </form>
    </xsl:template>
    
</xsl:stylesheet>
