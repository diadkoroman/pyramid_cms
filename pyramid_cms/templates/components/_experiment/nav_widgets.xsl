<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="TopNav" match="Widgets/Widget[@type='Navigators'][@name='TopNav']">
    <ul class="nav navbar-nav">
    <xsl:for-each select="Item">
        <!--<li class="dropdown">
            <a class="dropdown-toggle" 
            data-toggle="dropdown" 
            role="button" 
            aria-haspopup="true" 
            aria-expanded="false" 
            href="{@href}">
                <xsl:value-of select="text()"/>
                <span class="caret"></span>
            </a>
            <xsl:if test="count(./Item)>0">
                <ul class="dropdown-menu">
                    <xsl:for-each select="./Item">
                        <li>
                            <a href="{@href}"><xsl:value-of select="text()"/></a>
                        </li>
                    </xsl:for-each>
                </ul>
            </xsl:if>
        </li>-->
        <xsl:if test="count(./Item) > 0">
            <xsl:call-template name="topnav-dropdown-item"/>
        </xsl:if>
    </xsl:for-each>
    </ul>
</xsl:template>

<!-- Menu subelements template -->
<xsl:template name="topnav-dropdown-item">
    <li class="dropdown">
        <a class="dropdown-toggle" 
        data-toggle="dropdown" 
        role="button" 
        aria-haspopup="true" 
        aria-expanded="false" 
        href="{@href}">
        <xsl:value-of select="text()"/>
        <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
            <xsl:for-each select="./Item">
                <li>
                    <a href="{@href}"><xsl:value-of select="text()"/></a>
                </li>
            </xsl:for-each>
        </ul>
    </li>
</xsl:template>

</xsl:stylesheet>
