<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:import href="./form_widgets.xsl"/>
    <xsl:template name="WelcomeScreen" match="//Widgets/Widget[@type='Teasers'][@name='Welcome']">
        <xsl:variable name="mainclass" select="./MetaData/Class/text()"></xsl:variable>
        <div class="row">
            <div class="{$mainclass}">
                <h1><xsl:value-of select="Title"/></h1>
                <p><xsl:value-of select="./SubTitle"/></p>
            </div>
        </div>
        
        <xsl:for-each select="./Sections/Section">
        <div class="row">
            <div class="{$mainclass}">
                <div class="callout">
                    <h3><xsl:value-of select="./Title"/></h3>
                    <p><xsl:value-of select="./Text"/></p>
                    <!---->
                    <xsl:if test="Widget[@name='CreateUser']">
                        <xsl:call-template name="CreateUserForm"/>
                    </xsl:if>
                    <!---->
                    <xsl:if test="count(Buttons/Button)>0">
                    <div class="row">
                        <div class="small-3 small-push-9 columns">
                            <!--<div class="small button-group">-->
                                <xsl:for-each select="Buttons/Button">
                                    <a class="small button {@class_} small-12" href="{@href}">
                                        <xsl:value-of select="."/>
                                    </a>
                                </xsl:for-each>
                            <!--</div>-->
                        </div>
                    </div>
                    </xsl:if>
                </div>
            </div>
        </div>
        </xsl:for-each>
        
    </xsl:template>

</xsl:stylesheet>
