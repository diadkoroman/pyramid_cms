<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:import="./navigators.xsl"/>
    <xsl:template name="NavBar">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <!-- navbar-header -->
                <div class="navbar-header">
                    <a class="navbar-brand" href=""></a>
                </div>
                <!-- navbar-header (end) -->

                <!---->
                <div class="collapse navbar-collapse">
                
                </div>
                <!---->
        
            </div>
        </nav>
    </xsl:template>
</xsl:stylesheet>
