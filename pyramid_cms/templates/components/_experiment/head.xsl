<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template name="CSSFiles">
        <link rel="stylesheet" type="text/css" href="/f6/css/foundation.min.css"/>
        <link rel="stylesheet" type="text/css" href="/css/pyramid_cms.css"/>
    </xsl:template>

    <xsl:template name="JSFiles">
        <script type="text/javascript" src="/f6/js/vendor/jquery.min.js">&#160;</script>
        <script type="text/javascript" src="/f6/js/foundation.min.js">&#160;</script>
    </xsl:template>
    
    <xsl:template name="DocTitle" match="//DocTitle">
        <title><xsl:value-of select="text()"/></title>
    </xsl:template>
    
</xsl:stylesheet>
