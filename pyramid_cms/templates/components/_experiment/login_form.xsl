<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" doctype-system="" encoding="UTF-8" omit-xml-declaration="yes"/>
    <xsl:template name="LoginForm">
        <form 
        method="{//Widgets/Widget[@type='Forms'][@name='Login']/MetaData/Method/text()}" 
        action="{//Widgets/Widget[@type='Forms'][@name='Login']/MetaData/Action/text()}">
            <xsl:for-each select="//Widgets/Widget[@type='Forms'][@name='Login']/Body/Field">
                <xsl:value-of select="./Label/text()" disable-output-escaping="yes"/>
                <xsl:value-of select="./Item/text()" disable-output-escaping="yes"/>
            </xsl:for-each>
            <button class="small button small-3 small-push-9" type="submit">
                Sign In
            </button>
        </form>
    </xsl:template>
    
</xsl:stylesheet>
