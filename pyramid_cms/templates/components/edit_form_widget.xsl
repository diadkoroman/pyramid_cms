<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" doctype-system="" encoding="UTF-8" omit-xml-declaration="yes"/>
<xsl:template name="EditForm">
    <h3>
        <xsl:value-of select="//Widget[@name='edit_form']/ModelName"/> <small>Edit Item</small>
    </h3>
    <hr/>
    <xsl:for-each select="//Widget[@name='edit_form']/form">
        <form method="{@method}" action="{@action}" class="{@class}">
            <xsl:for-each select="fieldgroup">
                <xsl:choose>
                    <xsl:when test="@for='csrf_token'">
                        <xsl:copy-of select="./input"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <div class="row">
                        <div class="form-group">
                            <xsl:copy-of select="./label"/>
                                <xsl:copy-of select="./input|./select"/>
                                <xsl:if test="./errors/text()">
                                    <span class="text-danger col-xs-9 col-xs-offset-3">
                                        <xsl:value-of select="./errors/text()"/>
                                    </span>
                                </xsl:if>
                        </div>
                        </div>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            <hr/>
            <div class="form-group">
                <button class="btn btn-success pull-right" type="submit" name="option" value="leave">
                    Save
                </button>
            </div>
        </form>
    </xsl:for-each>
    <script type="text/javascript" src="/jss/fields_handler.js">&#160;</script>
</xsl:template>
</xsl:stylesheet>
