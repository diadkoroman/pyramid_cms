<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" doctype-system="" encoding="UTF-8" omit-xml-declaration="yes"/>
<xsl:template match="//Widget[@name='login_form']/form[@id='login_form']" name="LoginForm">
    <!---->
    <div class="col-xs-4 col-xs-offset-4">
        <div class="panel panel-primary">
            <!---->
            <div class="panel-heading">
                <h4>Welcome to PCMS!</h4>
            </div>
            <!---->
            <!---->
            <div class="panel-body">
                <form method="{@method}" action="{@action}" class="{@class}">
                    <xsl:for-each select="fieldgroup">
                        <xsl:choose>
                            <xsl:when test="@for='csrf_token'">
                                <xsl:copy-of select="./input"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <div class="form-group">
                                    <div class="col-xs-3">
                                    <xsl:copy-of select="./label"/>
                                    </div>
                                    <div class="col-xs-9">
                                    <xsl:copy-of select="./input"/>
                                    </div>
                                    <xsl:if test="./errors/text()">
                                        <span class="text-danger col-xs-9 col-xs-offset-3">
                                            <xsl:value-of select="./errors/text()"/>
                                        </span>
                                    </xsl:if>
                                </div>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                    
                    <div class="form-group">
                        <div class="col-xs-4 col-xs-offset-8">
                            <button type="submit" class="btn btn-default btn-block">
                                Log In
                                <span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>
                            </button>
                        </div>
                    </div>
                    <xsl:choose>
                        <xsl:when test="//PgeoTrust-Status=1">
                            <div class="label label-success">
                                PgeoTrust enabled
                            </div>
                        </xsl:when>
                        <xsl:otherwise>
                            <div class="label label-warning">
                                Pgeotrust disabled
                            </div>
                        </xsl:otherwise>
                    </xsl:choose>
                    
                </form>
            </div>
            <!---->
        </div>
    
        <hr/>
        <!-- footer -->
        <div class="row">
            <div class="col-xs-12">
                <small class="text-right">
                <ul class="list-inline">
                    <li>Developer: Roman Shmatok</li>
                    <li>2015</li>
                </ul>
                </small>
            </div>
        </div>
        <!-- footer(end) -->
    <!---->
    </div>
</xsl:template>
</xsl:stylesheet>
