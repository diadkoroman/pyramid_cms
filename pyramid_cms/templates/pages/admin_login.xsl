<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:include href="../components/css.xsl"/>
    <xsl:include href="../components/jss.xsl"/>
    <xsl:include href="../components/login_form_widget.xsl"/>
    <xsl:output method="html" doctype-system="" encoding="UTF-8" omit-xml-declaration="yes"/>
    <xsl:template match="/HtmlDocument/DocumentBody">
        <html>
            <head>
                <meta charset="utf-8"/>
                <xsl:call-template name="Css"/>
                <xsl:call-template name="Jss"/>
            </head>
            <body>
                <div class="row">
                    <xsl:apply-templates name="LoginForm"/>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
