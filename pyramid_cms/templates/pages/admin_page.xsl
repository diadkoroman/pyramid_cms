<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../components/nav_widgets.xsl"/>
<xsl:output method="html" doctype-system="" encoding="UTF-8" omit-xml-declaration="yes"/>
<xsl:template match="/">
    <html>
        <head>
            <meta charset="utf-8"/>
            <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css"/>
            <link rel="stylesheet" type="text/css" href="/css/pyramid_cms.css"/>
            <script type="text/javascript" src="/jquery/jquery.min.js">&#160;</script>
            <script type="text/javascript" src="/bootstrap/js/bootstrap.min.js">&#160;</script>
        </head>
        <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <!-- navbar-header -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="">PyrAdmin</a>
                </div>
                <!-- navbar-header (end) -->

                <!---->
                <div class="collapse navbar-collapse">
                    <xsl:apply-templates name="TopNav"/>
                </div>
                <!---->
            </div>
        </nav>
        </body>
    </html>
</xsl:template>
</xsl:stylesheet>
