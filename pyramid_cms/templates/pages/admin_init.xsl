<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:include href="../components/head.xsl"/>
    <xsl:include href="../components/teaser_widgets.xsl"/>
    <xsl:output method="html" doctype-system="" encoding="UTF-8" omit-xml-declaration="yes"/>
    <xsl:template match="/">
        <html>
            <head>
                <meta charset="utf-8"/>
                <xsl:apply-templates name="DocTitle"/>
                <xsl:call-template name="CSSFiles"/>
                <xsl:call-template name="JSFiles"/>
            </head>
            <body>
                <xsl:call-template name="WelcomeScreen"/>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
