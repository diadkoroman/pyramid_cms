<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:include href="../components/css.xsl"/>
    <xsl:include href="../components/jss.xsl"/>
    <xsl:include href="../components/topnav_widget.xsl"/>
    <xsl:include href="../components/breadcrumbs_widget.xsl"/>
    <xsl:include href="../components/sidenav_widget.xsl"/>
    <xsl:include href="../components/footer_widget.xsl"/>
    <xsl:output method="html" doctype-system="" encoding="UTF-8" omit-xml-declaration="yes"/>
    <xsl:template match="/HtmlDocument/DocumentBody">
        <html>
            <head>
                <meta charset="utf-8"/>
                <xsl:call-template name="Css"/>
                <xsl:call-template name="Jss"/>
            </head>
            <body>
                <!-- header -->
                <div class="header">
                    <!-- top menu -->
                    <xsl:call-template name="TopNav"/>
                    <xsl:call-template name="Breadcrumbs"/>
                    <!-- top menu (end) -->
                </div>
                <!-- header (end) -->
                
                <!-- page -->
                <div class="content">
                    <div class="container-fluid">
                    <!-- side menu -->
                    <div class="col-md-2">
                        <xsl:call-template name="SideNav"/>
                    </div>
                    <!-- side menu (end) -->
                    </div>
                </div>
                <!-- page (end) -->
                
                <!-- footer -->
                <div class="footer">
                    <div class="container-fluid">
                    <xsl:call-template name="Footer"/>
                    </div>
                </div>
                <!-- footer (end) -->
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
