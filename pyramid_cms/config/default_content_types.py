# -*- coding: utf-8 -*-
content_types = {
    "user":{
        "mnemo": "user",  # content type mnemo
        "description": "Pyramid CMS user",  # content type description
        "fields":{
            "login": "string",  # field(name,type)
            "passw": "text",  # field(name, type)
        },
    }
}
