# -*- coding: utf-8 -*-

"""
Initialization scenarios for PyrAdmin.

"""
from pyramid.httpexceptions import HTTPFound

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.handlers import encr
from pyramid_cms.apps.auth.dbmodels import Permission, Group, User
from pyramid_cms.apps.roots.dbmodels import (
    Site,
    Language,
    PageType,
    PageWidget,
    TreeNode
    )

def _unset_items(model):
    """ Unsettig model """
    try:
        for i in Q(model).all():
            i.delete()
    except:
        pass
        
def _check_items(models):
    """ Checking model """
    for model in models:
        if Q(model).count() == 0:
            return False
    return True

def _set_permissions():
    """
    Init sub-scenario:
    Set default permissions
    """
    from ..default_perms import perms

    for perm in perms:
        try:
            q = Q(Permission).filter(Permission.name == perm)
            if q.count() == 0:
                p = Permission()
                p.inner_name = perm[0].upper()+perm[1:]
                p.name = perm
                p.active = True
                p.save()
        except:
            raise Exception("Setting permissions failed!")
    # successful final
    return True


def _unset_permissions():
    try:
        for i in Q(Permission).all():
            i.delete()
    except:
        pass


def _set_groups():
    """
    Init sub-scenario:
    Set default groups
    """
    from ..default_groups import groups
    for groupname, perms in groups.items():
        try:
            q = Q(Group).filter(Group.name == groupname)
            if q.count() == 0:
                gpermsQ = Q(Permission).filter(Permission.name.in_(perms))
                g = Group()
                g.inner_name = groupname[0].upper()+groupname[1:]
                g.name = groupname
                g.active = True
                g.perms = [i for i in gpermsQ]
                g.save()
        except:
            # test mode. Need to return False in such case
            raise Exception("Setting groups failed!")
    # successful final
    return True


def _unset_groups():
    try:
        for i in Q(Group).all():
            i.delete()
    except:
        pass


def _set_superuser(req, userdata):
    """ Setting superuser """
    from ..default_groups import admin_groups
    if req.method == "POST":
        try:
            login = userdata["login"].data
            passw = encr.encr1(userdata["passw"].data)
            q = Q(User).filter(User.login == login, User.password == passw)
            if q.count() == 0:
                user = User()
                user.login = login
                user.password = passw
                user.groups = [g for g in Q(Group)
                                .filter(Group.name.in_(admin_groups))]
                user.save()
        except:
            raise Exception("Setting superuser failed!")
    return True


def _unset_superuser():
    """ Unsetting superuser """
    from ..default_groups import admin_groups
    for i in Q(User).filter(User.groups.any(Group.name.in_(admin_groups))):
        i.delete()


def _set_host(req):
    """
    Init sub-scenario:
    Set default host
    Add address of current host.
    (If host is in allowed)
    """
    try:
        c = Q(Site).count()
        if req.host in admin.admin.config.allowed_hosts:
            h = Q(Site).filter(Site.address == req.host)
            if h.count() == 0:
                host = Site()
                host.inner_name = req.host
                host.address = req.host
                host.active = True
                host.rank = c + 1
                host.save()
    except:
        # test mode. Need to return False in such case
        raise Exception("Setting host failed!")
    # successful final
    return True


def _unset_host():
    try:
        for i in Q(Site).all():
            i.delete()
    except:
        pass


def _set_langs(req):
    """
    Init sub-scenario:
    Set default languages
    """
    from ..default_langs import langs
    for lang in langs:
        try:
            q = Q(Language).filter(Language.mnemo == lang["mnemo"])
            if q.count() == 0:
                l = Language()
                for k, v in lang.items():
                    setattr(l, k, v)
                l.save()
        except:
            # test mode. Need to return False in such case
            raise Exception("Setting languages failed!")
    # successful final
    return True


def _unset_langs():
    """ Unsetting languages """
    try:
        for i in Q(Language).all():
            i.delete()
    except:
        pass


def _set_pagetypes(req):
    """
    Init sub-scenario:
    Set default pagetypes
    """
    from ..default_pagetypes import pagetypes
    for pagetype in pagetypes:
        #try:
        q = Q(PageType).filter(PageType.mnemo == pagetype["mnemo"])
        if q.count() == 0:
            pt = PageType()
            pt.inner_name = pagetype.get("inner_name", None)
            pt.mnemo = pagetype.get("mnemo", None)
            pt.template = pagetype.get("template", None)
            pt.widgets = Q(PageWidget).filter(PageWidget.mnemo.in_(pagetype["widgets"]))
            pt.rank = pagetype["rank"]
            pt.active = pagetype["active"]
            pt.save()
        #except:
        #    # test mode. Need to return False in such case
        #    raise Exception("Setting pagetypes failed!")
    # successful final
    return True


def _unset_pagetypes():
    """ Unsetting pagetypes """
    try:
        for i in Q(PageType).all():
            i.delete()
    except:
        pass


def _set_widgets(req):
    """
    Init sub-scenario:
    Set default PageWidgets
    """
    from ..default_widgets import widgets
    for widget in widgets:
        try:
            q = Q(PageWidget).filter(PageWidget.mnemo == widget["mnemo"])
            if q.count() == 0:
                w = PageWidget()
                for k, v in widget.items():
                    setattr(w, k, v) 
                w.save()
        except:
            # test mode. Need to return False in such case
            raise Exception("Setting PageWidgets failed!")
    # successful final
    return True


def _unset_widgets():
    """ Unsetting PageWidgets """
    try:
        for i in Q(PageWidget).all():
            i.delete()
    except:
        pass


def _set_nodes(req):
    """
    Init sub-scenario:
    Set default tree nodes
    """
    from ..default_nodes import tree_nodes
    for node in tree_nodes:

        parent = None

        # if node supposed to have a parent - find a parent
        if node["parent_mnemo"]:
            try:
                parent = Q(TreeNode)\
                    .filter(TreeNode.mnemo == node["parent_mnemo"])\
                    .first()
            except:
                # exception stops process if parent was not found
                raise Exception("Parent not found!")
            q = Q(TreeNode)\
                .filter(
                    TreeNode.mnemo == node["mnemo"],
                    TreeNode.parent_id == parent.id
                    )
        else:
            q = Q(TreeNode).filter(TreeNode.mnemo == node["mnemo"])

        # if node was not found - create it
        if q.count() == 0:
            #try:
            tn = TreeNode()
            tn.inner_name = node["inner_name"]
            tn.mnemo = node["mnemo"]
            tn.pagetype_id = Q(PageType)\
                .filter(PageType.mnemo == node["pagetype_mnemo"])\
                .first().id or None
            tn.parent_id = parent.id if parent else None
            tn.admin_node = node["admin_node"]
            tn.rank = node["rank"]
            tn.active = node["active"]
            tn.permissions = [i for i in Q(Permission)\
                .filter(Permission.name.in_(node["permissions"]))]
            tn.save()
            #except:
            #    # test mode. Need to return False in such case
            #    raise Exception("Setting tree nodes failed!")
    # successful final
    return True


def stage1(req, stages_status, restart=None):
    """ Methods wrapper for stage 1 functions """
    if restart:
        if not _check_items([User]):
            """ Unset only if user was deleted already """
            _unset_items(Permission)
            _unset_items(Group)
            stages_status[0] = False
    else:
        _set_permissions()
        _set_groups()
        stages_status[0] = True


def stage2(req, stages_status, restart=None, userdata=None):
    """ Methods wrapper for stage 2 functions """
    if restart:
        if _check_items([Permission, Group]):
            _unset_items(User)
            stages_status[1] = False
    else:
        if _check_items([Permission, Group]):
            _set_superuser(req, userdata)
            stages_status[1] = True


def stage3(req, stages_status, restart=None):
    """ Methods wrapper for stage 3 functions """
    if restart:
        _unset_items(Site)
        _unset_items(Language)
        _unset_items(PageType)
        _unset_items(PageWidget)
        _unset_items(TreeNode)
        stages_status[2] = False
    else:
        if _check_items([Permission, Group]) and _check_items([User]):
            _set_host(req)
            _set_langs(req)
            _set_widgets(req)
            _set_pagetypes(req)
            _set_nodes(req)
            stages_status[2] = True
            return HTTPFound(location=req.route_url("inner", tail=("pcms","welcome")))

def stage4(req, stages_status, restart=None):
    """ Methods wrapper for stage 4 (cancel all) functions """
    stage3(req, stages_status, restart=True)
    stage2(req, stages_status, restart=True)
    stage1(req, stages_status, restart=True)

def start_init(req, **kw):
    stage = kw.get("stage", None)
    restart = kw.get("restart", None)
    userdata = kw.get("form", None)
    """
    Start init scenario.
    Checks (and creates if not exist):
    Stage 1:
        1. Standard premissions
        2. Standard groups
    Stage 2:
        3. Superuser - UI Form
    Stage 3:
        4. Default site (current)
        5. Default language
        6. Default pagetypes
        7. Default PageWidgets
        8. Default tree nodes.
    """
    # status of init comlete. False means "incomplete", True - "complete"
    stages_status = [False, False, False]
    if stage == 1:
        stage1(req, stages_status, restart=restart)
    elif stage == 2:
        stage2(req, stages_status, restart=restart, userdata=userdata)
    elif stage == 3:
        stage3(req, stages_status, restart=restart)
    elif "cancel" in req.GET:
        stage4(req, stages_status, restart=True)
    stages_status[0] = _check_items([Permission, Group])
    stages_status[1] = _check_items([User])
    stages_status[2] = _check_items([Site, Language, PageType, PageWidget, TreeNode])
    return stages_status
