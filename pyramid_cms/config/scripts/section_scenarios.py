# -*- coding: utf-8 -*-
""" Admin scenarios for work with sections """
import transaction
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.apps.auth.dbmodels import Permission
from pyramid_cms.apps.roots.dbmodels import TreeNode, PageType

aconfig = admin.admin.config


def check_contenttypes_sections():
    """ Check contenttypes sections """
    try:
        ct_sections = Q(TreeNode).filter(TreeNode.mnemo == "contenttypes")
        if ct_sections.count() != 0:
            ctypes = admin.admin.content_types.all
            ctype = ct_sections.first()
            pagetype = Q(PageType).filter(PageType.mnemo == "admin_listview").first()
            parent = Q(TreeNode).filter(TreeNode.mnemo == "contenttypes").first()
            #perms = Q(Permission).filter(Permission.name.in_(["all"]))
            for ct in ctypes:
                with transaction.manager:
                    node = Q(TreeNode)\
                    .filter(TreeNode.mnemo == ct.__name__.lower(),
                    TreeNode.parent_id == parent.id)
                    if node.count() == 0:
                        node = TreeNode()
                    else:
                        node = node.first()
                    node.inner_name = \
                    "{0} :: {1}".format(ct.__name__, "PyrAdmin Content Types")
                    node.mnemo = ct.__name__.lower()
                    node.pagetype_id = pagetype.id
                    node.parent_id = parent.id
                    node.admin_node = True
                    node.active = True
                    node.permissions = [p for p in parent.permissions]
                    node.save()
    except:
        pass


def check_plugins_sections():
    """
    Check admin.admin.plugins stack for items and registering
    all plugins found into the tree
    1. Check section for plugins in the tree
    2. Register plugins
    """
    try:
        # 1.
        plugin_section = Q(TreeNode).filter(TreeNode.mnemo == aconfig.plugins_dir)
        if plugin_section.count() == 0:
            parent = Q(TreeNode).filter(TreeNode.mnemo == aconfig.admin_root_dir).first()
            pagetype = Q(PageType).filter(PageType.mnemo == "admin_plugins").first()
            plugin_section = TreeNode()
            plugin_section.inner_name = "Plugins"
            plugin_section.mnemo = aconfig.plugins_dir
            plugin_section.pagetype_id = pagetype.id
            plugin_section.parent_id = parent.id
            plugin_section.admin_node = True
            plugin_section.permissions = [p for p in parent.permissions]
            plugin_section.save(flush=True)
        else:
            plugin_section = plugin_section.first()

        # 2.
        #try:
        for plugin in admin.admin._plugins.all:
            p = plugin(section_class=TreeNode, parent=plugin_section)
            admin.admin.plugins.append(p)
        #except:
        #    pass
    except:
        pass
