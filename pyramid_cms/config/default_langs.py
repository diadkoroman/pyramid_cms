# -*- coding: utf-8 -*-
"""
Default languages for user interface
"""
langs = [
        {
            "mnemo":"en",
            "inner_name": "English",
            "deflang": True,
            "use_for_admin": True,
            "use_for_project": True,
            "rank": 1,
        },
        {
            "mnemo":"ua",
            "inner_name": "Українська",
            "deflang": False,
            "use_for_admin": True,
            "use_for_project": True,
            "rank": 2,
        },
        {
            "mnemo":"ru",
            "inner_name": "Русский",
            "deflang": False,
            "use_for_admin": True,
            "use_for_project": True,
            "rank": 3,
        }
        ]
