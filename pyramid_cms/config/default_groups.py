# -*- coding: utf-8 -*-

"""
Default droups created at the time of init process
with its permissions.
Note: All permissions must be in default_perms.perms listing
"""
groups = {
    "superusers":["all", "login"],
    "editors":["view", "add", "edit", "delete","login"]
}

admin_groups = "superusers",
