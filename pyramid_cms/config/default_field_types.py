# -*- coding: utf-8 -*-
field_types = {
    "integer": {
        "inner_name": "INTEGER",
        "mnemo": "integer",
        "description": "Field type for storing integer data",
        "rank": 1,
        "active": True
    },
    "string": {
        "inner_name": "STRING",
        "mnemo": "string",
        "description": "Field type for storing string data",
        "rank": 2,
        "active": True
    },
    "text": {
        "inner_name": "TEXT",
        "mnemo": "text",
        "description": "Field type for storing texts",
        "rank": 3,
        "active": True
    },
    "fk": {
        "inner_name": "FOREIGN KEY",
        "mnemo": "fk",
        "description": "Foreign Keys",
        "rank": 4,
        "active": True
    },
    "m2m": {
        "inner_name": "MANY TO MANY",
        "mnemo": "m2m",
        "description": "Many-to-many relations",
        "rank": 5,
        "active": True
    },
}
