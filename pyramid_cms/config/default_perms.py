# -*- coding: utf-8 -*-
"""
Default permissions created at the time of init process
"""
perms = "all", "view", "add", "edit", "delete", "login"
