# -*- coding: utf-8 -*-

"""
Default pagetypes created at the time of init process
Note: default widgets must be in a config.default_widgets
"""
pagetypes = [
        {
        'inner_name': 'Admin Welcome Page',
        'mnemo':'admin_init',
        'template': 'admin_init.pt',
        'widgets': ['welcome_screen'],
        'rank': 0,
        'active': True,
        },
        {
        'inner_name': 'Admin Login Page',
        'mnemo':'admin_login',
        'template': 'admin_login.pt',
        'widgets': ['login_form'],
        'rank': 0,
        'active': True,
        },
        {
        'inner_name': 'Admin Logout Page',
        'mnemo':'admin_logout',
        'template': 'admin_logout.pt',
        'widgets': ['logout_form'],
        'rank': 0,
        'active': True,
        },
        {
        'inner_name': 'Admin Home Page',
        'mnemo':'admin_home',
        'template': 'admin_home.pt',
        'widgets': [
            'header',
            'breadcrumbs',
            'topnav',
            'func_panel',
            'sidenav',
            'content',
            'footer'
        ],
        'rank': 0,
        'active': True,
        },
        {
        'inner_name': 'Admin Inner Page',
        'mnemo':'admin_inner',
        'template': 'admin_inner.pt',
        'widgets': [
            'header',
            'breadcrumbs',
            'topnav',
            'sidenav',
            'content',
            'footer'
        ],
        'rank': 0,
        'active': True,
        },
        {
        'inner_name': 'Admin Plugins Page',
        'mnemo':'admin_plugins',
        'template': 'admin_plugins.pt',
        'widgets': [
            'header',
            'breadcrumbs',
            'topnav',
            'sidenav',
            'content',
            'footer'
        ],
        'rank': 0,
        'active': True,
        },
        {
        'inner_name': 'Admin List View Page',
        'mnemo':'admin_listview',
        'template': 'admin_listview.pt',
        'widgets': [
            'header',
            'breadcrumbs',
            'topnav',
            'sidenav',
            'content',
            'listview',
            'footer'
        ],
        'rank': 0,
        'active': True,
        },
        {
        'inner_name': 'Admin Item View Page',
        'mnemo':'admin_itemview',
        'template': 'admin_itemview.pt',
        'widgets': [
            'header',
            'breadcrumbs',
            'topnav',
            'sidenav',
            'content',
            'itemview',
            'edit_form',
            'delete_form',
            'footer'
        ],
        'rank': 0,
        'active': True,
        },
        {
        'inner_name': 'Admin Add Item Page',
        'mnemo':'admin_add',
        'template': 'admin_inner.pt',
        'widgets': [
            'header',
            'breadcrumbs',
            'topnav',
            'content',
            'sidenav',
            'add_form',
            'footer'
        ],
        'rank': 0,
        'active': True,
        },
        {
        'inner_name': 'Admin Edit Item Page',
        'mnemo':'admin_edit',
        'template': 'admin_inner.pt',
        'widgets': [
            'header',
            'breadcrumbs',
            'topnav',
            'content',
            'sidenav',
            'edit_form',
            'footer'
        ],
        'rank': 0,
        'active': True,
        },
        {
        'inner_name': 'Admin Delete Item Page',
        'mnemo':'admin_delete',
        'template': 'admin_inner.pt',
        'widgets': [
            'header',
            'breadcrumbs',
            'topnav',
            'content',
            'delete_form',
            'footer'
        ],
        'rank': 0,
        'active': True,
        },
        {
        'inner_name': 'Project Home Page',
        'mnemo':'project_home',
        'template': 'project_home.pt',
        'widgets': [
            'project_head',
            'project_body'
        ],
        'rank': 0,
        'active': True,
        },
        {
        'inner_name': 'Project Downloads Page',
        'mnemo':'project_downloads',
        'template': 'project_downloads.pt',
        'widgets': [],
        'rank': 0,
        'active': True,
        },
        {
        'inner_name': 'Project Ajax Requests Page',
        'mnemo':'projectajaxpage',
        'template': 'project_ajax.pt',
        'widgets': [],
        'rank': 0,
        'active': True,
        },
        {
        'inner_name': 'Admin Ajax Requests Page',
        'mnemo':'adminajaxpage',
        'template': 'admin_ajax.pt',
        'widgets': [],
        'rank': 0,
        'active': True,
        },
        {
        'inner_name': 'Admin Filemanager Page',
        'mnemo':'admin_fm',
        'template': 'admin_fm.pt',
        'widgets': [],
        'rank': 0,
        'active': True,
        },
        {
        'inner_name': 'Notfound Page',
        'mnemo':'notfound_page',
        'template': 'notfound_page.pt',
        'widgets': [],
        'rank': 0,
        'active': True,
        },
    ]
