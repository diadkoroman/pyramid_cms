# -*- coding: utf-8 -*-
"""
Default tree nodes.
"""
tree_nodes = [
        {
            "inner_name": "Project Homepage",
            "mnemo": "/",
            "pagetype_mnemo": "project_home",
            "parent_mnemo": None,
            "admin_node": False,
            "rank": 0,
            "active": True,
            "permissions":["view",]
        },
        {
            "inner_name": "Project Downloads",
            "mnemo": "downloads",
            "pagetype_mnemo": "project_downloads",
            "parent_mnemo": "/",
            "admin_node": False,
            "rank": 0,
            "active": True,
            "permissions":["view",]
        },
        {
            "inner_name": "Project Ajax Requests (JSON)",
            "mnemo": "ajax.json",
            "pagetype_mnemo": "projectajaxpage",
            "parent_mnemo": "/",
            "admin_node": False,
            "rank": 0,
            "active": True,
            "permissions":["view",]
        },
        {
            "inner_name": "Admin Homepage",
            "mnemo": "pcms",
            "pagetype_mnemo": "admin_home",
            "parent_mnemo": "/",
            "admin_node": True,
            "rank": 1,
            "active": True,
            "permissions":["login",]
        },
        {
            "inner_name": "Admin Ajax Requests (JSON)",
            "mnemo": "ajax.json",
            "pagetype_mnemo": "adminajaxpage",
            "parent_mnemo": "pcms",
            "admin_node": True,
            "rank": 0,
            "active": True,
            "permissions":["view",]
        },
        {
            "inner_name": "Welcome",
            "mnemo": "welcome",
            "pagetype_mnemo": "admin_init",
            "parent_mnemo": "pcms",
            "admin_node": True,
            "rank": 2,
            "active": True,
            "permissions":["view",]
        },
        {
            "inner_name": "Log In",
            "mnemo": "login",
            "pagetype_mnemo": "admin_login",
            "parent_mnemo": "pcms",
            "admin_node": True,
            "rank": 3,
            "active": True,
            "permissions":["view",]
        },
        {
            "inner_name": "Log Out",
            "mnemo": "logout",
            "pagetype_mnemo": "admin_logout",
            "parent_mnemo": "pcms",
            "admin_node": True,
            "rank": 4,
            "active": True,
            "permissions":["view",]
        },
        {
            "inner_name": "Content Types",
            "mnemo": "contenttypes",
            "pagetype_mnemo": "admin_inner",
            "parent_mnemo": "pcms",
            "admin_node": True,
            "rank": 5,
            "active": True,
            "permissions":["all"]
        },
        {
            "inner_name": "Plugins",
            "mnemo": "plugins",
            "pagetype_mnemo": "admin_plugins",
            "parent_mnemo": "pcms",
            "admin_node": True,
            "rank": 5,
            "active": True,
            "permissions":["all"]
        },
        {
            "inner_name": "Filemanager",
            "mnemo": "fm",
            "pagetype_mnemo": "admin_fm",
            "parent_mnemo": "pcms",
            "admin_node": True,
            "rank": 6,
            "active": True,
            "permissions":["all"]
        },
        {
            "inner_name": "Project Ajax Requests (JSON)",
            "mnemo": "ajax.json",
            "pagetype_mnemo": "project_ajax",
            "parent_mnemo": "/",
            "admin_node": False,
            "rank": 0,
            "active": True,
            "permissions":["view",]
        },
        {
            "inner_name": "Notfound Page",
            "mnemo": "404",
            "pagetype_mnemo": "notfound_page",
            "parent_mnemo": "/",
            "admin_node": False,
            "rank": 0,
            "active": True,
            "permissions":["view",]
        },
]
