# -*- coding: utf-8 -*-
import os
from pyramid_cms.base import admin

""" ADMIN SETTINGS """
admin.admin.config.basedir = os.path.dirname(os.path.dirname(__file__))
admin.admin.config.debug = True
admin.admin.config.appname = 'Pyramid CMS'
admin.admin.config.deflang = 'ua'
admin.admin.config.packagename = 'pyramid_cms'
admin.admin.config.admin_root_dir = 'pcms'
admin.admin.config.content_types_dir = 'contenttypes'
admin.admin.config.plugins_dir = 'plugins'
admin.admin.config.downloads_dir = 'downloads'
admin.admin.config.files = admin.admin.config.basedir + '/files'
admin.admin.config.imgs = admin.admin.config.files + '/imgs'
admin.admin.config.tmp_dir = admin.admin.config.basedir + '/tmp'

# dirs for storing staticfiles
admin.admin.config.css = admin.admin.config.basedir + '/static/css'
admin.admin.config.jss = admin.admin.config.basedir + '/static/jss'


# dirs for storing templates
admin.admin.config.templates = [
    admin.admin.config.basedir+'/templates',
    admin.admin.config.basedir+'/templates/pages',
    admin.admin.config.basedir+'/templates/components',
]
# number of templates dir used as default one
admin.admin.config.templates_default_dir=1
# template extension
admin.admin.config.template_ext = '.pt'
# .csv extension
admin.admin.config.csv_ext = \
    (
        '.csv',
    )


admin.admin.config.superadmin_groups = 'superusers',
admin.admin.config.standard_perms = 'view','add','edit','delete'

admin.admin.config.imgs_mime = \
    (
        'image/gif',
        'image/jpeg',
        'image/pjpeg',
        'image/png'
    )

admin.admin.config.css_mime = \
    (
        'text/css',
        'application/x-pointplus'
    )

admin.admin.config.js_mime = \
    (
        'text/javascript',
        'application/javascript',
        'text/jscript',
        'text/x-javascript',
        'application/x-javascript'
    )

admin.admin.config.docs_mime = \
    (
        'application/pdf',
        'application/excel',
        'application/vnd.ms-excel',
        'application/x-excel',
        'application/x-msexcel',
        'application/x-rar-compressed',
        'application/x-rar', 
        'application/octet-stream',
        'application/x-7z-compressed',
        'application/zip',
        'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    )
    
admin.admin.config.csv_mime = \
    (
        'text/csv',
    )

"""
Змінні для лічильника відвідувань
"""
# якщо в урлі трапляються ці розширення - лічильник не спрацьовує
admin.admin.config.vcounter_rejected_exts = ('.css', '.js', '.min.js', '.png')
############################


# This option enables pgeotrust module usage in pyramid_cms
admin.admin.config.enable_pgeotrust = False

# Адреса проксі-сервера, що використовується для з’єднань
"""
Ця адреса може використовуватися як окремо винесеними так і інтегрованими
модулями, для роботи котрих потрібне з’єднання із зовнішніми джерелами.
"""
admin.admin.config.proxy_address = None
#admin.admin.config.proxy_address = \
#    {
#        'http':'shmatok:Tishen572@192.168.50.9:8080',
#        'https':'shmatok:Tishen572@192.168.50.9:8080'
#    }


admin.admin.config.allowed_hosts = \
    (
        'http://new.accordbank.com.ua',
        'http://127.0.0.1:8775'
    )
