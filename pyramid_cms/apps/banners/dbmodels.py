# -*- coding: utf-8 -*-
import datetime

# SQLALCHEMY IMPORTS
from sqlalchemy import (
        Date,
        Column,
        ForeignKey,
        Integer,
        String,
        or_
        )
from sqlalchemy.orm import relationship
from sqlalchemy.sql.expression import asc

from wtforms.fields import FileField
# from wtforms import widgets
# from wtforms import validators
from wtforms.ext.sqlalchemy.fields import QuerySelectField

# APP IMPORTS
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Base, Q
from pyramid_cms.assets.db.sqlalchemy.models import mixins
from pyramid_cms.assets.db.sqlalchemy.models import tables
from pyramid_cms.assets.db.sqlalchemy import fields
from pyramid_cms.assets.translations import _db_ts as _

from pyramid_cms.apps.roots import dbmodels as rootdb


class BannerSlot(mixins.RankedColumnsMixin, Base):
    __tablename__ = tables.tablename_('bslots')    # fc_bslots
    __descr__ = 'Перелік слотів для розміщення банерів'
    verbose_name = 'банерний слот'
    verbose_name_plural = 'банерні слоти'

    def __str__(self):
        return '{0} ({1})'.format(self.inner_name, self.mnemo)

    site_id = Column(Integer, ForeignKey(tables.fk_('sites')))
    mnemo = Column('mnemo', String(100))
    # інтервал скролу банерів
    scrlint = Column(Integer, default=5000)
    site = relationship('Site', uselist=False)

    @classmethod
    def get_by_mnemo(cls, mnemo):
        """ Повертає екземпляр класу (Слот) за мнемо-назвою """
        try:
            return cls.filter(cls.mnemo == mnemo).scalar()
        except:
            return False

    def get_items(self, categ_mnemo=False, fallback_categ_mnemo=False):
        today = datetime.date.today()
        """
        Отримати банери поточного банерного слоту.
        Якщо зазначено категорію банерів - додатково фільтруємо
        вивід за категорією
        """
        items = Q(Banner).join(BannerSlot).join(BannerCateg)\
            .filter(BannerSlot.id == self.id)\
            .filter(Banner.active == True)\
            .filter(Banner.start_expo <= today,
                    or_(Banner.end_expo >= today, Banner.end_expo == None))\
            .order_by(asc(Banner.rank))
        if categ_mnemo:
            try:
                itemsCat = items.filter(BannerCateg.mnemo == categ_mnemo)
                if not itemsCat:
                    itemsCat = items\
                        .filter(BannerCateg.mnemo == fallback_categ_mnemo)
                if itemsCat:
                    items = itemsCat
            except:
                pass
        """ Повертаємо результат """
        return items

    @classmethod
    def _form(cls, *args, **kw):
        """ Form to process data """
        UIForm = cls._uiform()

        class __F(UIForm):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'site_id',
                       'site'
                       )

            class Meta:
                model = cls
            site = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    if Q(cls)\
                    .filter(cls.mnemo == self.mnemo.data)\
                    .count() == 0:
                        for attr in columns:
                            setattr(obj, attr, getattr(self, attr).data)
                        obj.site = self.site.data
                        obj.save(flush=True)
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj, attr, getattr(self, attr).data)
                    obj.site = self.site.data
                    obj.save(flush=True)
                    return True
                return False
        f = __F(*args, **kw)
        f.site.query = Q(rootdb.Site)
        return f
admin.admin.content_types.append(BannerSlot)

"""
Категорії банерів - додатковий фільтр, що дозволятим фільтрувати банери
всередині одного слоту.
"""


class BannerCateg(mixins.RankedColumnsMixin, Base):
    __tablename__ = tables.tablename_('bicategs')    # fc_bicategs
    __descr__ = ('Перелік категорій для банерних слотів')
    verbose_name = _('категорія банерів')
    verbose_name_plural = _('категорії банерів')

    def __str__(self):
        return '{0} ({1})'.format(self.inner_name, self.mnemo)

    mnemo = Column('mnemo', String(100))
admin.admin.content_types.append(BannerCateg)

"""
Клас, що об’єднує усі мовні локалі банерів у один логічний об’єкт.
Таким чином замість трьох окремих банерів на різних мовах ми
отримуємо Banner, що має властивість images, котра є переліком усіх
мовних локалей.Кожну з мовних локалей банера можна отримати за допомогою
спец.метода Banner().localized(lang_id)
"""


class Banner(mixins.RankedColumnsMixin, Base):
    __tablename__ = tables.tablename_('bitems')    # fc_bitems
    __descr__ = """
    Банери.Цей клас об’єднує кілька мовних локалей в один логічний
    об’єкт.
    """
    verbose_name = _('банер')
    verbose_name_plural = _('банери')

    def __str__(self):
        return '{0} ({1})'.format(self.inner_name, self.mnemo)

    slot_id = Column(Integer, ForeignKey(tables.fk_('bslots')))
    categ_id = Column(Integer, ForeignKey(tables.fk_('bicategs')))
    mnemo = Column('mnemo', String(100))
    link = Column('link', String(150), nullable=True)
    start_expo = Column('start_expo', Date, nullable=True)
    end_expo = Column('end_expo', Date, nullable=True)
    """ Rel between slots and items. """
    slot = relationship(
        'BannerSlot',
        backref='items',
        uselist=False,
        )
    """ Rel between items and categs. """
    categ = relationship(
        'BannerCateg',
        backref='items',
        uselist=False,
        )
    """ Rel to Banner locales (images) """
    images = relationship('BannerImage',
                          backref='item',
                          cascade='all, delete-orphan',
                          lazy='dynamic')

    def get_rank(self):
        """ Return its rank value """
        return self.rank

    def localized(self, lang_id):
        """
        Спочатку пробує повернути потрібну локаль.
        Якщо не виходить - пробує повернути першу картинку у списку.
        Якщо картинок немає - повертає None (порожнє посилання)
        """
        try:
            loc = self.images.filter_by(lang_id=lang_id).scalar()
            if loc is not None:
                return loc
            loc = self.images.order_by('id').scalar()
            if loc is not None:
                return loc
            return None
        except:
            return None

    # Кастомний table
    @classmethod
    def table(cls, **kw):
        kw['order_by'] = 'rank'
        return super().table(**kw)

    @classmethod
    def _form(cls, *args, **kw):
        """ Form to process data """
        UIForm = cls._uiform()

        class __F(UIForm):
            exclude = (
                'id',
                'created',
                'updated',
                'slot_id',
                'categ_id',
                'slot',
                'categ',
            )

            class Meta:
                model = cls
            slot = QuerySelectField()
            categ = QuerySelectField(allow_blank=True, blank_text='----')

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    if Q(cls)\
                    .filter(
                        cls.slot_id == self.slot.data.id,
                        cls.mnemo == self.mnemo.data)\
                    .count() == 0:
                        for attr in columns:
                            setattr(obj, attr, getattr(self, attr).data)
                        obj.slot = self.slot.data
                        obj.categ = self.categ.data
                        obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        setattr(obj, attr, getattr(self, attr).data)
                    obj.slot = self.slot.data
                    obj.categ = self.categ.data
                    obj.save()
                    return True
                return False
        f = __F(*args, **kw)
        f.slot.query = Q(BannerSlot)
        f.categ.query = Q(BannerCateg)
        return f
admin.admin.content_types.append(Banner)


class BannerImage(mixins.LiteColumnsMixin, Base):
    """
    Image item (locale) for banner instance
    """
    __tablename__ = tables.tablename_('bitem_images')    # fc_bitem_images
    __descr__ = _('Банер(зображення), локалізований по мові')

    """
    Для цього поля обов’язковим елементом є шлях до директорії,
    куди будуть зберігатися банери.
    Відсутність цієї директорії зробить неможливим роботу із
    завантаженням банерів.
    """
    verbose_name = _('банер (локаль)')
    verbose_name_plural = _('банери (локалі)')

    def __str__(self):
        return self.image

    image = Column(fields.UploadFileField(
        200,
        mimetypes=admin.admin.config.imgs_mime,
        upload_to='images/banners',
        ))
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))
    item_id = Column(Integer, ForeignKey(tables.fk_('bitems')))
    # якщо зазначено загальний лінк для Banner - цей рядок лишаємо порожнім
    link = Column('link', String(100), nullable=True)
    lang = relationship('Language', uselist=False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Form to process data """
        UIForm = cls._uiform()

        class __F(UIForm):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'lang_id',
                       'item_id',
                       'lang',
                       'item',
                       'image'
                       )

            class Meta:
                model = cls
            lang = QuerySelectField()
            item = QuerySelectField()
            image = FileField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    if Q(cls)\
                    .filter(
                        cls.item_id == self.item.id,
                        cls.lang_id == self.lang.id)\
                    .count() == 0:
                        for attr in columns:
                            setattr(obj, attr, getattr(self, attr).data)
                        obj.lang = self.lang.data
                        obj.item = self.item.data
                        try:
                            obj.image = self.image.data.__dict__
                        except:
                            pass
                        obj.save(flush=True)
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj, attr, getattr(self, attr).data)
                    obj.lang = self.lang.data
                    obj.item = self.item.data
                    try:
                        obj.image = self.image.data.__dict__
                    except:
                        pass
                    obj.save(flush=True)
                    return True
                return False
        f = __F(*args, **kw)
        f.lang.query = Q(rootdb.Language)
        f.item.query = Q(Banner)
        return f
admin.admin.content_types.append(BannerImage)
