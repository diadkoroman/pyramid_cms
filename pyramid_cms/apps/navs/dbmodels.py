# -*- coding: utf-8 -*-
import time

from wtforms.fields import SelectMultipleField, SelectField
from wtforms import widgets
from wtforms import validators
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField

# SQLALCHEMY IMPORTS
from sqlalchemy import (
        Table,
        Column,
        Boolean,
        ForeignKey,
        Integer,
        String,
        Text
        )
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql.functions import func

# APP IMPORTS
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Base, Metadata, Q
from pyramid_cms.assets.db.sqlalchemy.models import mixins
from pyramid_cms.assets.db.sqlalchemy.models import tables
from pyramid_cms.assets.db.sqlalchemy import fields
from pyramid_cms.assets.translations import _db_ts as _

from pyramid_cms.apps.roots import dbmodels as rootdb


"""
Many-to-many relationship table between categories and files
"""
nicategs_to_navitems = Table(
    tables.tablename_('nicategs_to_navitems'), # navitemcategs_navitems
    Metadata,
    Column('categ_id',Integer,ForeignKey(tables.fk_('nicategs'))),
    Column('navitem_id',Integer,ForeignKey(tables.fk_('navitems'))),
)


class NavSlot(mixins.RankedColumnsMixin,Base):
    __tablename__= tables.tablename_('navslots') # pyramid_cms_navslots
    __descr__ = """
    Типи меню. Використовується для класифікації різних меню і організації вибірки
    із бази даних.
    """
    verbose_name = _('навігатор')
    verbose_name_plural = _('навігатори')
    exclude = ('created', 'updated')
    
    def __str__(self):
        """ String presentation of model. """
        return self.inner_name
        
    site_id = Column(Integer,ForeignKey(tables.fk_('sites')))
    mnemo = Column('mnemo',String(100))
    site = relationship('Site',backref = 'navslots',uselist = False)
    items = relationship(
        'NavItem',
        backref = 'navslot',
        cascade = 'all, delete, delete-orphan',
        order_by = 'NavItem.rank',
        lazy = 'dynamic',
        )

    @classmethod
    def get_sites(cls):
        return Q(rootdb.Site)

    @classmethod
    def get_items(cls):
        try:
            return Q(NavItem).order_by('rank')
        except:
            return None
    
    @classmethod
    def _form(cls, *args, **kw):
        """ Form to process data """
        UIForm = cls._uiform()
        class __F(UIForm):
            exclude = 'id','created','updated','site_id','site','items'
            class Meta:
                model = cls
            site = QuerySelectField()
            items = QuerySelectMultipleField()
            
            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    if Q(cls).filter(cls.mnemo == self.mnemo.data).count() == 0:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                        obj.site = self.site.data
                        obj.items = self.items.data
                        obj.save(flush=True)
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    obj.site = self.site.data
                    obj.items = self.items.data
                    obj.save(flush=True)
                    return True
                return False
        f = __F(*args, **kw)
        f.site.query = cls.get_sites()
        f.items.query = cls.get_items()
        return f



class NavItemCategory(mixins.RankedColumnsMixin, Base):
    __tablename__ = tables.tablename_('nicategs') # pyramid_cms_nicategs
    __descr__ = 'Перелік категорій для пунктів меню'
    verbose_name = _('категорія')
    verbose_name_plural = _('категорії')
    
    def __str__(self):
        """ String presentation of model. """
        return self.inner_name
        
    site_id = Column(Integer,ForeignKey(tables.fk_('sites')))
    mnemo = Column('mnemo',String(100))
    site = relationship('Site',uselist = False)

    # рядкове представлення екземпляра класу
    def __str__(self):
        return self.inner_name

    @classmethod
    def get_sites(cls):
        return Q(rootdb.Site)

    @classmethod
    def _form(cls, *args, **kw):
        """ Form to process data """
        UIForm = cls._uiform()
        class __F(UIForm):
            exclude = 'id','created','updated','site_id','site','items'
            class Meta:
                model = cls
            site = QuerySelectField()
            
            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    if Q(cls).filter(cls.mnemo == self.mnemo.data).count() == 0:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                        obj.site = self.site.data
                        obj.save(flush=True)
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    obj.site = self.site.data
                    obj.save(flush=True)
                    return True
                return False
        f = __F(*args, **kw)
        f.site.query = cls.get_sites()
        return f



class NavItem(mixins.RankedColumnsMixin,Base):
    __tablename__= tables.tablename_('navitems') # pyradmin_navitems
    __descr__ = 'Пункти меню'
    verbose_name = _('пункт меню')
    verbose_name_plural = _('пункти меню')
    
    def __str__(self):
        """ String presentation of model. """
        return '{0} ({1})'.format(self.inner_name, self.navslot.inner_name)
    
    node_id = Column(Integer,ForeignKey(tables.fk_('tree_nodes')), nullable=True)
    mnemo = Column('mnemo',String(100),default=str(int(time.time())),nullable = True)
    link = Column('link',String(200), nullable = True)
    # мініатюрне зображення біля пункту меню(якщо потрібне)
    icon = Column(String(100),nullable = True)
    # тип меню
    navslot_id = Column('navslot_id',Integer,ForeignKey(tables.fk_('navslots')))
    # батьківський пункт меню(для каскадних навігаторів)(опц.)
    parent_id = Column('parent_id',Integer,ForeignKey(tables.fk_('navitems')),
        nullable = True)
    # якщо imaged-true, то name може містити посилання на картинки
    imaged = Column(Boolean,default = False)
    categs = relationship('NavItemCategory',
        secondary = nicategs_to_navitems,
        order_by='NavItemCategory.rank',
        lazy = 'dynamic',
        )
    node = relationship('TreeNode', uselist = False)
    parent = relationship('NavItem',
        remote_side = 'NavItem.id',
        order_by='NavItem.rank',
        lazy = 'joined',
        uselist = False
        )
    children = relationship('NavItem',
        remote_side = 'NavItem.parent_id',
        order_by='NavItem.rank',
        lazy = 'dynamic')
    
    """
    Метод, що повертає локалізовану версію пункту меню.
    Якщо меню ілюстроване - поверається картинка,
    якщо неілюстроване - напис.
    Якщо локалі не знайдено, повертається внутрішня назва пункту.
    """
    def localized(self,lang_id):
        
        try:
            name = self.names.filter_by(lang_id = lang_id).first()
            # якщо це ілюстроване меню - повертаємо картинку
            if self.imaged:
                return name.imgfile
            return name.content
        except:
            return self.inner_name
        
    # Кастомний метод table
    @classmethod
    def table(cls,**kw):
        kw['order_by'] = 'navslot_id,parent_id,rank'
        return super().table(**kw)
        
    @classmethod
    def _form(cls, *args, **kw):
        """ Form to process data """
        UIForm = cls._uiform()
        class __F(UIForm):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'site_id',
                       'node_id',
                       'navslot_id',
                       'parent_id',
                       'navslot',
                       'categs',
                       'node',
                       'parent',
                       'children',
                       )
            class Meta:
                model = cls
            navslot = QuerySelectField()
            categs = QuerySelectMultipleField(allow_blank=True, blank_text='----')
            node = QuerySelectField(allow_blank=True, blank_text='----')
            parent = QuerySelectField(allow_blank=True, blank_text='----')
            children = QuerySelectMultipleField(allow_blank=True, blank_text='----')
            
            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    if Q(cls).filter(cls.mnemo == self.mnemo.data).count() == 0:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                        if getattr(self.navslot, 'data', False):
                            obj.navslot = self.navslot.data
                        if getattr(self.categs, 'data', False):
                            obj.categs = self.categs.data
                        if getattr(self.node, 'data', False):
                            obj.node = self.node.data
                        if getattr(self.parent,'data', False):
                            obj.parent = self.parent.data
                        if getattr(self.children,'data',False):
                            obj.children = self.children.data
                        obj.save(flush=True)
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    if getattr(self.navslot, 'data', False):
                        obj.navslot = self.navslot.data
                    if getattr(self.categs, 'data', False):
                        obj.categs = self.categs.data
                    if getattr(self.node, 'data', False):
                        obj.node = self.node.data
                    if getattr(self.parent,'data', False):
                        obj.parent = self.parent.data
                    if getattr(self.children,'data',False):
                        obj.children = self.children.data
                    obj.save(flush=True)
                    return True
                return False
        f = __F(*args, **kw)
        f.navslot.query = NavSlot.index()
        f.categs.query = NavItemCategory.index()
        f.node.query = rootdb.TreeNode.index()
        f.parent.query = cls.index(exclude_id=[getattr(f._obj,'id',None)])
        f.children.query = cls.index(exclude_id=[getattr(f._obj,'id',None)])
        return f



class NavItemName(mixins.StringSlot, Base):
    __tablename__ = tables.tablename_('navitem_names') # pyradmin_navitem_names
    __descr__ = 'Мовні версії для написів у пунктах меню'
    verbose_name = _('пункт меню (локаль)')
    verbose_name_plural = _('пункти меню (локалі)')
    
    def __str__(self):
        """ String presentation of model. """
        return self.inner_name
    
    item_id = Column(Integer,ForeignKey(tables.fk_('navitems')))
    lang_id = Column(Integer,ForeignKey(tables.fk_('langs')))
    imgfile = Column(fields.UploadFileField(
        200,
        mimetypes = admin.admin.config.imgs_mime,
        upload_to= admin.admin.config.imgs),
        nullable = True
    )
    item = relationship('NavItem',
        backref=backref('names', lazy='dynamic',cascade='all, delete-orphan'),
        )
    # Кастомний метод table
    @classmethod
    def table(cls,**kw):
        kw['order_by'] = 'item_id,lang_id'
        return super().table(**kw)


""" Registration """
admin.admin.content_types.append(NavSlot)
admin.admin.content_types.append(NavItemCategory)
admin.admin.content_types.append(NavItem)
admin.admin.content_types.append(NavItemName)
