# -*- coding: utf-8 -*-

from sqlalchemy import (
        #Table,
        Column,
        #Boolean,
        ForeignKey,
        Integer,
        String,
        #Text
        )
from sqlalchemy.orm import relationship, backref
#from sqlalchemy.sql.functions import func

from wtforms.fields import FileField
from wtforms import widgets
from wtforms import validators
from wtforms.ext.sqlalchemy.fields import (
    QuerySelectField,
    QuerySelectMultipleField
)

# APP IMPORTS
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Base, Q
from pyramid_cms.assets.db.sqlalchemy import fields
from pyramid_cms.assets.db.sqlalchemy.models import mixins
from pyramid_cms.assets.db.sqlalchemy.models import tables
from pyramid_cms.assets.translations import _db_ts as _
from pyramid_cms.apps.roots import dbmodels as rootsdb


class JSFilesSlot(mixins.RankedColumnsMixin, Base):
    """
    Клас, що містить дані про модель слоту для файлів .js
    """
    __tablename__ = tables.tablename_('js_fslots') # fcbank_fslots
    __descr__ = 'Перелік слотів для розміщення файлів .js'
    verbose_name = '.js file slot'
    verbose_name_plural = '.js file slots'
    
    def __str__(self):
        return self.inner_name
    
    site_id = Column(Integer,ForeignKey(tables.fk_('sites')))
    mnemo = Column('mnemo',String(100))
    site = relationship('Site', uselist = False)
    files = relationship('JSFile',
        backref=backref('slot', uselist=False),
        order_by='JSFile.rank',
        lazy = 'dynamic',
        )
    
    @classmethod
    def _form(cls, *args, **kw):
        """ Form to process data """
        class __F(cls._uiform()):
            exclude = (
                'id',
                'created',
                'updated',
                'site_id',
                'site'
                )
            class Meta:
                model = cls

            site = QuerySelectField()
                
            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in 
self.exclude]
                if self.validate():
                    obj = cls()
                    if Q(cls).filter(cls.mnemo == self.mnemo.data).count() == 0:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                        obj.site = self.site.data
                        obj.save(flush=True)
                    return True
                return False
                                
            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in 
self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    obj.site = self.site.data
                    obj.save(flush=True)
                    return True
                return False

        f = __F(*args, **kw)
        f.site.query = Q(rootsdb.Site)
        return f
admin.admin.content_types.append(JSFilesSlot)


class CSSFile(mixins.RankedColumnsMixin, Base):
    """
    Stylesheet file asset. Is used only for project side for
    improve extensibility of the project.
    """
    __tablename__ = tables.tablename_('css_files')
    __descr__ = _('stylesheet files (.css) for project')
    verbose_name = _('stylesheet file')
    verbose_name_plural = _('stylesheet files')
    site_id = Column(Integer, ForeignKey(tables.fk_('sites')))
    fileitem = Column(fields.FileStorageField(
        200,
        mimetypes=admin.admin.config.css_mime),
        nullable=True
    )
    site = relationship('Site', backref='cssfiles', uselist=False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Form to process data """
        class __F(cls._uiform()):
            exclude = (
                'id',
                'created',
                'updated',
                'site_id',
                'site',
                'fileitem',
            )
            class Meta:
                model = cls
            site = QuerySelectField()
            fileitem = FileField()
            def validate_and_add(self):
                """ Validate and add css file """
                columns = [c for c
                           in cls.columns_names()
                           if not c in self.exclude
                ]
                if self.validate():
                    obj = cls()
                    if Q(cls).filter(
                            cls.site_id == self.site.id,
                            cls.fileitem == self.fileitem)\
                             .count() == 0:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                        obj.site = self.site.data
                        # write fileitem if exists only
                        try:
                            obj.fileitem = self.fileitem.data.__dict__
                        except:
                            pass
                        obj.save(flush=True)
                        return True
                    return True
                return False

            def validate_and_edit(self):
                """ Validate and add css file """
                columns = [c for c
                           in cls.columns_names()
                           if not c in self.exclude
                ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    obj.site = self.site.data
                    # write fileitem if exists only
                    try:
                        obj.fileitem = self.fileitem.data.__dict__
                    except:
                        pass
                    obj.save(flush=True)
                    return True
                return False

        f = __F(*args, **kw)
        f.site.query = Q(rootsdb.Site)
        return f
admin.admin.content_types.append(CSSFile)


class JSFile(mixins.RankedColumnsMixin, Base):
    """
    JavaScripts file asset. Is used only for project side for
    improve extensibility of the project.
    """
    __tablename__ = tables.tablename_('js_files')
    __descr__ = _('javascript files (.js) for project')
    verbose_name = _('js file')
    verbose_name_plural = _('js files')
    site_id = Column(Integer, 
                     ForeignKey(tables.fk_('sites')))
    slot_id = Column(Integer, 
                     ForeignKey(tables.fk_('js_fslots')), nullable=True)
    fileitem = Column(fields.FileStorageField(
        200,
        mimetypes=admin.admin.config.js_mime),
        nullable=True
    )
    site = relationship('Site', backref='jsfiles', uselist=False)
    
    @classmethod
    def _form(cls, *args, **kw):
        """ Form to process data """
        class __F(cls._uiform()):
            exclude = (
                'id',
                'created',
                'updated',
                'site_id',
                'slot_id',
                'fileitem',
            )
            class Meta:
                model = cls
            site = QuerySelectField()
            slot = QuerySelectField(allow_blank=True, blank_text='----')
            fileitem = FileField()
            def validate_and_add(self):
                """ Validate and add css file """
                columns = [c for c
                           in cls.columns_names()
                           if not c in self.exclude
                ]
                if self.validate():
                    obj = cls()
                    if Q(cls).filter(
                            cls.site_id == self.site.id,
                            cls.fileitem == self.fileitem)\
                             .count() == 0:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                        obj.site = self.site.data
                        obj.slot = self.slot.data
                        # write fileitem if exists only
                        try:
                            obj.fileitem = self.fileitem.data.__dict__
                        except:
                            pass
                        obj.save(flush=True)
                        return True
                    return True
                return False

            def validate_and_edit(self):
                """ Validate and add css file """
                columns = [c for c
                           in cls.columns_names()
                           if not c in self.exclude
                ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        setattr(obj,attr, getattr(self, attr).data)
                    obj.site = self.site.data
                    obj.slot = self.slot.data
                    # write fileitem if exists only
                    try:
                        obj.fileitem = self.fileitem.data.__dict__
                    except:
                        pass
                    obj.save(flush=True)
                    return True
                return False

        f = __F(*args, **kw)
        f.site.query = Q(rootsdb.Site)
        f.slot.query = Q(JSFilesSlot)
        return f
admin.admin.content_types.append(JSFile)
