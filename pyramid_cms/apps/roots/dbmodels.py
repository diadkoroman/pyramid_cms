# -*- coding: utf-8 -*-
# PYTHON IMPORTS
import os
import time
import datetime

from wtforms.fields import SelectMultipleField, SelectField, DateField
from wtforms import widgets
from wtforms import validators
from wtforms.ext.sqlalchemy.fields import (
    QuerySelectField,
    QuerySelectMultipleField
    )

# APP IMPORTS
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Base, Metadata, Q
from pyramid_cms.assets.db.sqlalchemy.models import mixins
from pyramid_cms.assets.db.sqlalchemy.models import tables
from pyramid_cms.assets.translations import _db_ts as _

from pyramid_cms.apps.auth import dbmodels as authdb


# SQLALCHEMY IMPORTS
from sqlalchemy import (
        Table,
        Column,
        Boolean,
        ForeignKey,
        Integer,
        String,
        Text,
        Date
        )
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql.functions import func


""" Many to Many between TreeNode and Widget """
pagetypes_to_widgets = Table('pagetypes_to_widgets', Metadata,
                             Column('pagetype_id', Integer,
                                    ForeignKey(tables.fk_('pagetypes'))),
                             Column('widget_id', Integer,
                                    ForeignKey(tables.fk_('pagewidgets'))))


""" M2M between TreeNode and Permission """
nodes_to_perms = Table(tables.tablename_('nodes_to_perms'), Metadata,
                       Column('node_id', Integer,
                              ForeignKey(tables.fk_('tree_nodes'))),
                       Column('perm_id', Integer,
                              ForeignKey(tables.fk_('perms'))))


""" M2M between TreeNode and TreeNodeTag """
nodes_to_tags = Table(tables.tablename_('nodes_to_tags'), Metadata,
                      Column('node_id', Integer,
                             ForeignKey(tables.fk_('tree_nodes'))),
                      Column('tag_id', Integer,
                             ForeignKey(tables.fk_('nodetags'))))


""" M2M between TreeNode and TreeNodeTag """
nodes_to_kwords = Table(tables.tablename_('nodes_to_kwords'), Metadata,
                        Column('node_id', Integer,
                               ForeignKey(tables.fk_('tree_nodes'))),
                        Column('kword_id', Integer,
                               ForeignKey(tables.fk_('nodekwords'))))


"""
M2M between TreeNode and PageWidget
Is useful for set customizable widgets list without creationof new pagetype
"""
nodes_to_widgets = Table('nodes_to_widgets', Metadata,
                         Column('node_id', Integer,
                                ForeignKey(tables.fk_('tree_nodes'))),
                         Column('widget_id', Integer,
                                ForeignKey(tables.fk_('pagewidgets'))))


class Site(mixins.RankedColumnsMixin, Base):
    """ Managed sites. """
    __tablename__ = tables.tablename_('sites')   # pyramid_cms_sites
    __descr__ = _('Web-pages supported by admin interface')
    verbose_name = _('webpage')
    verbose_name_plural = _('webpages')

    def __str__(self):
        return '{0} ({1})'.format(self.inner_name, self.address)

    # адреса сайту
    address = Column(String(100))
    # короткий опис сайту
    description = Column(String(200), nullable=True)

admin.admin.content_types.append(Site)


class Language(mixins.RankedColumnsMixin, Base):
    """ Site languages. """
    __tablename__ = tables.tablename_('langs')   # pyramid_cms_langs
    __descr__ = 'Мови'
    verbose_name = 'мова сайту'
    verbose_name_plural = 'мови сайтів'

    def __str__(self):
        return '{0} ({1})'.format(self.inner_name, self.mnemo)

    site_id = Column(Integer, ForeignKey(tables.fk_('sites')), nullable=True)
    mnemo = Column(String(5), info={'class_': 'form-control'})
    deflang = Column(Boolean, default=False)
    # language using for admin interface
    use_for_admin = Column(Boolean, default=False)
    # language using for project interface
    use_for_project = Column(Boolean, default=False)
    site = relationship('Site', uselist=False)

    @classmethod
    def get_sites(cls):
        return Q(Site)

    @classmethod
    def _form(cls, *args, **kw):
        """ Form to process data """
        UIForm = cls._uiform()

        class __F(UIForm):
            exclude = 'id', 'created', 'updated', 'site_id'

            class Meta:
                model = cls

            site = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [c for c in cls.columns_names() if c not in self.exclude]
                if self.validate():
                    obj = cls()
                    if Q(cls)\
                    .filter(cls.mnemo == self.mnemo.data).count() == 0:
                        for attr in columns:
                            if attr in ('site',):
                                pass
                            else:
                                setattr(obj, attr, getattr(self, attr).data)
                        obj.site = self.site.data
                        obj.save(flush=True)
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [c for c in cls.columns_names() if c not in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            if attr in ('site',):
                                pass
                            else:
                                setattr(obj, attr, getattr(self, attr).data)
                    obj.site = self.site.data
                    obj.save(flush=True)
                    return True
                return False
        f = __F(*args, **kw)
        f.site.query = cls.get_sites()
        return f

admin.admin.content_types.append(Language)


class PageType(mixins.RankedColumnsMixin, Base):
    """ Page types: basic markers for view framework desigion making. """
    __tablename__ = tables.tablename_('pagetypes')   # pyramid_cms_pagetypes
    __descr__ = 'Types of pages'
    verbose_name = 'Type of page'
    verbose_name_plural = 'Types of pages'

    def __str__(self):
        return '{0} ({1})'.format(self.inner_name, self.mnemo)

    site_id = Column(Integer, ForeignKey(tables.fk_('sites')), nullable=True)
    mnemo = Column('mnemo', String(150), unique=True)
    template = Column('template', String(200))
    widgets = relationship('PageWidget',
        secondary=pagetypes_to_widgets,
        backref='pagetypes',
        lazy='dynamic',
        )
    
    def get_widgets_mnemos(self):
        return [i.mnemo for i in self.widgets.filter_by(active=True)]
        
    @classmethod
    def get_widgets(cls):
        return Q(PageWidget)
    
    @classmethod
    def _form(cls, *args, **kw):
        """ Form to process data """
        UIForm = cls._uiform()

        class __F(UIForm):
            exclude = 'id', 'created', 'updated', 'site_id', 'widgets'

            class Meta:
                model = cls
            widgets = QuerySelectMultipleField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [c for c in cls.columns_names() if c not in self.exclude]
                if self.validate():
                    obj = cls()
                    if Q(cls)\
                    .filter(cls.mnemo == self.mnemo.data).count() == 0:
                        for attr in columns:
                            # if attr in ('widgets',):
                            #    pass
                            # else:
                            setattr(obj, attr, getattr(self, attr).data)
                        obj.widgets = self.widgets.data
                        obj.save(flush=True)
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [c for c in cls.columns_names() if c not in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            if attr in ('widgets',):
                                pass
                            else:
                                setattr(obj, attr, getattr(self, attr).data)
                    obj.widgets = self.widgets.data
                    obj.save(flush=True)
                    return True
                return False
        f = __F(*args, **kw)
        f.widgets.query = cls.get_widgets()
        return f

admin.admin.content_types.append(PageType)


class PageWidget(mixins.RankedColumnsMixin, Base):
    """ Widgets: structural components of page. """
    __tablename__ = tables.tablename_('pagewidgets')   # pyramid_cms_widgets
    __descr__ = 'Page widgets'
    verbose_name = 'Page widget'
    verbose_name_plural = 'Page widgets'

    def __str__(self):
        return '{0} ({1})'.format(self.inner_name, self.mnemo)

    site_id = Column(Integer, ForeignKey(tables.fk_('sites')), nullable=True)
    mnemo = Column(String(150), unique = True)
    template_file = Column(String(200))
    site = relationship('Site', uselist=False)

admin.admin.content_types.append(PageWidget)


class TreeNodeTag(mixins.SmallStringSlot, Base):
    """
    Спеціальні мітки(теги), котрі можна при потребі використовувати для
    динамічної категоризації. ПОв’язані залежністю м2м із класом TreeNode
    """
    __tablename__ = tables.tablename_('nodetags')
    verbose_name = _('TreeNode tag')
    verbose_name_plural = _('TreeNode tags')

    def __str__(self):
        return self.content

    # node_id = Column(Integer, ForeignKey(tables.fk_('tree_nodes')))

admin.admin.content_types.append(TreeNodeTag)


class TreeNodeKword(mixins.SmallStringSlot, Base):
    """
    Ключові слова для ноди (раптом знадобиться?).
    Пов’язані залежністю м2м із класом TreeNode
    """
    __tablename__ = tables.tablename_('nodekwords')
    verbose_name = _('TreeNode keyword')
    verbose_name_plural = _('TreeNode keywords')

    def __str__(self):
        return self.content

    # node_id = Column(Integer, ForeignKey(tables.fk_('tree_nodes')))

admin.admin.content_types.append(TreeNodeKword)


class TreeNode(mixins.RankedColumnsMixin, Base):
    """ Site tree nodes. """
    __tablename__ = tables.tablename_('tree_nodes')   # pyramid_cms_tree_nodes
    __descr__ = """
    Вузли (ноди) дерева сайту. Самі по собі не є розділами - використовуються
    лише для прив'язки розділу до дерева сайту.
    """
    verbose_name = 'tree node'
    verbose_name_plural = 'tree nodes'
    
    def __str__(self):
        return '{0} ({1})'.format(self.inner_name, self.get_path())
    
    site_id = Column(Integer, ForeignKey(tables.fk_('sites')))
    mnemo = Column('mnemo', String(150))
    pagetype_id = Column(Integer, ForeignKey(tables.fk_('pagetypes')))
    parent_id = Column(Integer,
                       ForeignKey(tables.fk_('tree_nodes')), nullable=True)
    sibling_id = Column(Integer,
                        ForeignKey(tables.fk_('tree_nodes')), nullable=True)
    redirect_id = Column(Integer,
                         ForeignKey(tables.fk_('tree_nodes')), nullable=True)
    redir_address = Column(String(150), nullable=True)
    """
    параметр, котрий вказує, чи належить нода до адміністративного
    розділу дерева.
    """
    admin_node = Column(Boolean, default=False)
    """
    параметр, котрий вказує, чи повинна нода відображатися
    як самостійна сторінка.
    """
    invisible_node = Column(Boolean, default=False)
    date_visible = Column(Date, default=func.now())
    site = relationship('Site', uselist=False)
    pagetype = relationship('PageType',
                            backref='tree_nodes',
                            uselist=False,
                            )
    #parent = relationship('TreeNode',
    #                      foreign_keys='TreeNode.parent_id',
    #                      uselist=False,
    #                      cascade='all, delete-orphan',
    #                      )
    children = relationship('TreeNode',
                            lazy='dynamic',
                            foreign_keys='TreeNode.parent_id',
                            #remote_side='TreeNode.parent_id',
                            cascade='all, delete-orphan',
                            order_by='TreeNode.rank'
                            )
    permissions = relationship('Permission',
                               secondary=nodes_to_perms,
                               backref = 'tree_nodes',
                               lazy='dynamic',
                               )
    titles = relationship('PageTitle',
                          lazy='dynamic',
                          cascade='all, delete-orphan'
    )
    descriptions = relationship('PageDescription',
                                lazy='dynamic',
                                cascade='all, delete-orphan'
    )
    texts = relationship('PageText',
                         lazy='dynamic',
                         cascade='all, delete-orphan'
    )
    tags = relationship('TreeNodeTag',
                        secondary=nodes_to_tags,
                        backref=backref('nodes'),
    )
    # set of custom widgets for exact node
    widgets = relationship('PageWidget',
                           secondary=nodes_to_widgets
    )
    kwords = relationship('TreeNodeKword',
                        secondary=nodes_to_kwords,
                        backref=backref('nodes')
    )

    @property
    def parent(self):
        try:
            return Q(TreeNode).get(self.parent_id)
        except:
            return None
            
    @property
    def children(self):
        try:
            return Q(TreeNode).filter(TreeNode.parent_id == self.id)
        except:
            return None

    @classmethod
    def get_sites(cls):
        return Q(Site)
        
    @classmethod
    def get_pagetypes(cls):
        return Q(PageType)
        
    @classmethod
    def get_treenodes(cls, **kw):
        excluded_id = kw.get('excluded_id', None)
        admin_node = kw.get('admin_node', None)
        q = Q(TreeNode)
        if excluded_id:
            q = q.filter(TreeNode.id != excluded_id)
        if not admin_node is None:
            q = q.filter(TreeNode.admin_node == int(admin_node))
        return q
        
    @classmethod
    def get_perms(cls):
        return Q(authdb.Permission)
        
    # get path
    def get_path(self):
        path = []
        def _get_path(node, stack):
            if not node.mnemo == '/':
                path.append(node.mnemo)
            if node.parent_id not in (None, ''):
                _get_path(node.parent, stack)
        _get_path(self, path)
        path.reverse()
        return '/{0}'.format('/'.join(path))
        
    # get path
    @staticmethod
    def get_path2(node):
        path = []
        def _get_path(node, stack):
            if not node.mnemo == '/':
                path.append(node.mnemo)
            if node.parent_id not in (None, ''):
                _get_path(node.parent, stack)
        _get_path(node, path)
        path.reverse()
        return '/{0}'.format('/'.join(path))

    # get breadcrumbs
    def get_bcrs(self):
        bcrs = []
        def _get_bcrs(node, stack, **kw):
            if not node.mnemo == '/':
                bcr = [node.get_path(),node.inner_name, kw.get('active', True)]
                stack.append(bcr)
            if node.parent_id not in (None,''):
                _get_bcrs(node.parent, stack)
        _get_bcrs(self, bcrs, active=False)
        bcrs.reverse()
        return bcrs
    
    
    # get breadcrumbs
    @staticmethod
    def get_bcrs2(node):
        bcrs = []
        def _get_bcrs(node, stack, **kw):
            if not node.mnemo == '/':
                bcr = [node.get_path2(node),node.inner_name, kw.get('active', True)]
                stack.append(bcr)
            if node.parent_id not in (None,''):
                _get_bcrs(node.parent, stack)
        _get_bcrs(node, bcrs, active=False)
        bcrs.reverse()
        return bcrs

    @classmethod
    def _add_form(cls, *args, **kw):
        UIForm = cls._uiform()
        class __F(UIForm):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'site_id',
                       'pagetype_id',
                       'parent_id',
                       'sibling_id',
                       'redirect_id',
                       'site',
                       'pagetype',
                       'parent',
                       'permissions',
                       'date_visible',
                       )
            class Meta:
                model = cls
            site = QuerySelectField()
            pagetype = QuerySelectField()
            parent = QuerySelectField()
            permissions = QuerySelectMultipleField()
            
            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    if Q(cls)\
                    .filter(cls.mnemo == self.mnemo.data,
                    cls.parent_id == self.parent.data.id)\
                    .count() == 0:
                        for attr in columns:
                            #if attr in ('site','pagetype','parent','permissions'):
                            #    pass
                            #else:
                            setattr(obj,attr, getattr(self, attr).data)
                        obj.site = self.site.data
                        obj.pagetype = self.pagetype.data
                        """
                        Мнемо-назва формується на основі поточного часового зрізу
                        якщо не зазначено нічого іншого
                        """
                        if self.mnemo.data in ('', False, None):
                            obj.mnemo = str(int(time.time()))
                        obj.parent_id = self.parent.data.id
                        obj.permissions = self.permissions.data
                        obj.save(flush=True)
                    return True
                return False
        f = __F(*args, **kw)
        f.site.query = cls.get_sites()
        f.pagetype.query = cls.get_pagetypes()
        f.parent.query = cls.get_treenodes()
        f.permissions.query = cls.get_perms()
        return f

    @classmethod
    def _edit_form(cls, *args, **kw):
        UIForm = cls._uiform()
        class __F(UIForm):
            exclude = (
                       'id',
                       'created',
                       'updated',
                       'site_id',
                       'pagetype_id',
                       'parent_id',
                       'sibling_id',
                       'redirect_id',
                       'site',
                       'pagetype',
                       'parent',
                       'permissions',
                       'date_visible'
                       )
            class Meta:
                model = cls
            site = QuerySelectField()
            pagetype = QuerySelectField()
            parent = QuerySelectField()
            permissions = QuerySelectMultipleField()
            
            def validate_and_edit(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        #if attr in ('site','pagetype','parent','permissions'):
                        #    pass
                        #else:
                        setattr(obj,attr, getattr(self, attr).data)
                    obj.site = self.site.data
                    obj.pagetype = self.pagetype.data
                    """
                    Мнемо-назва формується на основі поточного часового зрізу
                    якщо не зазначено нічого іншого
                    """
                    if self.mnemo.data in ('', False, None):
                        obj.mnemo = str(int(time.time()))
                    obj.parent_id = self.parent.data.id
                    obj.permissions = self.permissions.data
                    obj.save(flush=True)
                    return True
                return False
        f = __F(*args, **kw)
        f.site.query = cls.get_sites()
        f.pagetype.query = cls.get_pagetypes()
        f.parent.query = cls.get_treenodes(excluded_id=f._obj.id,admin_node=f._obj.admin_node)
        f.permissions.query = cls.get_perms()
        return f

    @classmethod
    def ui(cls, *args, **kw):
        #UIForm = cls._uiform()
        option = kw.get('req', None)
        if option and 'add' in option:
            f = cls._add_form(*args, **kw)
        else:
            f = cls._edit_form(*args, **kw)
        return f
    
    @classmethod
    def table(cls, **kw):
        kw['order_by'] = 'rank'
        return super().table(**kw)

    def title_localized(self, lang_id):
        try:
            loc = self.titles.filter_by(lang_id=lang_id).first()
            if loc:
                return loc.content
            return self.inner_name
        except:
            return self.inner_name
            
    def description_localized(self, lang_id):
        # get description for node in locale needed
        try:
            loc = self.descriptions.filter_by(lang_id=lang_id).first()
            if loc not in (None, '', False):
                return loc.content
            return self.inner_name
        except:
            return self.inner_name
            
    def get_kwords(self):
        # get node keywords
        try:
            kwords = [i for i in self.kwords]
            if kwords:
                return kwords
            return self.inner_name
        except:
            return self.inner_name

admin.admin.content_types.append(TreeNode)


class PageTitle(mixins.LiteColumnsMixin, Base):
    __tablename__ = tables.tablename_('pagetitles')
    verbose_name = _('page title')
    verbose_name_plural = _('page titles')
    node_id = Column(Integer, ForeignKey(tables.fk_('tree_nodes')))
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))
    content = Column(String(150), nullable=True)
    node = relationship('TreeNode')
    lang = relationship('Language', uselist=False)
    
    def __str__(self):
        return 'Title for {0} ({1})'.format(self.node.inner_name, self.lang.inner_name)

admin.admin.content_types.append(PageTitle)


class PageDescription(mixins.LiteColumnsMixin, Base):
    __tablename__ = tables.tablename_('pagedescrs')
    verbose_name = _('page short description')
    verbose_name_plural = _('page short descriptions')
    node_id = Column(Integer, ForeignKey(tables.fk_('tree_nodes')))
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))
    content = Column(Text, nullable=True)
    node = relationship('TreeNode',
                        uselist=False
                        )
    lang = relationship('Language', uselist=False)
    
    def __str__(self):
        return 'Description for {0} ({1})'.format(self.node.inner_name, self.lang.inner_name)

admin.admin.content_types.append(PageDescription)


class PageText(mixins.LiteColumnsMixin, Base):
    __tablename__ = tables.tablename_('pagetexts')
    verbose_name = _('page text')
    verbose_name_plural = _('page texts')
    node_id = Column(Integer, ForeignKey(tables.fk_('tree_nodes')))
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))
    content = Column(Text, nullable=True)
    node = relationship('TreeNode',
                        uselist=False
                        )
    lang = relationship('Language', uselist=False)
    
    def __str__(self):
        return 'Text for {0} ({1})'.format(self.node.inner_name, self.lang.inner_name)

admin.admin.content_types.append(PageText)
