# -*- coding: utf-8 -*-
""" Моделі для додатку "Авторизація" """
# PYTHON IMPORTS
# import os
# import time
# import transaction

# from wtforms.fields import SelectMultipleField, PasswordField
from wtforms import widgets
from wtforms import validators
from wtforms.ext.sqlalchemy.fields import QuerySelectMultipleField

# APP IMPORTS
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Base, Metadata, Q
from pyramid_cms.assets.db.sqlalchemy.models import mixins
from pyramid_cms.assets.db.sqlalchemy.models import tables
from pyramid_cms.assets.handlers import encr

# SQLALCHEMY IMPORTS
from sqlalchemy import (
    Table,
    Column,
    ForeignKey,
    Integer,
    String,
    Text
    )
from sqlalchemy.orm import relationship, backref
# from sqlalchemy.sql.functions import func
from sqlalchemy.orm.exc import NoResultFound

# таблиця з нарізкою прав для груп
# sitedesk_groups_to_perms
groups_to_perms = Table(
    tables.tablename_('groups_to_perms'),
    Metadata,
    Column('group_id', Integer, ForeignKey(tables.fk_('groups'))),
    Column('permission_id', Integer, ForeignKey(tables.fk_('perms')))
)

# таблиця з членством користувачів у групах
# sitedesk_users_to_group
users_to_groups = Table(
    tables.tablename_('users_to_groups'),
    Metadata,
    Column('user_id', ForeignKey(tables.fk_('users'))),
    Column('group_id', ForeignKey(tables.fk_('groups')))
)


# користувачі адмінки
#############################################
class User(mixins.StandardColumnsMixin, Base):
    """ Модель БД "Користувач" """
    __tablename__ = tables.tablename_('users')   # pyradmin_users
    verbose_name = 'користувач'
    verbose_name_plural = 'користувачі'
    exclude = 'created', 'updated', 'password'

    def __str__(self):
        return self.login

    login = Column(String(20))
    password = \
        Column(Text,
               info={'widget': widgets.PasswordInput(validators.Required())})
    token = Column(Text, nullable=True)
    name = Column(String(50), nullable=True)
    surname = Column(String(100), nullable=True)
    groups = relationship('Group',
                          secondary=users_to_groups,
                          lazy='joined',
                          backref=backref('users', lazy='joined'))

    @classmethod
    def check_superusers(cls):
        """ Method for check superusers list. """
        # check group
        try:
            superusers = Group.get_by_mnemo('superusers').first()
            if superusers is not None:
                return superusers.users
            return False
        except NoResultFound:
            return False

    @staticmethod
    def get_groups():
        """  """
        return Q(Group)

    @classmethod
    def _add_form(cls, *args, **kw):
        """ Form for add items """
        UIForm = cls._uiform()

        class __F(UIForm):
            exclude = 'id', 'created', 'updated', 'password', 'token'

            class Meta:
                model = cls
            groups = QuerySelectMultipleField()

            def validate_and_add(self):
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    passw = encr.encr1(getattr(self, 'password').data)
                    if Q(cls).filter(cls.password == passw).count() == 0:
                        for attr in columns:
                            if attr in ('groups',):
                                # groups should be processed later
                                pass
                            else:
                                setattr(obj, attr, getattr(self, attr).data)
                        obj.password = encr.encr1(self.password.data)
                        obj.token = encr.tokenize(obj.login)
                        obj.groups = self.groups.data
                        obj.save(flush=True)
                    return True
                return False
        f = __F(*args, **kw)
        f.groups.query = cls.get_groups()
        return f

    @classmethod
    def _edit_form(cls, *args, **kw):
        """ Form for editing items """
        UIForm = cls._uiform()

        class __F(UIForm):
            include = 'inner_name', 'login', 'groups'

            class Meta:
                model = cls
                only = ['inner_name', 'login']
            groups = QuerySelectMultipleField()

            def validate_and_edit(self):
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c in self.include
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        if attr in ('groups',):
                            # groups should be processed later
                            pass
                        else:
                            setattr(obj, attr, getattr(self, attr).data)
                    if obj.token in (None, False, '') or not obj.token:
                        obj.token = encr.tokenize(obj.login)
                    obj.groups = self.groups.data
                    obj.save(flush=True)
                    return True
                return False
        f = __F(*args, **kw)
        f.groups.query = cls.get_groups()
        return f

    @classmethod
    def ui(cls, *args, **kw):
        option = kw.get('req', None)
        if option and 'add' in option:
            f = cls._add_form(*args, **kw)
        else:
            f = cls._edit_form(*args, **kw)
        return f

admin.admin.content_types.append(User)

#############################################


# групи користувачів адмінки
#############################################
class Group(mixins.StandardColumnsMixin, Base):
    __tablename__ = tables.tablename_('groups')   # pyradmin_groups
    __descr__ = 'Групи користувачів сайту'
    verbose_name = 'група користувачів'
    verbose_name_plural = 'групи користувачів'

    name = Column(String(50), nullable=True)
    perms = relationship('Permission',
                         secondary=groups_to_perms,
                         backref='groups',
                         lazy='joined')

    @classmethod
    def get_by_mnemo(cls, mnemo, empty_boolean=False):
        """
        Get group by its mnemo-name.
        If there are no group with a such mnemo
        emty list returns
        """
        try:
            return Q(cls).filter_by(name=mnemo)
        except:
            if empty_boolean:
                """
                For special cases it might be need get bool
                from this method.
                """
                return False
            return []

    @classmethod
    def get_perms(cls):
        return Q(Permission)

    @classmethod
    def _form(cls, *args, **kw):
        UIForm = cls._uiform()

        class __F(UIForm):
            exclude = 'id', 'created', 'updated'

            class Meta:
                model = cls
            perms = QuerySelectMultipleField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    if Q(cls).filter(cls.name == self.name.data).count() == 0:
                        for attr in columns:
                            if attr in ('perms',):
                                pass
                            else:
                                setattr(obj, attr, getattr(self, attr).data)
                        obj.perms = self.perms.data
                        obj.save(flush=True)
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            if attr in ('perms',):
                                pass
                            else:
                                setattr(obj, attr, getattr(self, attr).data)
                    obj.perms = self.perms.data
                    obj.save(flush=True)
                    return True
                return False
        f = __F(*args, **kw)
        f.perms.query = cls.get_perms()
        return f

admin.admin.content_types.append(Group)

##############################################


# типи доступів для користувачів адмінки
##############################################
class Permission(mixins.StandardColumnsMixin, Base):
    __tablename__ = tables.tablename_('perms')   # pyradmin_perms
    __descr__ = 'Типи доступів, які користувачі сайту можуть мати'
    verbose_name = 'тип доступу'
    verbose_name_plural = 'типи доступів'

    name = Column('name', String(50), nullable=True)

    def __str__(self):
        return 'Тип доступу \'{0}\''.format(self.name)

admin.admin.content_types.append(Permission)
##################################################
