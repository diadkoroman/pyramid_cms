# -*- coding: utf-8 -*-
# PYTHON IMPORTS
import os
import time
import datetime

from wtforms.fields import SelectMultipleField, SelectField, DateField
from wtforms import widgets
from wtforms import validators
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField

# APP IMPORTS
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Base, Metadata, Q
from pyramid_cms.assets.db.sqlalchemy.models import mixins
from pyramid_cms.assets.db.sqlalchemy.models import tables
from pyramid_cms.assets.translations import _db_ts as _

from pyramid_cms.apps.auth import dbmodels as authdb


# SQLALCHEMY IMPORTS
from sqlalchemy import (
        Table,
        Column,
        Boolean,
        ForeignKey,
        Integer,
        Numeric,
        String,
        Text,
        Date
        )
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql.functions import func

# Моделі БД, що відповідають за створення таблиць, що містять дані про
# статистику

##############################################################################
class Visit(mixins.LiteColumnsMixin,Base):
    __tablename__ = tables.tablename_('visits') # pyradmin_visits
    __descr__ = _('Статистика відвідування розділів(у запитах)')
    verbose_name = _('статистика відвідувань')
    verbose_name_plural = _('статистика відвідувань')
    
    add_option = False
    edit_option = False
    
    def __str__(self):
        return '{0} ({1})'.format(self.client_ip, self.requested_url)

    site_id = Column(Integer,ForeignKey(tables.fk_('sites')))
    client_ip = Column(String(50))
    requested_url = Column(String(150))
    status = Column(String(20),nullable = True)
    answer_time = Column(Numeric(precision=5, scale=3), default=00.000)
    # custom table
    @classmethod
    def table(cls,**kw):
        kw['order_by'] = 'created'
        kw['desc'] = True
        kw['limit'] = 100
        return super().table(**kw)

admin.admin.content_types.append(Visit)
##############################################################################


##############################################################################
class VisitCounter(mixins.LiteColumnsMixin,Base):
    __tablename__ = tables.tablename_('counters') # pyradmin_counters
    __descr__ = _('Лічильник відвідувань розділів')
    verbose_name = _('лічильник відвідувань')
    verbose_name_plural = _('лічильники відвідувань')
    
    add_option = False
    edit_option = False
    
    def __str__(self):
        return self.requested_url

    site_id = Column(Integer,ForeignKey(tables.fk_('sites')))
    requested_url = Column(String(150))
    visits = Column(Integer,default = 0)
    aver_answer_time = Column(Numeric(precision=6, scale=4), default=00.0000)
    
   # custom table
    @classmethod
    def table(cls,**kw):
        kw['order_by'] = 'visits'
        kw['desc'] = True
        kw['limit'] = 100
        return super().table(**kw)

admin.admin.content_types.append(VisitCounter)
##############################################################################

