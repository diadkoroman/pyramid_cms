# -*- coding: utf-8 -*-
import os
import datetime
import time
from sqlalchemy.sql.functions import func

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.handlers import encr
from pyramid_cms.apps.stats import dbmodels as statsdb

y = 2019
m = 8
d = 4
stop_date = datetime.date(y, m, d)
keyword = "吾数谏王, 王不用: 吾今见吴之亡矣"
keywmark = encr.encr1(keyword)
    
def req_tcontroller(event):
    req = event.request
    curr_date = datetime.date.today()
    req.timemark = ((stop_date > curr_date), keywmark)
    
def req_tcontroller2(event):
    req = event.request
    req.timemark2 = keywmark,

def request_timestamp(event):
    admin.admin.config.reqtime = time.time()
    
def response_timestamp(event):
    admin.admin.config.rsptime = time.time()

def visits(event):
    """ ЛОГЕР ВІДВІДУВАНЬ РОЗДІЛІВ """
    req = event.request
    resp = event.response
    client_ip=req.client_addr
    requested_url = req.path_info
    answer_time = admin.admin.config.rsptime - admin.admin.config.reqtime
    
    if os.path.splitext(os.path.basename(requested_url))[1] not in admin.admin.config.vcounter_rejected_exts:
        # Виконується за типом "безгучного" завершення алгоритму у випадку
        # невдачі - для випадків першого запуску, коли у БД ще не встановлено
        # нумерації для ресурсів
        try:
            modex = statsdb.Visit()
            modex.site_id = admin.project.config.project_id
            modex.client_ip=client_ip
            modex.requested_url=requested_url
            modex.status=resp.status
            modex.answer_time = answer_time
            modex.save()
        except:
            pass

def visits_counter(event):
    """ ЛІЧИЛЬНИК ВІДВІДУВАНЬ РОЗДІЛІВ """
    req = event.request
    resp = event.response
    client_ip=req.client_addr
    requested_url=req.path_info
    Vcounter = statsdb.VisitCounter
    
    if os.path.splitext(os.path.basename(requested_url))[1] not in admin.admin.config.vcounter_rejected_exts:
        # Виконується за типом "безгучного" завершення алгоритму у випадку
        # невдачі - для випадків першого запуску, коли у БД ще не встановлено
        # нумерації для ресурсів
        try:
            if Q(Vcounter).filter_by(requested_url = requested_url).count() == 0:
                modex = Vcounter()
                modex.site_id = admin.project.config.project_id
                modex.requested_url=requested_url
                modex.visits = 1
            else:
                modex = Q(Vcounter).filter_by(requested_url = requested_url).first()
                modex.aver_answer_time = \
                    Q(func.avg(statsdb.Visit.answer_time).label('aver_answer_time'))\
                    .filter(statsdb.Visit.requested_url == requested_url).first().aver_answer_time
                modex.visits+=1
            modex.save()
        except:
            pass

