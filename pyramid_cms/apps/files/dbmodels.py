# -*- coding: utf-8 -*-
import time
"""
Database models for file management.
"""

# SQLALCHEMY IMPORTS
from sqlalchemy import (
    Table,
    Column,
    ForeignKey,
    Integer,
    String)
from sqlalchemy.orm import relationship

from wtforms.fields import FileField
from wtforms.ext.sqlalchemy.fields import (
    QuerySelectField,
    QuerySelectMultipleField
    )

# APP IMPORTS
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Base, Metadata, Q
from pyramid_cms.assets.db.sqlalchemy.models import mixins
from pyramid_cms.assets.db.sqlalchemy.models import tables
from pyramid_cms.assets.db.sqlalchemy import fields
from pyramid_cms.assets.translations import _db_ts as _

from pyramid_cms.apps.roots import dbmodels as rootdb


"""
Many-to-many relationship table between fileslots and uploaded files.
"""
slots_fitems = Table(
    tables.tablename_('slots_fitems'),  # pyradmin_slots_fitems
    Metadata,
    Column('slot_id', Integer, ForeignKey(tables.fk_('fslots'))),
    Column('fitem_id', Integer, ForeignKey(tables.fk_('fitems')))
)


"""
Many-to-many relationship table between categories and files
"""
fcategs_fitems = Table(
    tables.tablename_('fcategs_fitems'),    # pyradmin_fcategs_fitems
    Metadata,
    Column('categ_id', Integer, ForeignKey(tables.fk_('fcategs'))),
    Column('fitem_id', Integer, ForeignKey(tables.fk_('fitems'))),
)


class FileSlot(mixins.RankedColumnsMixin, Base):
    """
    Universal class describing stacks for
    files, banners, galleryitems etc.
    """
    __tablename__ = tables.tablename_('fslots')     # fcbank_fslots
    __descr__ = _('Перелік слотів для розміщення файлів')
    verbose_name = _('file slot')
    verbose_name_plural = _('file slots')

    def __str__(self):
        return self.inner_name

    site_id = Column(Integer, ForeignKey(tables.fk_('sites')))
    mnemo = Column('mnemo', String(100))
    site = relationship('Site', uselist=False)
    files = relationship('UploadedFile',
                         secondary=slots_fitems,
                         order_by='UploadedFile.rank',
                         lazy='dynamic')

    @classmethod
    def _form(cls, *args, **kw):
        """ Form to process data """
        class __F(cls._uiform()):
            exclude = (
                'id',
                'created',
                'updated',
                'site_id',
                'site'
                )

            class Meta:
                model = cls

            site = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    if Q(cls)\
                    .filter(cls.mnemo == self.mnemo.data).count() == 0:
                        for attr in columns:
                            setattr(obj, attr, getattr(self, attr).data)
                        obj.site = self.site.data
                        obj.save(flush=True)
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        setattr(obj, attr, getattr(self, attr).data)
                    obj.site = self.site.data
                    obj.save(flush=True)
                    return True
                return False

        f = __F(*args, **kw)
        f.site.query = Q(rootdb.Site)
        return f

admin.admin.content_types.append(FileSlot)


class FileCategory(mixins.RankedColumnsMixin, Base):
    """
    Class describes categorization inside FileSlot.
    """
    __tablename__ = tables.tablename_('fcategs')    # fcbank_fcategs
    __descr__ = _('Перелік категорій для банерних слотів')
    verbose_name = _('file category')
    verbose_name_plural = _('file categories')

    def __str__(self):
        return self.inner_name

    mnemo = Column('mnemo', String(100))

    @classmethod
    def _form(cls, *args, **kw):
        """ Form to process data """
        class __F(cls._uiform()):
            exclude = (
                'id',
                'created',
                'updated',
                'site_id',
                'site'
                )

            class Meta:
                model = cls

            site = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    if Q(cls)\
                    .filter(cls.mnemo == self.mnemo.data)\
                    .count() == 0:
                        for attr in columns:
                            setattr(obj, attr, getattr(self, attr).data)
                            obj.site = self.site.data
                            obj.save(flush=True)
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj, attr, getattr(self, attr).data)
                            obj.site = self.site.data
                            obj.save(flush=True)
                    return True
                return False

        f = __F(*args, **kw)
        f.site.query = Q(rootdb.Site)
        return f

admin.admin.content_types.append(FileCategory)


class UploadedFile(mixins.RankedColumnsMixin, Base):
    """
    Uploaded file item.
    """
    __tablename__ = tables.tablename_('fitems')   # pyradmin_fitems
    __descr__ = _('')
    verbose_name = _('uploaded file')
    verbose_name_plural = _('uploaded files')

    def __str__(self):
        return self.inner_name

    fpath = Column(fields.FileStorageField(
        200,
        mimetypes=admin.admin.config.docs_mime,
        ))
    descriptor = Column(
        String(150),
        default=str(int(time.time()))
        )
    slots = relationship('FileSlot',
                         secondary=slots_fitems,
                         order_by='FileSlot.rank',
                         lazy='dynamic')
    categs = relationship('FileCategory',
                          secondary=fcategs_fitems,
                          order_by='FileCategory.rank',
                          lazy='dynamic')
    names = relationship('UploadedFileName',
                         lazy='dynamic',
                         cascade='all, delete, delete-orphan')

    # Повертає своє значення rank
    def get_rank(self):
        return self.rank

    '''
    Повертає назву файлу для зазначеної локалі
    '''
    def localized(self, lang_id):
        try:
            return self.names.filter_by(lang_id=lang_id).first().content
        except:
            return self.inner_name

    '''
    Повертає назву файлу для зазначеної локалі (тільки текст)
    '''
    def content_localized(self, lang_id):
        try:
            return self.names.filter_by(lang_id=lang_id).first().content
        except:
            return False

    @classmethod
    def _form(cls, *args, **kw):
        """ Form to process data """
        class __F(cls._uiform()):
            exclude = (
                'id',
                'created',
                'updated',
                'fpath',
                'slots',
                'categs',
                'names'
                )

            class Meta:
                model = cls

            fpath = FileField()
            slots = QuerySelectMultipleField(
                allow_blank=True,
                blank_text='----')
            categs = QuerySelectMultipleField(
                allow_blank=True,
                blank_text='----')

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    #if Q(cls).filter(
                    #    cls.descriptor == self.descriptor.data)\
                    #        .count() == 0:
                    for attr in columns:
                        setattr(obj, attr, getattr(self, attr).data)
                    # if self.fpath not in (None, ''):
                    try:
                        obj.fpath = self.fpath.data.__dict__
                    except:
                        pass
                    obj.descriptor = str(int(time.time()))
                    obj.slots = self.slots.data
                    obj.categs = self.categs.data
                    obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        setattr(obj, attr, getattr(self, attr).data)
                    # if self.fpath not in (None, ''):
                    try:
                        obj.fpath = self.fpath.data.__dict__
                    except:
                        pass
                    obj.slots = self.slots.data
                    obj.categs = self.categs.data
                    obj.save(flush=True)
                    return True
                return False

        f = __F(*args, **kw)
        f.slots.query = Q(FileSlot)
        f.categs.query = Q(FileCategory)
        return f

    @classmethod
    def table(cls, **kw):
        kw['limit'] = 20
        kw['order_by'] = 'id'
        kw['desc'] = True
        return super().table(**kw)
admin.admin.content_types.append(UploadedFile)


class UploadedFileDescriptor(mixins.LiteColumnsMixin, Base):
    __tablename__ = tables.tablename_('fd')     # pyradmin_fd
    verbose_name = _('file descriptor')
    verbose_name_plural = _('file descriptors')

    def __str__(self):
        return self.descriptor

    fitem_id = Column(Integer, ForeignKey(tables.fk_('fitems')))
    name = Column(String(150))
    #path = Column(String(150))
    descriptor = \
        Column(String(150), default=str(int(time.time())), unique=True)

admin.admin.content_types.append(UploadedFileDescriptor)


class UploadedFileName(mixins.LongStringSlot, Base):
    __tablename__ = tables.tablename_('fitem_names')    # pyradmin_fitem_names
    __descr__ = _('Назви файлів')
    verbose_name = _('file name')
    verbose_name_plural = _('file names')

    def __str__(self):
        return self.content

    # мовна локалізація
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))
    # файл
    item_id = Column(Integer,
                     ForeignKey(tables.fk_('fitems'), ondelete='CASCADE'))
    lang = relationship('Language', uselist=False)
    fileitem = relationship('UploadedFile', uselist=False)

    @classmethod
    def _form(cls, *args, **kw):
        """ Форма для внесення даних про локалі слоту. """

        class __F(cls._uiform()):
            exclude = 'id', 'created', 'updated', 'item_id', 'lang_id'

            class Meta:
                model = cls
            lang = QuerySelectField()
            fileitem = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = cls()
                    if Q(cls)\
                    .filter(cls.item_id == self.fileitem.data.id,
                            cls.lang_id == self.lang.data.id)\
                    .count() == 0:
                        for attr in columns:
                            setattr(obj, attr, getattr(self, attr).data)
                        obj.fileitem = self.fileitem.data
                        obj.lang = self.lang.data
                        obj.save()

            def validate_and_edit(self):
                # edit existing instance
                columns = \
                    [
                        c for c in cls.columns_names()
                        if c not in self.exclude
                    ]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj, attr, getattr(self, attr).data)
                    obj.fileitem = self.fileitem.data
                    obj.lang = self.lang.data
                    obj.save()
        f = __F(*args, **kw)
        f.fileitem.query = Q(UploadedFile)
        f.lang.query = Q(rootdb.Language)
        return f

admin.admin.content_types.append(UploadedFileName)
