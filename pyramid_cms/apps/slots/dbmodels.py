# -*- coding: utf-8 -*-

"""
 __doc__ here
"""

import time

# SQLALCHEMY IMPORTS
from sqlalchemy import (
        Table,
        Column,
        Date,
        Boolean,
        Numeric,
        ForeignKey,
        Integer,
        String,
        Text
        )
from sqlalchemy.orm import relationship    # backref
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.expression import asc, desc

from wtforms.fields import FileField
from wtforms import widgets
from wtforms import validators
from wtforms.ext.sqlalchemy.fields import (
    QuerySelectField,
    QuerySelectMultipleField
)

# APP IMPORTS
from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy.connection import Base, Metadata, Q
from pyramid_cms.assets.db.sqlalchemy.models import mixins
from pyramid_cms.assets.db.sqlalchemy.models import tables
from pyramid_cms.assets.db.sqlalchemy import fields
from pyramid_cms.assets.translations import _db_ts as _
from pyramid_cms.apps.roots import dbmodels as rootsdb

# from pyramid_cms.apps.roots import dbmodels as rootsdb

'''
class TemplateModel(Base):
    """
    Template model.
    ===============
    __tablename__ - use tables.tablename function to define model
    with admin prefix
                    e.g. 'pyradmin_<name-of-table>',
                    and tables.app_tablename function to define app prefix
                    e.g. '<name-of-app>_<name-of-table>'.
                    '_' in __tablename__ is set by theese functions.
    __descr__ - use for human readable short description of model on
    admin side.
                is not implemented yet.
    verbose_name - human readable name of table.
    verbose_name_plural - verbose_name for plural
    """
    __tablename__ =
    tables.tablename('<name-of-table>')/tables.app_tablename('<name-of-table>')
    __descr__ = '<description(optional)>'
    verbose_name = _('<verbose name of model>', default='<default name>')
    verbose_name_plural = _('<verbose name of model(plural)>',
    default='<default name>')
    tables.fk_ - foreign key table definition for admin tables
    (standard components of PyrAdmin)
                tables.fk_('<name-of-table>') gives string value of forein key
                to table 'pyradmin_<name-of-table>'.
    tables.app_fk_ - foreign key table definition for app tables.
                    tables.app_fk_(<name-of-table>) gives string
                    value of forein key
                to table '<name-of-app>_<name-of-table>'.

    def __str__(self):
        """ String presentation of model. """
        pass
'''

def get_sites():
    """ Отримати список сайтів. """
    try:
        return Q(rootsdb.Site)\
            .order_by(asc(rootsdb.Site.rank))
    except:
        return None

def get_string_slots():
    """ Отримати список рядкових слотів. """
    try:
        return Q(FreeString)\
            .order_by(asc(FreeString.mnemo))
    except:
        return []

def get_text_slots():
    """ Отримати список текстових слотів. """
    try:
        return Q(FreeText)\
            .order_by(asc(FreeText.mnemo))
    except:
        return None

def get_langs():
    """ Отримати список мов. """
    try:
        return Q(rootsdb.Language)\
            .order_by(desc(rootsdb.Language.deflang))
    except:
        return None


class FreeString(mixins.LiteColumnsMixin, Base):
    __tablename__ = tables.tablename_('strings')    # pyramid_cms_strings
    __descr__ = 'Вільні рядкові слоти для окремих написів'
    verbose_name = _('рядковий слот')
    verbose_name_plural = _('рядкові слоти')

    def __str__(self):
        """ String presentation of model. """
        return self.mnemo

    site_id = Column(Integer, ForeignKey(tables.fk_('sites')))
    mnemo = Column('mnemo', String(150), default=str(int(time.time())))
    active = Column('active', Boolean, default=True)
    names = relationship('FreeStringName',
                         cascade='all, delete, delete-orphan',
                         lazy='dynamic')
    site = relationship('Site', uselist=False)

    def localized(self, lang_id):
        try:
            locale = self.names\
                .filter_by(lang_id=lang_id)\
                .first()
            if locale is not None:
                return locale.content
            return self.mnemo
        except:
            return self.mnemo

    @classmethod
    def _form(cls, *args, **kw):
        """ Форма для внесення даних про слот. """
        class __F(cls._uiform()):
            exclude = 'id','created','updated','site_id','site'
            class Meta:
                model = cls
            site = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    if Q(cls).filter(cls.mnemo == self.mnemo.data).count() == 0:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                        obj.site = self.site.data
                        obj.save(flush=True)
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    obj.site = self.site.data
                    obj.save(flush=True)
                    return True
                return False

        f = __F(*args, **kw)
        f.site.query = get_sites()
        return f
admin.admin.content_types.append(FreeString)


class FreeStringName(mixins.StringSlot, Base):
    __tablename__ = tables.tablename_('stringnames')
    # pyramid_cms_stringnames
    __descr__ = 'Вільні рядкові слоти для окремих написів(локалі)'
    slot_id = Column(Integer, ForeignKey(tables.fk_('strings')))
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))

    @classmethod
    def _form(cls, *args, **kw):
        """ Форма для внесення даних про локалі слоту. """
        class __F(cls._uiform()):
            exclude = 'id','created','updated','slot_id','lang_id'
            class Meta:
                model = cls
            slot = QuerySelectField()
            lang = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    if Q(cls)\
                    .filter(cls.slot_id == self.slot.data.id,
                            cls.lang_id == self.lang.data.id)\
                    .count() == 0:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                        obj.slot_id = self.slot.data.id
                        obj.lang_id = self.lang.data.id
                        obj.save()
                    return True
                return False

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    obj.slot_id = self.slot.data.id
                    obj.lang_id = self.lang.data.id
                    obj.save()
                    return True
                return False

        f = __F(*args, **kw)
        f.slot.query = get_string_slots()
        f.lang.query = get_langs()
        return f
admin.admin.content_types.append(FreeStringName)


class FreeText(mixins.LiteColumnsMixin, Base):
    __tablename__ = tables.tablename_('texts')  # pyramid_cms_texts
    __descr__ = 'Вільні текстові слоти для окремих написів'
    verbose_name = _('текстовий слот')
    verbose_name_plural = _('текстові слоти')

    def __str__(self):
        return self.mnemo

    site_id = Column(Integer, ForeignKey(tables.fk_('sites')))
    mnemo = Column('mnemo', String(150), default=str(int(time.time())))
    active = Column('active', Boolean, default=True)
    names = relationship('FreeTextName',
                         cascade='all, delete, delete-orphan',
                         lazy='dynamic')
    site = relationship('Site', uselist=False)

    def localized(self, lang_id):
        try:
            locale = self.names\
                .filter_by(lang_id=lang_id)\
                .first()
            if locale is not None:
                return locale.content
            return self.mnemo
        except:
            return self.mnemo

    @classmethod
    def _form(cls, *args, **kw):
        """ Форма для внесення даних про слот. """
        class __F(cls._uiform()):
            exclude = 'id','created','updated','site_id','site'
            class Meta:
                model = cls
            site = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    if Q(cls).filter(cls.mnemo == self.mnemo.data).count() == 0:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                        obj.site = self.site.data
                        obj.save(flush=True)

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    obj.site = self.site.data
                    obj.save(flush=True)
        f = __F(*args, **kw)
        f.site.query = get_sites()
        return f
admin.admin.content_types.append(FreeText)

class FreeTextName(mixins.TextSlot, Base):
    __tablename__ = tables.tablename_('textnames')  # pyramid_cms_textnames
    __descr__ = 'Вільні текстові слоти для окремих написів(локалі)'
    slot_id = Column(Integer, ForeignKey(tables.fk_('texts')))
    lang_id = Column(Integer, ForeignKey(tables.fk_('langs')))

    @classmethod
    def _form(cls, *args, **kw):
        """ Форма для внесення даних про локалі слоту. """
        class __F(cls._uiform()):
            exclude = 'id','created','updated','slot_id','lang_id'
            class Meta:
                model = cls
            slot_id = QuerySelectField()
            lang_id = QuerySelectField()

            def validate_and_add(self):
                # add new instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = cls()
                    if Q(cls)\
                    .filter(cls.slot_id == self.slot_id.data.id,
                            cls.lang_id == self.lang_id.data.id)\
                    .count() == 0:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                        obj.slot_id = self.slot_id.data.id
                        obj.lang_id = self.lang_id.data.id
                        obj.save()

            def validate_and_edit(self):
                # edit existing instance
                columns = [c for c in cls.columns_names() if not c in self.exclude]
                if self.validate():
                    obj = self._obj
                    for attr in columns:
                        for attr in columns:
                            setattr(obj,attr, getattr(self, attr).data)
                    obj.slot_id = self.slot_id.data.id
                    obj.lang_id = self.lang_id.data.id
                    obj.save()
        f = __F(*args, **kw)
        f.slot_id.query = get_text_slots()
        f.lang_id.query = get_langs()
        return f
admin.admin.content_types.append(FreeTextName)
