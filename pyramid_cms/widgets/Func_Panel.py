# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.widgets import AdminWidget

class Func_Panel(AdminWidget):
    """
    Main navigation tool for first page of PCMS.    
    Shows full accessible PCMS functionality at the
    moment.
    """
    def dispatch(self):
        """
        Dispatcher method
        """
        pass
admin.admin.widgets.append(Func_Panel)