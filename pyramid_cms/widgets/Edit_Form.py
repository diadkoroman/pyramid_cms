# -*- coding: utf-8 -*-
from pyramid.httpexceptions import HTTPFound

from pyramid_cms import admin
from pyramid_cms.assets.widgets import AdminWidget
from pyramid_cms.assets.db.sqlalchemy import DBSession, Q



class Edit_Form(AdminWidget):
    """
    Widget for generation edit-item-form
    (admin-side)
    """
    def dispatch(self):
        """ Dispatcher method """
        self.wdata['name'] = self.kw["current"].model_name()
        self.wdata['body'] = None

        # get instance for update
        try:
            item = int(self.req.GET.get("i"))
            i = Q(self.kw["current"]).get(item)
        except Exception as e:
            raise Exception(e)
            item = None
            i = None

        if self.req.method == 'GET':
            try:
                self._data_for_get(i)
            except Exception as e:
                raise Exception(e)
        if self.req.method == 'POST':
            self._data_for_post(i)

    def _data_for_get(self, i):
        """ Generate form in GET context """
        if i:
            form = self.kw["current"]\
            .ui(meta={"csrf_context":self.req.session},
                obj=i,req=self.req.GET)
        else:
            # empty form
            form = self.kw["current"]\
            .ui(meta={"csrf_context":self.req.session},
                req=self.req.GET)
        self.wdata['body'] = form

    def _data_for_post(self, i):
        """ Generate form in POST context """
        if i:
            # in this case form created with obj=instance
            form = self.kw["current"]\
            .ui(self.req.POST,
                meta={"csrf_context":self.req.session},
                req=self.req.GET,
                obj=i,
                files=getattr(self.req, 'FILES', False))
            form.validate_and_edit()
            option = self.req.POST.get("option","leave")
            if option == "leave":
                redir_to = \
                           self.req.route_url("inner",
                                              tail=("pcms",
                                                    "contenttypes",
                                                    self.kw["current"].__name__.lower()))
                raise HTTPFound(location=redir_to)
            form = self.kw["current"]\
                       .ui(self.req.POST,
                           meta={"csrf_context":self.req.session},
                           obj=i,
                           req=self.req.GET)
        form = self.kw["current"]\
                   .ui(self.req.POST,
                       meta={"csrf_context":self.req.session},
                       req=self.req.GET)
        self.wdata['body'] = form

admin.admin.widgets.append(Edit_Form)
