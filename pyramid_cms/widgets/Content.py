# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.widgets import AdminWidget

class Content(AdminWidget): pass
admin.admin.widgets.append(Content)
