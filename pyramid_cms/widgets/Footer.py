# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.widgets import AdminWidget
from pyramid_cms.assets.widgets.xml import AdminXmlWidget


class Footer(AdminWidget):
    pass

admin.admin.widgets.append(Footer)
