# -*- coding: utf-8 -*-


from pyramid_cms import admin
from pyramid_cms.assets.widgets import AdminWidget

#from .forms import createuser_form_widget


def create_step1_block(req, data, **kw):
    data["step1"] = {}
    """ Step 1 Block element """
    data["step1"]["done"] = done = kw["stages_status"][0]
    #step1 = etree.SubElement(
    #                         parent,
    #                         "Section"
    #                         )
    #etree.SubElement(step1,"Title").text = "Step 1"
    data["step1"]["text"] = \
    """
    At this stage we create some essential things used in authorization.
    """
    if not done:
        data["step1"]["url"] = req.route_url("inner", tail=("pcms", "welcome", 1))
    else:
        data["step1"]["url"] = req.route_url("inner", tail=("pcms", "welcome", 1)) + "?retry"
    #etree.SubElement(step1,"Text").text = txt
    # Step 1 buttons
    #buttons = etree.SubElement(step1,"Buttons")
    #attrs_retry = {
    #    "class_": "secondary",
    #    "href": req.route_url("inner", tail=("pcms", "welcome", 1)) + "?retry"
    #    }
    #attrs_go = {
    #    "class_": "",
    #    "href": req.route_url("inner", tail=("pcms", "welcome", 1))
    #    }
    #if done:
    #    etree.SubElement(buttons,"Button", attrs_retry).text = "Uninstall"
    #else:
    #    etree.SubElement(buttons,"Button", attrs_go).text = "Install"


def create_step2_block(req, data, **kw):
    """ Step 2 block element """
    data["step2"] = {}
    data["step2"]["form"] = None
    data["step2"]["done"] = done = kw["stages_status"][1]
    #step2 = etree.SubElement(
    #                         parent,
    #                         "Section"
    #                         )
    #etree.SubElement(step2,"Title").text = "Stage 2"
    #etree.SubElement(step2,"Text").text = ""
    data["step2"]["text"] = \
    """
    Please create root user for PyrAdmin.
    (The form will be opened after the Stage 1 is done.)
    """
    if not done:
        if kw["stages_status"][0]:
            data["step2"]["form"] = kw["form"]
        data["step2"]["url"] = req.route_url("inner", tail=("pcms", "welcome", 2))
    else:
        data["step2"]["url"] = req.route_url("inner", tail=("pcms", "welcome", 2)) + "?retry"
    # Step 2 buttons
    #buttons = etree.SubElement(step2,"Buttons")
    #attrs_retry = {
    #    "class_": "secondary",
    #    "href": req.route_url("inner", tail=("pcms", "welcome", 2)) + "?retry"
    #    }
    #attrs_go = {
    #    "class_": "",
    #    "href": req.route_url("inner", tail=("pcms", "welcome", 2))
    #    }
    #if done:
    #    etree.SubElement(buttons,"Button", attrs_retry).text = "Uninstall"
    #else:
    #    if kw["stages_status"][0]:
    #        # create superuser form
    #        createuser_form_widget(req, step2, **kw)
    #    else:
    #        etree.SubElement(buttons,"Button", attrs_go).text = "Install"

def create_step3_block(req, data, **kw):
    """ Step 3 block element """
    data["step3"] = {}
    data["step3"]["done"] = done = kw["stages_status"][2]
    #step3 = etree.SubElement(
    #                         parent,
    #                         "Section"
    #                         )
    #etree.SubElement(step3,"Title").text = "Stage 3"
    #etree.SubElement(step3,"Text").text = ""
    data["step3"]["text"] = \
    """
    Create main sections of your project`s tree
    """
    if not done:
        data["step3"]["url"] = req.route_url("inner", tail=("pcms", "welcome", 3))
    else:
        data["step3"]["url"] = req.route_url("inner", tail=("pcms", "welcome", 3)) + "?retry"
    # Step 3 buttons
    #buttons = etree.SubElement(step3,"Buttons")
    #attrs_retry = {
    #    "class_": "secondary",
    #    "href": req.route_url("inner", tail=("pcms", "welcome", 3)) + "?retry"
    #    }
    #attrs_go = {
    #    "class_": "",
    #    "href": req.route_url("inner", tail=("pcms", "welcome", 3))
    #    }
    #if done:
    #    etree.SubElement(buttons,"Button", attrs_retry).text = "Uninstall"
    #else:
    #    etree.SubElement(buttons,"Button", attrs_go).text = "Install"
def create_step4_block(req, data, **kw):
    """ Step 4 block element """
    data["step4"] = {}
    data["step4"]["project_url"] = req.route_url("index")
    data["step4"]["login_url"] = req.route_url("inner", tail=("pcms","login"))
    data["step4"]["cancel_url"] = req.route_url("inner", tail=("pcms", "welcome")) + "?cancel"

class Welcome_Screen(AdminWidget):
    """
    Welcome screen widget
    """
    def dispatch(self):
        """ Dispatcher """
        self.wdata['title'] = self.data['title']
        self.wdata['subtext'] = \
                                """
                                There are fiew simple steps to install PyrAdmin.
                                Please click "Go" button to start each step
                                and "Retry" button to retry each one.
                                """
        create_step1_block(self.req, self.wdata, **self.kw)
        create_step2_block(self.req, self.wdata, **self.kw)
        create_step3_block(self.req, self.wdata, **self.kw)
        create_step4_block(self.req, self.wdata, **self.kw)
        self.wdata["stages_status"] = self.kw["stages_status"]
admin.admin.widgets.append(Welcome_Screen)
'''
def welcome_screen_widget(req, data, **kw):
    data["welcome_screen"] = {}
    data["welcome_screen"]["title"] = data["title"]
    data["welcome_screen"]["subtext"] = \
    """
    There are fiew simple steps to install PyrAdmin.
    Please click "Go" button to start each step and "Retry" button 
    to retry each one.
    """
    #widget = etree.SubElement(parent, "Widget", type="Teasers", name="Welcome")
    #
    #meta = etree.SubElement(widget, "MetaData")
    #etree.SubElement(meta, "Class").text = "small-6 small-centered columns"
    
    # title
    #etree.SubElement(widget,"Title").text = "Welcome to PyrAdmin!"
    # subtitle text
    #subtext = \
    #"""
    #There are fiew simple steps to install PyrAdmin.
    #Please click "Go" button to start each step and "Retry" button 
    #to retry each one.
    #"""
    #etree.SubElement(widget,"SubTitle").text = subtext
    #
    # Sections
    #sections = etree.SubElement(widget, "Sections")
    #
    # Steps
    create_step1_block(req, data["welcome_screen"], **kw)
    create_step2_block(req, data["welcome_screen"], **kw)
    create_step3_block(req, data["welcome_screen"], **kw)
    create_step4_block(req, data["welcome_screen"], **kw)
    data["welcome_screen"]["stages_status"] = kw["stages_status"]
    pass

admin.admin.widgets.append(welcome_screen_widget)
'''
