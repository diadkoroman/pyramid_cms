# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.widgets import AdminWidget

class CreateUser_Form(AdminWidget):
    """ Create user form widget """
    pass
admin.admin.widgets.append(CreateUser_Form)
