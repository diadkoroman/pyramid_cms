# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.widgets import AdminWidget

class Logout_Form(AdminWidget):
    """ Logout user form widget """
    pass
admin.admin.widgets.append(Logout_Form)
