# -*- coding: utf-8 -*-
from pyramid.httpexceptions import HTTPFound

from pyramid_cms import admin
from pyramid_cms.assets.widgets import ProjectWidget

class Project_Head(ProjectWidget):
    """
    Widget to generate page <head> tag content
    (project-side)
    """
    default_temp_dir_index = 1
    
    def dispatch(self):
        pass
        


admin.admin.widgets.append(Project_Head)
