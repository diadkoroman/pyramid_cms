# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.widgets import AdminWidget
from pyramid_cms.assets.widgets.xml import AdminXmlWidget


class TopNav(AdminWidget):
    def dispatch(self):
        if self.kw.get('plugins', False):
            self.wdata['body']['title'] = 'Plugins'
            self.wdata['body']['nodes'] = [p.menu_data()
                                for p in self.kw['plugins'].all]

admin.admin.widgets.append(TopNav)
