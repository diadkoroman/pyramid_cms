# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.widgets import AdminWidget
from pyramid_cms.assets.widgets.xml import AdminXmlWidget


class BreadCrumbs(AdminWidget):

    def dispatch(self):
        if self.kw.get('node', None):
            self.wdata['body'] = self.kw['node'].get_bcrs()

admin.admin.widgets.append(BreadCrumbs)
