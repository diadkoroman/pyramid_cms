# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.widgets import AdminWidget

class Header(AdminWidget):
    def dispatch(self):
        pass
        
admin.admin.widgets.append(Header)
