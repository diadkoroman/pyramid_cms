# -*- coding: utf-8 -*-

from pyramid_cms import admin
from pyramid_cms.assets.widgets import AdminWidget


class Login_Form(AdminWidget):
    """ Login user form widget """
    # xml_mode = True
    def dispatch(self):
        """ Dispatcher """
        self.wdata['body'] = self.kw['form']
        self.wdata['meta']['pgeotrust'] = admin.admin.config.enable_pgeotrust

admin.admin.widgets.append(Login_Form)
