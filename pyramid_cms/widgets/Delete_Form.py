# -*- coding: utf-8 -*-
from pyramid.httpexceptions import HTTPFound

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import DBSession, Q
from pyramid_cms.assets.widgets import AdminWidget

class Delete_Form(AdminWidget):
    """
    Widget for generation form to delete items.
    """
    def dispatch(self):
        """ Dispatcher """
        delete = self.req.GET.get("delete", None)
        path = self.kw["node"].get_path()
        self.wdata["name"] = ''
        self.wdata["baseurl"] = ''
        self.wdata["childobj"] = None
        # Getting item to delete
        try:
            item = int(self.req.GET.get("i"))
            i = Q(self.kw["current"]).get(item)
            self.wdata["name"] = getattr(i,'inner_name', 'noname')
            self.wdata["baseurl"] = "{0}?i={1}"\
                .format(path, str(i.id))
        except:
            item = None
            i = None
        if delete and delete == "y":
            if i:
                i.delete()
                raise HTTPFound(location=path)
admin.admin.widgets.append(Delete_Form)
