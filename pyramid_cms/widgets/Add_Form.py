# -*- coding: utf-8 -*-
from pyramid.httpexceptions import HTTPFound

from pyramid_cms import admin
from pyramid_cms.assets.widgets import AdminWidget


class Add_Form(AdminWidget):
    """
    Widget for generation of add-item-form
    (admin-side)
    """
    def dispatch(self):
        self.wdata['name'] = self.kw["current"].model_name()
        self.wdata['body'] = None
        """ Dispatcher method """
        if self.req.method == 'GET':
            self._data_for_get()
        if self.req.method == 'POST':
            self._data_for_post()
        

    def _data_for_get(self):
        """ Generate form in GET context """
        if 'add' in self.req.GET:
            form = self.kw["current"]\
            .ui(meta={"csrf_context":self.req.session},
                req=self.req.GET)
            self.wdata['body'] = form

    def _data_for_post(self):
        """ Generate form in POST context """
        if "add" in self.req.GET:
            # create form for POST...
            form = self.kw["current"]\
            .ui(self.req.POST,
                meta={"csrf_context":self.req.session},
                req=self.req.GET, files=getattr(self.req, 'FILES', False))
            #... and add data
            if form.validate_and_add():
                #option = self.req.POST.get("option","stay")
                #if option == "leave":
                redir_to = \
                self.req.route_url("inner",
                                tail=("pcms",
                                    "contenttypes",
                                    self.kw["current"].__name__.lower()))
                #raise Exception(redir_to)
                raise HTTPFound(location=redir_to)
            else:
                pass
            self.wdata['body'] = form

admin.admin.widgets.append(Add_Form)
