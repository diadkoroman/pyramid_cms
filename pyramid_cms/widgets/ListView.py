# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.widgets import AdminWidget
from pyramid_cms.assets.widgets.xml import AdminXmlWidget


class ListView(AdminWidget):
    """
    Widget providing listing output for
    UI
    """
    default_pagenum = 1
    current_page = None
    prev_page = 1
    next_page = 1
    def get_current_page(self):
        """
        Отримати поточну сторінку.
        Пробуємо визначити наявність номера поточної сторінки в 
        GET-запиті.
        Якщо не знаходимо - присвоюємо значення за умовчанням.
        """
        try:
            self.current_page = int(self.req.GET.get('p'))
        except:
            self.current_page = self.default_pagenum
    
    def get_prev_page(self):
        """ Отримати попередню сторінку для горталки """
        if self.current_page:
            if self.current_page > 1:
                self.prev_page = self.current_page - 1

    def get_next_page(self):
        """ Отримати наступну сторінку для горталки """
        if self.current_page:
            self.next_page = self.current_page + 1

    def dispatch(self):
        self.get_current_page()
        self.get_prev_page()
        self.get_next_page()
        self.wdata['body'] = self.kw["current"].table(page=self.current_page)
        self.wdata['current_page'] = self.current_page
        self.wdata['prev_page'] = self.prev_page
        self.wdata['next_page'] = self.next_page


admin.admin.widgets.append(ListView)
