# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.widgets import AdminWidget
from pyramid_cms.assets.widgets.xml import AdminXmlWidget



class SideNav(AdminWidget):

    def dispatch(self):
        self.wdata['contenttypes'] = []
        self.wdata['plugins'] = []
        for i in self.kw['content_types'].sorted(sort_mode='asc'):
            i_dict = {}
            i_dict['name'] = i.__name__
            i_dict['path'] = \
                             self.req.route_url('inner',
                                                tail=(
                                                    'pcms',
                                                    'contenttypes',
                                                    i.__name__.lower()))
            self.wdata['contenttypes'].append(i_dict)

        if self.kw.get('plugins', False):
            for i in self.kw['plugins'].sorted(sort_mode='asc'):
                i_dict = {}
                i_dict['name'] = i.__class__.__name__
                i_dict['path'] = \
                                 self.req.route_url('inner',
                                                    tail=(
                                                        'pcms',
                                                        'plugins',
                                                        i.__class__.__name__.lower()))
                self.wdata['plugins'].append(i_dict)

admin.admin.widgets.append(SideNav)
