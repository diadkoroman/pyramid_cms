# -*- coding: utf-8 -*-

from pyramid.httpexceptions import HTTPFound
# from pyramid.response import Response

from pyramid_cms import admin
from pyramid_cms.assets.views import DownloadsViewType
from pyramid_cms.config.scripts.init_scenarios import start_init


class Project_Downloads(DownloadsViewType):

    """
    Сторінка для опрацювання завантажень файлів.
    """

    def prepare_data(self):
        self.vdata['test'] = self.widgets
        if False in start_init(self.req):
            raise HTTPFound(
                location=self.req.route_url(
                    "inner", tail=("pcms", "welcome")))

admin.admin.viewtypes.append(Project_Downloads)
