# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.views import ViewType
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.apps.roots.dbmodels import TreeNode

class Admin_Inner(ViewType):

    #template = 'admin_home.pt'
    #template = 'admin_page.xsl'
    #test = True
    #test_xml_document = True
    #widgets = ['topnav',]
    
    def plugins_section(self):
        try:
            plugin_name = self.kw['tail'].pop()
            self.kw['current'] = admin.admin.plugins.get(plugin_name)
            self.vdata['test'] = self.widgets
        except:
            pass
            
    def manage_widgets(self):
        ''' Get additional widgets '''
        try:
            if getattr(self.kw['current'],'widgets', False):
                for w in self.kw['current'].widgets:
                    if w.startswith(self.kw['node'].mnemo):
                        self.widgets.append(w)
        except:
            pass

    def get_commons(self):
        self.kw['content_types'] = admin.admin.content_types
        self.kw['plugins'] = admin.admin.plugins
        # Plugins section
        if admin.admin.config.plugins_dir in self.rmd.get('tail'):
            self.plugins_section()
        

admin.admin.viewtypes.append(Admin_Inner)
