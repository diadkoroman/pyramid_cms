# -*- coding: utf-8 -*-

"""
Logout View Class
"""

from pyramid.httpexceptions import HTTPFound
from pyramid.security import forget

from pyramid_cms import admin
from pyramid_cms.assets.views import ViewType


class Admin_Logout(ViewType):

    def prepare_data(self):
        # raise Exception(self.widgets)
        pass

    def get(self):
        headers = forget(self.req)
        return HTTPFound(location=self.req.route_url("index"), headers=headers)
        

admin.admin.viewtypes.append(Admin_Logout)
