# -*- coding: utf-8 -*-
from pyramid.httpexceptions import HTTPFound
from pyramid.response import Response
from pyramid.view import view_config

from pyramid_cms import admin
from pyramid_cms.assets.views import AjaxViewType
from pyramid_cms.config.scripts.init_scenarios import start_init

class AdminAjaxPage(AjaxViewType):
    """  """
    renderer = 'json'

    def curr_location_option(self):
        """
        Getting current user location
        """
        from pgeotrust import pgtm
        try:
            pgtm.set_curr_location(curr_location= {
            'lat': float(self.data['lat']),
            'lng': float(self.data['lng'])})
            self.resp = {'response':'success'}
        except:
            self.resp = {'response':'fail'}

    def get(self):
        return super().get(self.renderer)

    def post(self):
        return super().post(self.renderer)

admin.admin.viewtypes.append(AdminAjaxPage)
