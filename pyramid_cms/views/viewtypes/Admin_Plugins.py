# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.views import ViewType, AdminViewType
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.apps.roots.dbmodels import TreeNode

class Admin_Plugins(AdminViewType):

    #template = 'admin_home.pt'
    #template = 'admin_page.xsl'
    #test = True
    #test_xml_document = True
    #widgets = ['topnav',]
    
    def plugins_section(self):
        """ Get current plugin by section name """
        try:
            # get section name
            plugin_name = self.kw['tail'].pop()
            # get current plugin instance from propoer stack
            self.kw['current'] = admin.admin.plugins.get(plugin_name)
            # set plugin request attribute
            self.kw['current'].req = self.req
            self.vdata['template'] = self.kw['current'].get_template()
        except:
            pass
            
    def manage_widgets(self):
        """ Get additional widgets """
        if getattr(self.kw['current'],'widgets', False):
            for w in self.kw['current'].widgets:
                if w.startswith(self.kw['node'].mnemo):
                    self.widgets.append(w)

    def get_commons(self):
        self.vdata['template'] = None
        self.kw['content_types'] = admin.admin.content_types
        self.kw['plugins'] = admin.admin.plugins
        self.plugins_section()
        

admin.admin.viewtypes.append(Admin_Plugins)
