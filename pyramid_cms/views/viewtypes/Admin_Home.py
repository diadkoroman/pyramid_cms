# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.views import ViewType
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.apps.roots.dbmodels import TreeNode


class Admin_Home(ViewType):

    #template = 'admin_home.pt'
    #template = 'admin_page.xsl'
    #test = True
    test_xml_document = True
    #widgets = ['topnav',]

    def prepare_data(self):
        self.vdata['test'] = self.widgets
        self.kw['content_types'] = admin.admin.content_types
        self.kw['plugins'] = admin.admin.plugins

admin.admin.viewtypes.append(Admin_Home)
