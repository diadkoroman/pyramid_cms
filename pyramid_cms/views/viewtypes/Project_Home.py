# -*- coding: utf-8 -*-
from pyramid.httpexceptions import HTTPFound
from pyramid.response import Response

from pyramid_cms import admin
from pyramid_cms.assets.views import ViewType, ProjectViewType
from pyramid_cms.config.scripts.init_scenarios import start_init

class Project_Home(ProjectViewType):

    #template = "admin_home.pt"
    #template = "project_home.xsl"
    #test = True
    #test_xml_document = True
    #widgets = ["topnav","welcome_teaser"]

    def prepare_data(self):
        self.vdata['test'] = self.widgets
        #if False in start_init(self.req):
        #    raise HTTPFound(location=self.req.route_url("inner", tail=("pcms","welcome")))

admin.admin.viewtypes.append(Project_Home)
