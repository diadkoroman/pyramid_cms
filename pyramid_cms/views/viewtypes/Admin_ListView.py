# -*- coding: utf-8 -*-
from pyramid.httpexceptions import HTTPFound
from pyramid_cms import admin
from pyramid_cms.assets.views import ViewType
from pyramid_cms.assets.db.sqlalchemy import DBSession, Q
from pyramid_cms.apps.roots.dbmodels import TreeNode
from pyramid_cms.apps.auth.dbmodels import Group


class Admin_ListView(ViewType):

    def get_commons(self):
        #self.vdata['test'] = admin.admin.config.imgs
        self.kw['content_types'] = admin.admin.content_types
        self.kw['plugins'] = admin.admin.plugins
        # get current content type
        self.kw['current'] = self.kw['content_types'].get(self.kw['tail'].pop())
        #self.kw['node'] = self.node
        
    def get_POSTs(self):
        #self.vdata['test'] = self.req.POST['image'].__dict__
        pass

    def manage_widgets(self):
        if 'add' in self.req.GET:
            if 'listview' in self.widgets:
                self.widgets.remove('listview')
            self.widgets.append('add_form')
        elif 'delete' in self.req.GET:
            if 'listview' in self.widgets:
                self.widgets.remove('listview')
            if 'add_form' in self.widgets:
                self.widgets.remove('add_form')
            if 'edit_form' in self.widgets:
                self.widgets.remove('edit_form')
        try:
            item = int(self.req.GET.get('i'))
        except:
            item = None
        if item:
            if 'listview' in self.widgets:
                self.widgets.remove('listview')
            if 'delete' in self.req.GET:
                self.widgets.append('delete_form')
            else:
                self.widgets.append('edit_form')

admin.admin.viewtypes.append(Admin_ListView)
