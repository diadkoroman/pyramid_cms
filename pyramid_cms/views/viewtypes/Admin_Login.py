# -*- coding: utf-8 -*-
from pyramid.httpexceptions import HTTPFound
from pyramid.response import Response
from pyramid.security import remember

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.handlers import encr
from pyramid_cms.assets.forms import ProtectedForm
from pyramid_cms.assets.views import ViewType

from pyramid_cms.apps.auth.dbmodels import User




class Admin_Login(ViewType):

    #template = "admin_home.pt"
    #template = "admin_page.xsl"
    #test = True
    #test_xml_document = True
    #widgets = ["topnav",]
    
    def check_user_token(self, user_token):
        """ Перевірити користувацький токен """
        if Q(User)\
        .filter(User.token == user_token, User.active == True)\
        .count() > 0:
            return True
        return False

    def create_login_form(self):
        from pyramid_cms.forms import LoginForm
        '''
        from wtforms import form
        from wtforms import fields
        from wtforms import validators
        class LoginForm(ProtectedForm):
            login = fields.StringField("Log In",
                                       description=\
                                       "Enter your login here.",
                                       validators=[validators.InputRequired()]
                                       )
            passw = fields.PasswordField("Password",
                                         description=\
                                         "Enter your password here.",
                                         validators=[validators.InputRequired()]
                                         )
            def login_user(self, req):
                # set login url
                login_url = req.route_url("inner",tail=("pcms","login"))
                # set referer url
                referrer = req.url
                # if user visits login page directly admin_home page becomes referer
                #if referrer == login_url:
                referrer = req.route_url("inner",tail=("pcms",))
                came_from = req.params.get("came_from", referrer)
                # login.data is used as a key, encrypted passw.data - as a value.
                if admin.security.users.get(self.login.data) == encr.encr1(self.passw.data):
                    headers = remember(req, self.login.data)
                    raise HTTPFound(location = came_from, headers = headers)
        '''
        if self.req.method == "POST":
            form = LoginForm(self.req.POST, meta={"csrf_context":self.req.session})
            if form.validate():
                form.login_user(self.req)
                self.vdata["test"] = "VALID"
            else:
                self.vdata["test"] = "NOT VALID"
            return form
        return LoginForm(meta={"csrf_context":self.req.session})

    def prepare_data(self):
        self.kw["form"] = self.create_login_form()
        
    def get(self):
        user_token = self.req.GET.get('tkn', None)
        if not user_token or not self.check_user_token(user_token):
            raise HTTPFound(location='/404/')
        else:
            return super().get()
            
    def post(self):
        user_token = self.req.GET.get('tkn', None)
        if not user_token or not self.check_user_token(user_token):
            raise HTTPFound(location='/404/')
        else:
            return super().post()

admin.admin.viewtypes.append(Admin_Login)
