# -*- coding: utf-8 -*-
from pyramid_cms import admin
from pyramid_cms.assets.views import ViewType
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.apps.roots.dbmodels import TreeNode

class Admin_FM(ViewType):
    """
    Тип в’ю, що використовується для відображення вікна вбудованого
    файлового менеджера.
    По ідеї, в’ю має бути подібним до в’юхи плагіна, за тим винятком,
    що використовуватиме інший шаблон для відображення - без панелей,
    бокових та інших меню тощо.
    Тому за основу для цього в’ю взято viewtypes.Admin_Plugins, а сам
    файловий менеджер буде виконаний у вигляді одного з плагінів.
    """
    
    def plugins_section(self):
        """ Get current plugin by section name """
        try:
            # get section name
            plugin_name = self.kw['tail'].pop()
            # get current plugin instance from propoer stack
            self.kw['current'] = admin.admin.plugins.get(plugin_name)
            # set plugin request attribute
            self.kw['current'].req = self.req
            self.vdata['template'] = self.kw['current'].get_template()
        except:
            pass
            
    def manage_widgets(self):
        """ Get additional widgets """
        if getattr(self.kw['current'],'widgets', False):
            for w in self.kw['current'].widgets:
                if w.startswith(self.kw['node'].mnemo):
                    self.widgets.append(w)

    def get_commons(self):
        self.vdata['template'] = None
        self.kw['content_types'] = admin.admin.content_types
        self.kw['plugins'] = admin.admin.plugins
        self.plugins_section()
        

admin.admin.viewtypes.append(Admin_FM)
