# -*- coding: utf-8 -*-
from pyramid.httpexceptions import HTTPFound
from pyramid.response import Response
# -*- coding: utf-8 -*-

from pyramid_cms import admin
from pyramid_cms.assets.views import ViewType
from pyramid_cms.config.scripts.init_scenarios import start_init

class Admin_Init(ViewType):

    #template = "admin_home.pt"
    template = "admin_init.pt"
    #test = True
    #test_xml_document = True
    widgets = ["welcome_screen"]
    
    def create_superuser_form(self):
        from wtforms import form
        from wtforms import fields
        class SuperuserForm(form.Form):
            login = fields.StringField("Log In", description="Enter your login here.")
            passw = fields.PasswordField("Password", description="Enter your password here.")
        if self.req.method == "POST":
            return SuperuserForm(self.req.POST, meta={"csrf_context":self.req.session})
        return SuperuserForm(meta={"csrf_context":self.req.session})

    def prepare_data(self):
        """ Data preparation method """
        self.kw["form"] = self.create_superuser_form()
        #raise Exception(self.kw["form"]["login"].label.text)
        
        #title = etree.SubElement(self.vdata,"DocTitle")
        self.vdata["title"] = "Welcome to PyrAdmin!"
        
        # get last item from URL
        try:
            self.kw["stage"] = int(self.kw["tail"].pop())
        except:
            self.kw["stage"] = None
        self.kw["restart"] = "retry" in self.kw["get"]
        # Let`s start init with all options we have got
        self.kw["stages_status"] = start_init(self.req, **self.kw)
        if self.kw["stage"] == 3 and not False in self.kw["stages_status"]:
            raise HTTPFound(location=self.req.route_url("inner", tail=("pcms","welcome")))


    def get(self):
        return self.render()

admin.admin.viewtypes.append(Admin_Init)
