# -*- coding: utf-8 -*-

from pyramid.response import Response
from pyramid.view import view_config, view_defaults

from pyramid_cms import admin
from pyramid_cms.assets.views import ViewFramework

'''
@view_defaults(route_name='ajax_json')
class AjaxJsonRouteFramework(object):

    test = True

    @view_config(request_method="GET", renderer='json')
    def get(self):
        return self._impl.get()

    @view_config(request_method="POST", renderer='json')
    def post(self):
        return self._impl.post()
'''
