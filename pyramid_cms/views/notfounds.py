# -*- coding: utf-8 -*-

from pyramid.response import Response
from pyramid.view import (
    view_config,
    forbidden_view_config,
    notfound_view_config
)

#############################################################
@notfound_view_config(append_slash = True)
def nfound_get(request):
    return Response('RESOURCE NOT FOUND',status='404')
#############################################################
