# -*- coding: utf-8 -*-

from pyramid.response import Response
from pyramid.view import view_config, view_defaults

from pyramid_cms import admin
from pyramid_cms.assets.views import ViewFramework

@view_defaults(route_name="index_localized")
class LocalizedIndexRouteFramework(ViewFramework):

    test = True

    @view_config(request_method="GET")
    def get(self):
        return self._impl.get()

    @view_config(request_method="POST")
    def post(self):
        return self._impl.post()
