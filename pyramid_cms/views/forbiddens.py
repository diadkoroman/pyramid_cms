# -*- coding: utf-8 -*-
from pyramid.httpexceptions import HTTPFound
from pyramid.response import Response
from pyramid.view import (
    forbidden_view_config,
)

#############################################################
@forbidden_view_config()
def forbidden_view(req):
    return HTTPFound(
        location = req.route_url('admin_login')
        )
#############################################################
