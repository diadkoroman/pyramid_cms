# -*- coding: utf-8 -*-

from pyramid.security import remember
from pyramid.httpexceptions import HTTPFound

from wtforms import form
from wtforms import fields
from wtforms import validators

from pyramid_cms import admin
from pyramid_cms.assets.db.sqlalchemy import Q
from pyramid_cms.assets.handlers import encr
from pyramid_cms.assets.forms import ProtectedForm
from pyramid_cms.apps.auth.dbmodels import User

if admin.admin.config.enable_pgeotrust:
    from pgeotrust import pgtm

class LoginForm(ProtectedForm):
    _user_obj = None
    login = fields.StringField("Log In",
                               description=\
                               "Enter your login here.",
                               validators=[validators.InputRequired()]
                               )
    passw = fields.PasswordField("Password",
                                 description=\
                                 "Enter your password here.",
                                 validators=[validators.InputRequired()]
                                )
                                
    def _get_user(self, login, passw):
        self._user_obj = None
        try:
            self._user_obj = Q(User)\
            .filter(User.login == login, User.password == encr.encr1(passw))\
            .first()
        except:
            pass

    def get_user_id(self, login, passw):
        if self._user_obj:
            return self._user_obj.id
        return None
        
    def check_user_token(self, user_token):
        # checking user token
        if self._user_obj.token == user_token:
            return True
        return False
        
    def login_user(self, req):
        # get login token
        user_token = req.GET.get('tkn', None)
        if user_token:
            # set login url
            login_url = req.route_url("inner",tail=("pcms","login"))
            # set referer url
            referrer = req.url
            # if user visits login page directly admin_home page becomes referer
            #if referrer == login_url:
            referrer = req.GET.get('next', False) or req.route_url("inner",tail=("pcms",))
            came_from = req.params.get("came_from", referrer)
            # login.data is used as a key, encrypted passw.data - as a value.
            if admin.security.users.get(self.login.data) == encr.encr1(self.passw.data):

                # getting user
                self._get_user(self.login.data, self.passw.data)

                if admin.admin.config.enable_pgeotrust:
                    #pgeotrust enabled
                    if pgtm.validate_user_location(user_id=self.get_user_id(self.login.data, self.passw.data)):
                        if self.check_user_token(user_token):
                            headers = remember(req, self.login.data)
                            raise HTTPFound(location = came_from, headers = headers)
                else:
                    #pgeotrust disabled
                    if self.check_user_token(user_token):
                        headers = remember(req, self.login.data)
                        raise HTTPFound(location = came_from, headers = headers)
