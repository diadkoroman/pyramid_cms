$(function(){
    $.datetimepicker.setLocale('uk');
    var date_picker = $('input[name=created], input[name=date_visible]');
    date_picker.datetimepicker({
        format: 'Y-m-d H:i:s',
    });

    var date_picker2 = $('input[type=date]');
    date_picker2.datetimepicker({
        format: 'Y-m-d',
    });
});
