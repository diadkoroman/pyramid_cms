var message_geolocation_success = "Thank you! Your location data have been successfully registered!";
var message_geolocation_fail = "Ups... Something went wrong with your data. Please try again."

function handleErrors(err){
    alert('Error');
    };

function sendPositionData(position) {
    sendAjax(position);
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(sendPositionData,handleErrors);
    } else { 
        alert("Geolocation is not supported by this browser.");
    }
}


function sendAjax(position){
    $.ajax({
            method:'GET',
            url:'/pcms/ajax.json',
            data: {
                'option':'curr_location',
                'lat':position.coords.latitude,
                'lng': position.coords.longitude
            },
            success: function(data){
                var data = $.parseJSON(data);
                console.log(data['response'])
                if(data['response'] == 'success'){
                    alert(message_geolocation_success);
                }else{
                    alert(message_geolocation_fail);
                };
                
            },
    });
};


getLocation();


