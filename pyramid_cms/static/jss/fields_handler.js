$(function(){
    var textinput = $('.form input[type=text]');
    var dateinput = $('.form input[type=date]');
    var numberinput = $('.form input[type=number]');
    var select = $('.form select');
    
    function handleTextInputs(){
        textinput.each(function(){
            $(this).addClass('form-control input-sm');
            $(this).parent().addClass('col-xs-12');
        });
    };
    
    function handleDateInputs(){
        dateinput.each(function(){
            $(this).addClass('form-control input-sm');
            $(this).parent().addClass('col-xs-3');
        });
    };
    
    function handleNumberInputs(){
        numberinput.each(function(){
            $(this).addClass('form-control input-sm');
            $(this).parent().addClass('col-xs-3');
        });
    };
    
    function handleSelects(){
        select.each(function(){
            $(this).addClass('form-control input-sm select');
            $(this).parent().addClass('col-xs-6');
        });
    };
    
    function init(){
        handleTextInputs();
        handleDateInputs();
        handleNumberInputs();
        handleSelects();
    };
    
    // init
    init();
});
