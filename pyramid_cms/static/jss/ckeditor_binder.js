$(function(){
    $('textarea[name|=texts]').ckeditor({
        height:350,
        filebrowserBrowseUrl: '/pcms/plugins/fm/',
        filebrowserUploadUrl: '/pcms/plugins/fm/',
        filebrowserImageBrowseUrl: '/pcms/plugins/fm/?opt=images',
        filebrowserImageUploadUrl: '/pcms/plugins/fm/?opt=images',
        allowedContent: {
            $1:{
                elements: CKEDITOR.dtd,
                attributes: true,
                styles: true,
                classes: true,
                },
            },
        disallowedContent: 'script; *[on*]'
    });
});

$(function(){
    $('textarea[name|=annots]').ckeditor({
        height:200,
        allowedContent: {
            $1:{
                elements: CKEDITOR.dtd,
                attributes: true,
                styles: true,
                classes: true,
                },
            },
        disallowedContent: 'script; *[on*]'
    });
});

$(function(){
    $('textarea[name|=promo_annots]').ckeditor({
        height:200,
        allowedContent: {
            $1:{
                elements: CKEDITOR.dtd,
                attributes: true,
                styles: true,
                classes: true,
                },
            },
        disallowedContent: 'script; *[on*]'
    });
});
    
$(function(){
    $('textarea[name|=useful_infos]').ckeditor({
        height:200,
        allowedContent: {
            $1:{
                elements: CKEDITOR.dtd,
                attributes: true,
                styles: true,
                classes: true,
                },
            },
        disallowedContent: 'script; *[on*]'
    });
});

$(function(){
    $('textarea[name=content]').ckeditor({
        height:200,
        allowedContent: {
            $1:{
                elements: CKEDITOR.dtd,
                attributes: true,
                styles: true,
                classes: true,
                },
            },
        disallowedContent: 'script; *[on*]'
    });
});
