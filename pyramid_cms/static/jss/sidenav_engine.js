$(function(){
    var category = $('.menu-category');
    var scroll_area = $('.scrolling-area');
    var avH = screen.availHeight;
    
    
    // hide all categs
    function hideCategs(){
        scroll_area.hide();
    };
    
    //
    function toggleCategory(obj){
        var current_categ = obj.siblings('.scrolling-area');
        scroll_area.hide();
        current_categ.show();
    };
    
    //
    function makeScrolled(){
        scroll_area.each(function(){
            var h = $(this).innerHeight();
            if(h > (avH/2)){
                $(this).innerHeight((avH/2));
                $(this).css({'overflow':'auto'});
            };
        });
    };
    
    //
    function init(){
        hideCategs();
        makeScrolled();
        category.click(function(){
            toggleCategory($(this));
        });
    };
    
    init();
});
