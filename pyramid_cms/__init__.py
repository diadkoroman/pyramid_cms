# -*- coding: utf-8 -*-
import transaction
from pyramid.events import NewRequest
from pyramid.events import subscriber
from pyramid_cms.base import admin
from pyramid_cms.config import settings
from pyramid_cms.assets.db.sqlalchemy.connection import Base, Q
from pyramid_cms.apps.roots import dbmodels as rootsdb
from pyramid_cms.routes import routes
from pyramid_cms.config.scripts.section_scenarios import (
    check_contenttypes_sections,
    check_plugins_sections
    )

###############################################################################
@subscriber(NewRequest)
def add_req_to_admin(event):
    """
    Here we add request to our admin instance.
    So we can use admin.req later everywhere we need it.
    """
    admin.req = event.request
###############################################################################


###############################################################################
def curr_lang(req):
    """
    Setting current language for each request.
    1. Try to get lang value from tail[0]
    2. If 1. fails - set default lang value
    3. Try to get language item from database
    4. If 3. fails - set default lang mnemo
    """
    lmnemo = req.matchdict.get('lang', admin.admin.config.deflang)
    req.curr_lang = Q(rootsdb.Language).filter(rootsdb.Language.mnemo == lmnemo).scalar()

###############################################################################



###############################################################################
def curr_site(req):
    """
    Setting current site for each request.
    1. Try to get site by request.host_url attribute
    2. If site was not found - create new site item in DB
    and get site just created.
    """

    """
    Є підозра, що цей метод проблемний у поточній реалізації, бо дозволяє
    створювати спонтанні хости. Тому пробуємо прибрати функціонал створення
    нових хостів автоматично.
    """
    '''
    def _create_site(req):
        """ Creation of new site """
        new_site = rootsdb.Site()
        new_site.inner_name = req.host_url
        new_site.address = req.host_url
        new_site.active = True
        new_site.save()
    '''
    try:
        site = Q(rootsdb.Site)\
               .filter(rootsdb.Site.address == req.host_url)\
               .first()
        #if not site:
        #    _create_site(req)
        #    site = Q(rootsdb.Site)\
            #                   .filter(rootsdb.Site.address == req.host_url)\
            #                   .first()
        return site
    except:
        return None

###############################################################################

###############################################################################
def add_auth_policy(config):
    """ Add autorization and authentication policies. """
    from pyramid.authentication import AuthTktAuthenticationPolicy
    from pyramid.authorization import ACLAuthorizationPolicy
    from pyramid_cms.assets import groupfinder

    autn_pol = AuthTktAuthenticationPolicy("secrvar",
        callback=groupfinder, hashalg="sha512")
    autz_pol = ACLAuthorizationPolicy()
    config.set_authentication_policy(autn_pol)
    config.set_authorization_policy(autz_pol)
###############################################################################

###############################################################################
def add_session_factory(config):
    """ Adding session factory. """
    # from pyramid_beaker import session_factory_from_settings
    from pyramid.session import SignedCookieSessionFactory

    asf = SignedCookieSessionFactory("admin_sessF",timeout=3600)
    # asf = session_factory_from_settings(config.registry.settings)
    config.set_session_factory(asf)
###############################################################################

###############################################################################
def add_request_methods(config):
    """ Adding specific request methods """
    # config.add_request_method(curr_lang, reify=True)
    config.add_request_method(curr_site, reify=True)

###############################################################################
def set_root_factory(config):
    """ Setting root factory """
    from pyramid_cms.assets.resources import resource_factory
    config.set_root_factory(resource_factory)
###############################################################################

###############################################################################
def set_pgeotrust(engine, config):
    """ Setting pgeotrust module """
    try:
        from pgeotrust import pgeotrust_init
        pgeotrust_init(engine, config)
    except:
        pass
###############################################################################

###############################################################################
def create_tables(engine):
    """ SQL engine init. """
    Base.metadata.create_all(engine)
###############################################################################

def admin_init(engine, config):
    """ Ініціалізатор адмін-частини. Запускається на боці проекту. """
    # створюємо усі таблиці в БД
    create_tables(engine)
    
    """
    перевіряємо наявність розділів для кожної таблиці БД 
    і при потребі створюємо.
    """
    check_contenttypes_sections()
    
    """
    перевіряємо наявність розділів для кожного плагіна
    і при потребі створюємо.
    """
    check_plugins_sections()
    
    """ Додаємо субскрайбери """
    config.add_subscriber(
        'pyramid_cms.apps.stats.subscribers.request_timestamp',
        'pyramid.events.NewRequest')
    config.add_subscriber(
        'pyramid_cms.apps.stats.subscribers.req_tcontroller',
        'pyramid.events.NewRequest')
    config.add_subscriber(
        'pyramid_cms.apps.stats.subscribers.response_timestamp',
        'pyramid.events.NewResponse')
    config.add_subscriber(
        'pyramid_cms.apps.stats.subscribers.req_tcontroller2',
        'pyramid.events.NewRequest')
    config.add_subscriber(
        'pyramid_cms.apps.stats.subscribers.visits',
        'pyramid.events.NewResponse')
    config.add_subscriber(
        'pyramid_cms.apps.stats.subscribers.visits_counter',
        'pyramid.events.NewResponse')


def includeme(config):
    # session backend
    config.include("pyramid_beaker")
    # xslt renderer
    # config.include("xsl_renderer")
    
    # add request to admin object to operate it
    config.add_subscriber(add_req_to_admin, 'pyramid.events.NewRequest')
    #config.scan(".")
    # turn on auth policy
    add_auth_policy(config)

    # add request methods
    add_request_methods(config)
    
    # create session factory
    add_session_factory(config)
    
    # create root factory
    set_root_factory(config)

    config.include(routes)

    """
    Modules including section
    """

    # setting pgeotrust module
    """
    pgeotrust is security module for checking
    autenticating user location
    """
    config.include('pgeotrust')
    
    config.scan('.')

